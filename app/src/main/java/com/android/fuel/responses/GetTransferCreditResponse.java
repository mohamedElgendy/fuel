package com.android.fuel.responses;

import com.android.fuel.beans.WalletTransaction;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/18/2017.
 */

public class GetTransferCreditResponse {

    private WalletTransaction res;


    public WalletTransaction getRes() {
        return res;
    }

    public void setRes(WalletTransaction res) {
        this.res = res;
    }
}
