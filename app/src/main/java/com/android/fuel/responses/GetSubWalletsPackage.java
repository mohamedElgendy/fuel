package com.android.fuel.responses;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/21/2017.
 */

public class GetSubWalletsPackage {

    private ArrayList<SubWalletPurchasePackageResponse> items;

    public ArrayList<SubWalletPurchasePackageResponse> getItems() {
        return items;
    }

    public void setItems(ArrayList<SubWalletPurchasePackageResponse> items) {
        this.items = items;
    }
}
