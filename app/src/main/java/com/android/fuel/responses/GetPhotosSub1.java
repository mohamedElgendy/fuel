package com.android.fuel.responses;

/**
 * Created by beshoy adel on 11/11/2017.
 */

public class GetPhotosSub1 {

    private GetPhotosSub2 files;


    public GetPhotosSub2 getFiles() {
        return files;
    }

    public void setFiles(GetPhotosSub2 files) {
        this.files = files;
    }
}
