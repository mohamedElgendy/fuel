package com.android.fuel.responses;

import com.android.fuel.beans.WalletTransaction;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/19/2017.
 */

public class GetWalletTransactionsResponse {

    private ArrayList<WalletTransaction> items;

    public ArrayList<WalletTransaction> getItems() {
        return items;
    }

    public void setItems(ArrayList<WalletTransaction> items) {
        this.items = items;
    }
}
