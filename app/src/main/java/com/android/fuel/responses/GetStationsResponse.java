package com.android.fuel.responses;

import com.android.fuel.beans.Station;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/17/2017.
 */

public class GetStationsResponse {

    private ArrayList<Station> items;

    public ArrayList<Station> getItems() {
        return items;
    }

    public void setItems(ArrayList<Station> items) {
        this.items = items;
    }
}
