package com.android.fuel.responses;

/**
 * Created by beshoy adel on 12/17/2017.
 */

public class AttendationConfirmationResponse {

    private String id;

    private String gasFillId;

    private int liters;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGasFillId() {
        return gasFillId;
    }

    public void setGasFillId(String gasFillId) {
        this.gasFillId = gasFillId;
    }

    public int getLiters() {
        return liters;
    }

    public void setLiters(int liters) {
        this.liters = liters;
    }
}
