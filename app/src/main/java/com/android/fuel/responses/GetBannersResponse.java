package com.android.fuel.responses;

import com.android.fuel.beans.Banner;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/17/2017.
 */

public class GetBannersResponse {

    private ArrayList<Banner> items;

    public ArrayList<Banner> getItems() {
        return items;
    }

    public void setItems(ArrayList<Banner> items) {
        this.items = items;
    }
}
