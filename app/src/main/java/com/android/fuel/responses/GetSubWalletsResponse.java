package com.android.fuel.responses;

import com.android.fuel.beans.SubWallet;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/23/2017.
 */

public class GetSubWalletsResponse {

    private ArrayList<SubWallet> items;

    public ArrayList<SubWallet> getItems() {
        return items;
    }

    public void setItems(ArrayList<SubWallet> items) {
        this.items = items;
    }
}
