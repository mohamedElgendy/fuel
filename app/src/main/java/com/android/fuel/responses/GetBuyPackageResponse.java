package com.android.fuel.responses;

import com.android.fuel.beans.WalletTransaction;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/18/2017.
 */

public class GetBuyPackageResponse {

    private ArrayList<WalletTransaction> res;

    public ArrayList<WalletTransaction> getRes() {
        return res;
    }

    public void setRes(ArrayList<WalletTransaction> res) {
        this.res = res;
    }
}
