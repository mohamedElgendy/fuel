package com.android.fuel.responses;

/**
 * Created by beshoy adel on 11/9/2017.
 */

public class GeneralResponse {

    private String id;

    private String userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
