package com.android.fuel.responses;

/**
 * Created by beshoy adel on 12/23/2017.
 */

public class InviteToSubWalletResponse {

    private String id;

    private String driverId;

    private boolean hasChargePermission;

    private boolean hasGasfillPermission;


    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public boolean isHasChargePermission() {
        return hasChargePermission;
    }

    public void setHasChargePermission(boolean hasChargePermission) {
        this.hasChargePermission = hasChargePermission;
    }

    public boolean isHasGasfillPermission() {
        return hasGasfillPermission;
    }

    public void setHasGasfillPermission(boolean hasGasfillPermission) {
        this.hasGasfillPermission = hasGasfillPermission;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
