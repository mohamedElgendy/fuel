package com.android.fuel.responses;

import com.android.fuel.beans.DriverInvoice;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/19/2017.
 */

public class GetInvoicesResponse {

    private ArrayList<DriverInvoice> items;

    public ArrayList<DriverInvoice> getItems() {
        return items;
    }

    public void setItems(ArrayList<DriverInvoice> items) {
        this.items = items;
    }
}
