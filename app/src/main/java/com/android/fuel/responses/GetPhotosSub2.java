package com.android.fuel.responses;


import com.android.fuel.beans.PhotoFile;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 11/11/2017.
 */

public class GetPhotosSub2 {

    private ArrayList<PhotoFile> file;


    public ArrayList<PhotoFile> getFile() {
        return file;
    }

    public void setFile(ArrayList<PhotoFile> file) {
        this.file = file;
    }
}
