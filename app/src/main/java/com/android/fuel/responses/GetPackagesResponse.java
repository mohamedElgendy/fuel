package com.android.fuel.responses;

import com.android.fuel.beans.ChargePackage;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/18/2017.
 */

public class GetPackagesResponse {

    private ArrayList<ChargePackage> items;

    public ArrayList<ChargePackage> getItems() {
        return items;
    }

    public void setItems(ArrayList<ChargePackage> items) {
        this.items = items;
    }
}
