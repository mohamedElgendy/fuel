package com.android.fuel.responses;

/**
 * Created by beshoy adel on 12/19/2017.
 */

public class GetSubWalletsCount {

    private int max;

    private int current;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}
