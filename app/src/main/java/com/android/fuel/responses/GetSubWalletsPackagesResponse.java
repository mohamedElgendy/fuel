package com.android.fuel.responses;

import com.android.fuel.beans.Station;
import com.android.fuel.beans.SubWalletPackage;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/17/2017.
 */

public class GetSubWalletsPackagesResponse {

    private ArrayList<SubWalletPackage> items;

    public ArrayList<SubWalletPackage> getItems() {
        return items;
    }

    public void setItems(ArrayList<SubWalletPackage> items) {
        this.items = items;
    }
}
