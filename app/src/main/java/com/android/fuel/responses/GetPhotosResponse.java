package com.android.fuel.responses;

/**
 * Created by beshoy adel on 11/11/2017.
 */

public class GetPhotosResponse {

    private GetPhotosSub1 result;


    public GetPhotosSub1 getResult() {
        return result;
    }

    public void setResult(GetPhotosSub1 result) {
        this.result = result;
    }
}
