package com.android.fuel.responses;

import com.android.fuel.beans.SubWalletInvitation;

import java.util.ArrayList;

/**
 * Created by beshoy adel on 12/23/2017.
 */

public class GetSubWalletInvitationsResponse {

    private ArrayList<SubWalletInvitation> items;

    public ArrayList<SubWalletInvitation> getItems() {
        return items;
    }

    public void setItems(ArrayList<SubWalletInvitation> items) {
        this.items = items;
    }
}
