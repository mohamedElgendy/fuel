package com.android.fuel.responses;

/**
 * Created by beshoy adel on 12/21/2017.
 */

public class SubWalletPurchasePackageResponse {

    private String id;
    private String walletId;
    private String walletPackageId;
    private String expiryDate;
    private String createdOn;
    private int usedCount;
    private int maxCount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getWalletPackageId() {
        return walletPackageId;
    }

    public void setWalletPackageId(String walletPackageId) {
        this.walletPackageId = walletPackageId;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getUsedCount() {
        return usedCount;
    }

    public void setUsedCount(int usedCount) {
        this.usedCount = usedCount;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    @Override
    public String toString() {
        return maxCount+" Wallets";
    }
}
