package com.android.fuel.requests;

/**
 * Created by beshoy adel on 11/7/2017.
 */

public class ImageRequest {

    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
