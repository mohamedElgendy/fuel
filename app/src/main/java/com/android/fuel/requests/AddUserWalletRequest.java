package com.android.fuel.requests;

/**
 * Created by beshoy adel on 12/21/2017.
 */

public class AddUserWalletRequest {

    private String name;

    private String type;

    private String parentWalletId;

    private String usedSubWalletPuchaseId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentWalletId() {
        return parentWalletId;
    }

    public void setParentWalletId(String parentWalletId) {
        this.parentWalletId = parentWalletId;
    }

    public String getUsedSubWalletPuchaseId() {
        return usedSubWalletPuchaseId;
    }

    public void setUsedSubWalletPuchaseId(String usedSubWalletPuchaseId) {
        this.usedSubWalletPuchaseId = usedSubWalletPuchaseId;
    }
}
