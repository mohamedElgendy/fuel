package com.android.fuel.requests;

/**
 * Created by beshoy adel on 11/14/2017.
 */

public class VerifyPhoneRequest {

    private String a4RUserId;

    private String countryCode;

    private String phone;

    public String getA4RUserId() {
        return a4RUserId;
    }

    public void setA4RUserId(String a4RUserId) {
        this.a4RUserId = a4RUserId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
