package com.android.fuel.requests;

/**
 * Created by beshoy adel on 12/19/2017.
 */

public class TransferCreditRequest {

    private String toUserId;

    private int balance;

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
