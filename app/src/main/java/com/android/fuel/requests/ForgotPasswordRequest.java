package com.android.fuel.requests;

/**
 * Created by beshoy adel on 11/14/2017.
 */

public class ForgotPasswordRequest {

    private String email;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
