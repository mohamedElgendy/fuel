package com.android.fuel.requests;

import com.android.fuel.beans.UserImage;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beshoy adel on 11/17/2017.
 */

public class EditProfileRequest {

    private String email;

    private String name;

    private String iqama;


    @SerializedName("_image")
    private UserImage image;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIqama() {
        return iqama;
    }

    public void setIqama(String iqama) {
        this.iqama = iqama;
    }

    public UserImage getImage() {
        return image;
    }

    public void setImage(UserImage image) {
        this.image = image;
    }
}
