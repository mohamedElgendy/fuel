package com.android.fuel.requests;

import com.android.fuel.beans.UserImage;

/**
 * Created by beshoy adel on 12/21/2017.
 */

public class AddCarWalletRequest {

    private String name;

    private String type;

    private String fuelType;

    private String carModel;

    private String plateNumber;

    private String parentWalletId;

    private String usedSubWalletPuchaseId;

    private UserImage _image;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentWalletId() {
        return parentWalletId;
    }

    public void setParentWalletId(String parentWalletId) {
        this.parentWalletId = parentWalletId;
    }

    public String getUsedSubWalletPuchaseId() {
        return usedSubWalletPuchaseId;
    }

    public void setUsedSubWalletPuchaseId(String usedSubWalletPuchaseId) {
        this.usedSubWalletPuchaseId = usedSubWalletPuchaseId;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public UserImage get_image() {
        return _image;
    }

    public void set_image(UserImage _image) {
        this._image = _image;
    }
}
