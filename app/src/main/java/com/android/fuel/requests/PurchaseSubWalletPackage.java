package com.android.fuel.requests;

/**
 * Created by beshoy adel on 12/21/2017.
 */

public class PurchaseSubWalletPackage {

    private String walletPackageId;

    public String getWalletPackageId() {
        return walletPackageId;
    }

    public void setWalletPackageId(String walletPackageId) {
        this.walletPackageId = walletPackageId;
    }
}
