package com.android.fuel.authentication.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class ChangePasswordFragment extends Fragment implements RequestListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_old_password)
    EditText mOldPasswordView;

    @BindView(R.id.et_new_password)
    EditText mNewPasswordView;

    @BindView(R.id.et_confirm_new_password)
    EditText mConfirmPasswordView;

    @BindView(R.id.bt_update)
    Button updateButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.change_password));
        }



        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.bt_update)
    void update() {
        attemptLogin();
    }

    private void attemptLogin() {

        // Reset errors.
        mOldPasswordView.setError(null);
        mNewPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String oldPass = mOldPasswordView.getText().toString();
        String newPass = mNewPasswordView.getText().toString();
        String confirmPass = mConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(oldPass)) {
            mOldPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mOldPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(newPass)) {
            mNewPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mNewPasswordView;
            cancel = true;
        }


        if (TextUtils.isEmpty(confirmPass)) {
            mConfirmPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(oldPass) && !isPasswordValid(oldPass) && !isPasswordValid(confirmPass)) {
            mOldPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mOldPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (!TextUtils.isEmpty(newPass) && !isPasswordValid(newPass)) {
            mNewPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mNewPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (!newPass.equals(confirmPass)) {
            mNewPasswordView.setError(getString(R.string.password_not_match));
            focusView = mNewPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callLogin();
        }
    }

    private void callLogin() {
        Utils.hideKeyboard(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);
        /*mServiceFactory = new ServiceFactory();
        mServiceFactory.callPostServiceWithSSOAuthToken(Constants.ID_SSO_LOGIN,this);*/

        Map<String, String> params = new HashMap<>();
        params.put("oldPassword",mOldPasswordView.getText().toString());
        params.put("newPassword",mNewPasswordView.getText().toString());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Drivers/change-password",
                GeneralResponse.class, this,params,Parameters.SERVICE_CHANGE_PASSWORD);

        requestHelper.executeFormUrlEncoded();
    }

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})";


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
        // return password.length() > 5;
    }

    @Override
    public void onSuccess(Object response, String apiName) {

        GeneralResponse generalResponse = (GeneralResponse) response;
        if (response instanceof GeneralResponse)
        {

            if (generalResponse.getId().equals("401"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.error_old_incorrect_password);
            }
            else
            {
                Utils.showToast(getActivity(),R.string.change_password_success);
                getActivity().finish();
            }
        }
    }

    @Override
    public void onFail(String message, String apiName) {
        mProgressBar.setVisibility(View.GONE);
        Utils.showToast(getActivity(),message);
    }

}
