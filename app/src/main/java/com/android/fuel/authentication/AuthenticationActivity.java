package com.android.fuel.authentication;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.android.fuel.R;
import com.android.fuel.application.BaseActivity;
import com.android.fuel.application.FuelConstants;
import com.android.fuel.authentication.fragments.ActivationFragment;
import com.android.fuel.authentication.fragments.managePhoneFragment;
import com.android.fuel.authentication.fragments.ChangePasswordFragment;
import com.android.fuel.authentication.fragments.EditProfileFragment;
import com.android.fuel.authentication.fragments.ForgetPasswordFragment;
import com.android.fuel.authentication.fragments.LoginFragment;
import com.android.fuel.authentication.fragments.SignUpFragment;
import com.android.fuel.main.fragments.fillfuel.FillFuelFragment;
import com.android.fuel.main.fragments.fillfuel.FillOrderResultFragment;
import com.android.fuel.main.fragments.findstation.FindStationDetailsFragment;
import com.android.fuel.main.fragments.findstation.StationInfoFragment;
import com.android.fuel.main.fragments.profile.SettingsFragment;

import com.android.fuel.main.fragments.wallets.buysubwallets.BuySubWalletsFragment;
import com.android.fuel.main.fragments.wallets.convertpoints.ConvertPointsFragment;
import com.android.fuel.main.fragments.wallets.rechargewallet.PackageDetailsFragment;
import com.android.fuel.main.fragments.wallets.rechargewallet.RechargePackageListFragment;
import com.android.fuel.main.fragments.wallets.rechargewallet.RechargeWalletFinishFragment;
import com.android.fuel.main.fragments.wallets.subwallet.AddSubWalletFragment;
import com.android.fuel.main.fragments.wallets.subwallet.InviteToSubWalletFragment;
import com.android.fuel.main.fragments.wallets.subwallet.SubWalletFragment;
import com.android.fuel.main.fragments.wallets.subwallet.SubWalletsListFragment;
import com.android.fuel.main.fragments.wallets.transfercredit.TransferCreditDetailsFragment;
import com.android.fuel.main.fragments.wallets.transfercredit.TransferCreditFragment;
import com.android.fuel.main.fragments.wallets.wallettransactions.TransactionsFragment;
import com.android.fuel.utils.FragmentUtils;

import java.util.List;

import butterknife.ButterKnife;

public class AuthenticationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        ButterKnife.bind(this);

        String mTag = getIntent().getStringExtra("tag");
        switch (mTag)
        {
            case FuelConstants.LOGIN_FRAG_TAG:
                //show login fragment
                FragmentUtils.replaceFragment(this, new LoginFragment(), R.id.authentication_container,
                        false, FuelConstants.LOGIN_FRAG_TAG);
                break;
            case FuelConstants.SIGN_UP_FRAG_TAG:
                //show sign up fragment
                FragmentUtils.replaceFragment(this, new SignUpFragment(), R.id.authentication_container,
                      false, FuelConstants.SIGN_UP_FRAG_TAG);
                break;
            case FuelConstants.CHANGE_PASSWORD_FRAG_TAG:
                //show change password fragment
                FragmentUtils.replaceFragment(this, new ChangePasswordFragment(), R.id.authentication_container,
                     false, FuelConstants.CHANGE_PASSWORD_FRAG_TAG);
                break;
            case FuelConstants.EDIT_PROFILE_FRAG_TAG:
                //show edit profile fragment
                FragmentUtils.replaceFragment(this, new EditProfileFragment(), R.id.authentication_container,
                      false, FuelConstants.EDIT_PROFILE_FRAG_TAG);
                break;
            case FuelConstants.FORGET_PASSWORD_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new ForgetPasswordFragment(), R.id.authentication_container,
                      false, FuelConstants.FORGET_PASSWORD_FRAG_TAG);
                break;
            case FuelConstants.ACTIVATION_FRAG_TAG:
                //show Activation fragment
                Fragment fragment = new ActivationFragment();
                Bundle args = new Bundle();
                args.putString("userId",getIntent().getStringExtra("userId"));
                fragment.setArguments(args);
                FragmentUtils.replaceFragment(this, fragment, R.id.authentication_container,
                      false, FuelConstants.ACTIVATION_FRAG_TAG);
                break;
            case FuelConstants.ADD_PHONE_FRAG_TAG:
                //show Add Phone fragment
                Fragment fragment2 = new managePhoneFragment();
                Bundle args2 = new Bundle();
                args2.putString("userId",getIntent().getStringExtra("userId"));
                fragment2.setArguments(args2);
                FragmentUtils.replaceFragment(this, fragment2, R.id.authentication_container,
                        false, FuelConstants.ADD_PHONE_FRAG_TAG);
                break;
            case FuelConstants.SETTINGS_FRAG_TAG:
                //show forget Settings fragment
                FragmentUtils.replaceFragment(this, new SettingsFragment(), R.id.authentication_container,
                        false, FuelConstants.SETTINGS_FRAG_TAG);
                break;
            case FuelConstants.FILL_ORDER_RESULT_FRAG_TAG:
                //show Add Phone fragment
                Fragment fragment3 = new FillOrderResultFragment();
                Bundle args3 = new Bundle();
                args3.putString("item",getIntent().getStringExtra("item"));
                fragment3.setArguments(args3);
                FragmentUtils.replaceFragment(this, fragment3, R.id.authentication_container,
                        false, FuelConstants.ADD_PHONE_FRAG_TAG);
                break;
            case FuelConstants.STATION_LISTING_FRAG_TAG:
                //show forget Settings fragment
                FragmentUtils.replaceFragment(this, new FindStationDetailsFragment(), R.id.authentication_container,
                        false, FuelConstants.STATION_LISTING_FRAG_TAG);
                break;
            case FuelConstants.STATION_INFO_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new StationInfoFragment(), R.id.authentication_container,
                        false, FuelConstants.STATION_INFO_FRAG_TAG);
                break;
            case FuelConstants.STATION_FILL_FUEL_FRAG_TAG:
                //show Add Phone fragment
                Fragment fragment4 = new FillFuelFragment();
                Bundle args4 = new Bundle();
                args4.putString("stationId",getIntent().getStringExtra("stationId"));
                fragment4.setArguments(args4);
                FragmentUtils.replaceFragment(this, fragment4, R.id.authentication_container,
                        false, FuelConstants.STATION_FILL_FUEL_FRAG_TAG);
                break;
            case FuelConstants.WALLET_RECHARGE_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new RechargePackageListFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_RECHARGE_FRAG_TAG);
                break;
            case FuelConstants.WALLET_PACKAGE_DETAILS_FRAG_TAG:
                //show forget password fragment
                Fragment fragment5 = new PackageDetailsFragment();
                Bundle args5 = new Bundle();
                args5.putString("package",getIntent().getStringExtra("package"));
                fragment5.setArguments(args5);
                FragmentUtils.replaceFragment(this, fragment5, R.id.authentication_container,
                        false, FuelConstants.WALLET_PACKAGE_DETAILS_FRAG_TAG);
                break;
            case FuelConstants.WALLET_RECHARGE_FINISH_FRAG_TAG:
                //show forget password fragment
                Fragment fragment6 = new RechargeWalletFinishFragment();
                Bundle args6 = new Bundle();
                args6.putInt("totalPoints",getIntent().getIntExtra("totalPoints",0));
                args6.putFloat("totalBalance",getIntent().getFloatExtra("totalBalance",0));
                fragment6.setArguments(args6);
                FragmentUtils.replaceFragment(this, fragment6, R.id.authentication_container,
                        false, FuelConstants.WALLET_RECHARGE_FINISH_FRAG_TAG);
                break;
            case FuelConstants.WALLET_TRANSACTIONS_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new TransactionsFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_TRANSACTIONS_FRAG_TAG);
                break;
            case FuelConstants.WALLET_CREDIT_TRANSFER_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new TransferCreditFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_CREDIT_TRANSFER_FRAG_TAG);
                break;
            case FuelConstants.WALLET_CREDIT_TRANSFER_DETAILS_FRAG_TAG:
                //show forget password fragment
                Fragment fragment7 = new TransferCreditDetailsFragment();
                Bundle args7 = new Bundle();
                args7.putString("user",getIntent().getStringExtra("user"));
                args7.putInt("transferBalance",getIntent().getIntExtra("transferBalance",0));
                fragment7.setArguments(args7);
                FragmentUtils.replaceFragment(this, fragment7, R.id.authentication_container,
                        false, FuelConstants.WALLET_CREDIT_TRANSFER_DETAILS_FRAG_TAG);
                break;
            case FuelConstants.WALLET_CONVERT_POINTS_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new ConvertPointsFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_CONVERT_POINTS_FRAG_TAG);
                break;
            case FuelConstants.WALLET_CONVERT_POINTS_FINISH_FRAG_TAG:
                //show forget password fragment
                Fragment fragment8 = new RechargeWalletFinishFragment();
                Bundle args8 = new Bundle();
                args8.putInt("totalPoints",getIntent().getIntExtra("totalPoints",0));
                args8.putFloat("totalBalance",getIntent().getFloatExtra("totalBalance",0));
                args8.putBoolean("isFromConvertPoints",true);
                fragment8.setArguments(args8);
                FragmentUtils.replaceFragment(this, fragment8, R.id.authentication_container,
                        false, FuelConstants.WALLET_CONVERT_POINTS_FINISH_FRAG_TAG);
                break;
            case FuelConstants.WALLET_BUY_SUBWALLET_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new BuySubWalletsFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_BUY_SUBWALLET_FRAG_TAG);
                break;
            case FuelConstants.WALLET_ADD_SUBWALLET_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new AddSubWalletFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_ADD_SUBWALLET_FRAG_TAG);
                break;
            case FuelConstants.WALLET_SUBWALLET_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new SubWalletFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_SUBWALLET_FRAG_TAG);
                break;
            case FuelConstants.WALLET_SUBWALLET_INVITATIONS_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new SubWalletsListFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_SUBWALLET_INVITATIONS_FRAG_TAG);
                break;
            case FuelConstants.WALLET_INVITE_SUBWALLET_FRAG_TAG:
                //show forget password fragment
                FragmentUtils.replaceFragment(this, new InviteToSubWalletFragment(), R.id.authentication_container,
                        false, FuelConstants.WALLET_INVITE_SUBWALLET_FRAG_TAG);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
