package com.android.fuel.authentication.fragments;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.beans.Phone;
import com.android.fuel.beans.PhotoFile;
import com.android.fuel.beans.User;
import com.android.fuel.beans.UserImage;
import com.android.fuel.requests.ImageRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetPhotosResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.SavePrefs;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Mohamed Elgendy.
 */

public class SignUpFragment extends Fragment implements RequestListener{

    @BindView(R.id.iv_profile_thumbnail)
    ImageView profileThumbnailImageView;

    @BindView(R.id.et_name)
    EditText mUserNameView;

    /*@BindView(R.id.et_country_code)
    EditText mCountryCodeEditText;

    @BindView(R.id.et_mobile)
    EditText mMobileEditText;*/

    @BindView(R.id.et_email)
    EditText mEmailView;

    @BindView(R.id.et_id)
    EditText idEditText;

    @BindView(R.id.et_password)
    EditText mPasswordView;

    @BindView(R.id.et_retype_password)
    EditText mConfirmedPasswordView;

    @BindView(R.id.bt_sign_up)
    Button signUpButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;
    private Uri outputFileUri;
    private String mCurrentPath=null;
    private ArrayList<PhotoFile> photosUrls = new ArrayList<>();//Uploaded photos

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, mRootView);

        return mRootView;
    }

    @OnClick(R.id.iv_profile_thumbnail)
    void profileThumbnail() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }

    @OnClick(R.id.bt_sign_up)
    void signUp() {
        attemptLogin();
    }


    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mUserNameView.setError(null);
        //mMobileEditText.setError(null);
        //mCountryCodeEditText.setError(null);
        idEditText.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String userName = mUserNameView.getText().toString();
        String iqama = idEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }

        /*if (TextUtils.isEmpty(mCountryCodeEditText.getText().toString())) {
            mCountryCodeEditText.setError(getString(R.string.error_field_required));
            focusView = mCountryCodeEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(mMobileEditText.getText().toString())) {
            mMobileEditText.setError(getString(R.string.error_field_required));
            focusView = mMobileEditText;
            cancel = true;
        }*/

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(iqama)) {
            idEditText.setError(getString(R.string.error_field_required));
            focusView = idEditText;
            cancel = true;
        }

        if (!TextUtils.isEmpty(idEditText.getText().toString()) && idEditText.getText().toString().length()!=10) {
            idEditText.setError(getString(R.string.iqama_validation));
            focusView = idEditText;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (!password.equals(mConfirmedPasswordView.getText().toString())) {
            mPasswordView.setError(getString(R.string.password_not_match));
            focusView = mPasswordView;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            uploadImage();
        }
    }


    private void uploadImage()
    {
        mProgressBar.setVisibility(View.VISIBLE);
        if (mCurrentPath!=null && !TextUtils.isEmpty(mCurrentPath))
        {

            Map<String, File> queryParams = new HashMap<>();
            queryParams.put("file",new File(mCurrentPath));

            if (queryParams.size()>0)
            {
                RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                        Parameters.BASE_URL, Parameters.UPLOAD_PHOTOS,
                        GetPhotosResponse.class, new RequestListener<Object>() {

                    @Override
                    public void onSuccess(Object response, String apiName) {

                        GetPhotosResponse getPhotosResponse = (GetPhotosResponse) response;
                        if (getPhotosResponse!=null && getPhotosResponse.getResult()!=null
                                &&getPhotosResponse.getResult().getFiles() !=null
                                &&getPhotosResponse.getResult().getFiles().getFile() !=null)
                        {
                            photosUrls.addAll(getPhotosResponse.getResult().getFiles().getFile());
                            callRegister();
                        }

                    }

                    @Override
                    public void onFail(String message, String apiName) {

                        mProgressBar.setVisibility(ProgressBar.GONE);
                        mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                                uploadImage();
                            }
                        });
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
                    }
                }, Parameters.SERVICE_UPLOAD_PHOTO, queryParams);

                requestHelper.executeMultiPart();
            }
            else
                callRegister();

        }
        else
            callRegister();
    }

    private void callRegister() {
        Utils.hideKeyboard(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);
        /*mServiceFactory = new ServiceFactory();
        mServiceFactory.callPostServiceWithSSOAuthToken(Constants.ID_SSO_LOGIN,this);*/

        User loginRequest = new User();
        loginRequest.setEmail(mEmailView.getText().toString());
        loginRequest.setPassword(mPasswordView.getText().toString());
        loginRequest.setUsername(mUserNameView.getText().toString());
        loginRequest.setName(mUserNameView.getText().toString());
        loginRequest.setIqama(idEditText.getText().toString());
        loginRequest.setCountryId("5a6b433ffc1a420028718bf4");
        loginRequest.setPrefCurrencyId("5a6b42ac5eebec001074c701");

        Phone phone = new Phone();
        //phone.setCountryCode(mCountryCodeEditText.getText().toString());
        //phone.setPhone(mMobileEditText.getText().toString());
        loginRequest.setPhone(phone);

        ArrayList<UserImage> images = new ArrayList<>();
        for (int i=0;i<photosUrls.size();i++)
        {
            UserImage image = new UserImage();
            image.setUrl(Parameters.PHOTOS_BASE+photosUrls.get(i).getName());
            images.add(image);
        }
        if (images.size()>0)
            loginRequest.setImage(images.get(0));

        /*SavePrefs<UserPreferences> userPreferencesSavePrefs = new SavePrefs<>(this,UserPreferences.class);
        UserPreferences userPreferences = userPreferencesSavePrefs.load();
        if (userPreferences!=null)
            loginRequest.setCountryId(userPreferences.getCountry().getId());*/

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Drivers",
                User.class, this,Parameters.SERVICE_REGISTER);

        requestHelper.executeFromRawJson(new Gson().toJson(loginRequest));
    }

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})";


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
        // return password.length() > 5;
    }

    @Override
    public void onSuccess(Object response, String apiName) {
        mProgressBar.setVisibility(View.GONE);

        if (response instanceof GeneralResponse)
        {
            GeneralResponse generalResponse = (GeneralResponse) response;
            if (generalResponse.getId().equals("401"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.name_exist);
            }
            else  if (generalResponse.getId().equals("402"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.email_exist);
            }
            else  if (generalResponse.getId().equals("403"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.check_login_info);
            }
        }
        else
        {
            User user = (User)response;
            showAlertDialog(user.getId());
        }
    }

    private void showAlertDialog(final String userId) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(R.string.account_created)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        //AppManager.openVerifyPhone(RegisterActivity.this,userId);
                        getActivity().finish();
                    }
                })
                .setCancelable(true)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        getActivity().finish();
                    }
                })
                .show();
    }

    @Override
    public void onFail(String message, String apiName) {
        mProgressBar.setVisibility(View.GONE);
        Utils.showToast(getActivity(),message);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openPickerIntent();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void openPickerIntent() {

// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name) + File.separator);
        root.mkdirs();
        final String fname = "profile"+ Calendar.getInstance().getTimeInMillis()+".png";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getActivity().getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, 1200);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri selectedImageUri=null;
            if (requestCode == 1200) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }
                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }

                if(selectedImageUri!=null)
                {
                    mCurrentPath = getRealPathFromURI(getActivity(),selectedImageUri);

                    Picasso.with(getActivity())
                            .load(selectedImageUri)
                            .fit()
                            .into(profileThumbnailImageView);

                }
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = getActivity().managedQuery(contentUri, projection, null, null,
                null);

        if (cursor != null)
        {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return contentUri.getPath();
    }

}