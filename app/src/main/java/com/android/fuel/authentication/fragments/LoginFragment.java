package com.android.fuel.authentication.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.beans.SocialResponse;
import com.android.fuel.beans.User;
import com.android.fuel.requests.LoginRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class LoginFragment extends Fragment {

    @BindView(R.id.et_user_name)
    EditText mEmailView;

    @BindView(R.id.et_password)
    EditText mPasswordView;

    @BindView(R.id.tv_forget_password)
    TextView forgetPasswordTextView;

    @BindView(R.id.bt_login)
    Button loginButton;

    @BindView(R.id.iv_facebook)
    ImageView facebookImageView;

    @BindView(R.id.iv_twitter)
    ImageView twitterImageView;

    @BindView(R.id.iv_gmail)
    ImageView gmailImageView;

    @BindView(R.id.bt_sign_up)
    Button signUpButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    private GoogleApiClient mGoogleApiClient;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, rootView);

        getHashCode();
        initFacebookLogin();


        return rootView;
    }

    @OnClick(R.id.tv_forget_password)
    void forgetPassword() {
        FuelManager.openForgotPassword(getActivity());
    }

    @OnClick(R.id.bt_login)
    void login() {
        attemptLogin();
    }

    @OnClick(R.id.iv_facebook)
    void facebookLogin() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(
                "public_profile", "email"));//Login Now
    }

    @OnClick(R.id.iv_twitter)
    void twitterLogin() {
        Toast.makeText(getActivity(), "Twitter Button Clicked !", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.iv_gmail)
    void gmailLogin() {
        mProgressBar.setVisibility(View.VISIBLE);
       // Intent signInIntent = mGoogleSignInClient.getSignInIntent();
       // startActivityForResult(signInIntent, RC_SIGN_IN);
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, OUR_REQUEST_CODE);
    }

    @OnClick(R.id.bt_sign_up)
    void signUp() {
        FuelManager.openRegister(getActivity());
    }


    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callLogin();
        }
    }


    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})";


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
       // return password.length() > 5;
    }

    private void callLogin() {
        Utils.hideKeyboard(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);
        /*mServiceFactory = new ServiceFactory();
        mServiceFactory.callPostServiceWithSSOAuthToken(Constants.ID_SSO_LOGIN,this);*/

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(mEmailView.getText().toString());
        loginRequest.setPassword(mPasswordView.getText().toString());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/login?",
                User.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                if (response instanceof GeneralResponse)
                {
                    GeneralResponse generalResponse = (GeneralResponse) response;
                    if (generalResponse.getId().equals("401"))//Error
                    {
                        mProgressBar.setVisibility(View.GONE);
                        Utils.showToast(getActivity(),R.string.check_login_info);
                    }
                }
                else if (response instanceof User)
                {
                    User userResponse = (User) response;
                    AppConstants.setAccessToken(getActivity(),userResponse.getId());//To save token to request the details
                    getUserDetails(userResponse.getUserId(),AppConstants.LOGIN_TYPE_ACCOUNT);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),message);
            }
        }, Parameters.SERVICE_LOGIN);

        requestHelper.executeFromRawJson(new Gson().toJson(loginRequest));
    }

    private void getUserDetails(final String userId, final int loginType) {

        String filter = "?";
        if (loginType==AppConstants.LOGIN_TYPE_ACCOUNT)
        {
            filter = "?filter="+"{\"include\":\"phone\"}";
        }
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/" + userId+filter,
                User.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof User)
                {
                    User user = (User) response;
                    user.setUserId(user.getId());
                    if(!user.isEmailVerified() && loginType==AppConstants.LOGIN_TYPE_ACCOUNT)
                    {
                        showAlertForVerifyEmailDialog(user.getUserId());
                    }
                    else if (!user.isPhoneVerified() && loginType==AppConstants.LOGIN_TYPE_ACCOUNT)
                    {
                        if (user.getPhone()!=null)
                            FuelManager.openVerifyPhone(getActivity(),user.getUserId());
                        else
                            FuelManager.openAddOrEditPhone(getActivity(),user.getId());
                    }
                    else
                    {
                        user.setLoginType(loginType);
                        AppConstants.setUser(getActivity(),user);
                        Utils.showToast(getActivity(),R.string.login_success);
                        FuelManager.openMain(getActivity());
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);

            }
        }, -1);
        requestHelper.executeGet();
    }

    private void showAlertForVerifyEmailDialog(final String id) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder.setTitle(R.string.verify_email_title)
                .setMessage(R.string.verify_email_msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        sendVerificationMail(id);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                    }
                })
                .setCancelable(true)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                })
                .show();
    }

    private void sendVerificationMail(final String id)
    {
        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/" + id + "/verify",
                GeneralResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                Utils.showToast(getActivity(),R.string.check_email_inbox);
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, Parameters.SERVICE_EMAIL_VERIFY);

        requestHelper.executeFormUrlEncoded();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestIdToken("1071094093764-oi04fh86k96dcb9tpngp6vnbe72djcp7.apps.googleusercontent.com")
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);*/

        // First we need to configure the Google Sign In API to ensure we are retrieving
        // the server authentication code as well as authenticating the client locally.

        String serverClientId = getString(R.string.google_server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .requestIdToken(serverClientId)
                .build();

        // We pass through three "this" arguments to the builder, specifying the:
        // 1. Context
        // 2. Object to use for resolving connection errors
        // 3. Object to call onConnectionFailed on
        // We also add the Google Sign in API we previously created.
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity() /* Context */)
                .enableAutoManage(getActivity() /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


    }


    private static final String EMAIL = "email";
    private static final String PUBLIC_PROFILE = "public_profile";
    private CallbackManager callbackManager;
    private String mUserId,mUserName,mUserPicture,mUserEmail,mUserGender,mFacebookToken;
    private static final int OUR_REQUEST_CODE = 49404;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        /*if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }*/
        if (requestCode == OUR_REQUEST_CODE) {
            // Hide the progress dialog if its showing.
            //mConnectionProgressDialog.dismiss();

            // Resolve the intent into a GoogleSignInResult we can process.
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            callLoginWithGoogle(result.getSignInAccount());
        }
        else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initFacebookLogin() {

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // App code
                mProgressBar.setVisibility(View.VISIBLE);
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    mFacebookToken = loginResult.getAccessToken().getToken();
                                    mUserId = object.getString("id");
                                    mUserEmail = object.getString("email");
                                    mUserName = object.getString("name");
                                    mUserPicture = "https://graph.facebook.com/" + mUserId + "/picture?type=normal";
                                    mUserGender = object.getString("gender");
                                    callLoginWithFacebook();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,picture");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
                mProgressBar.setVisibility(View.GONE);
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                mProgressBar.setVisibility(View.GONE);
                Log.v("LoginActivity", exception.getCause().toString());
            }
        });

    }

    private void callLoginWithFacebook() {
        mProgressBar.setVisibility(View.VISIBLE);
        LoginRequest loginRequest = new LoginRequest();
       // loginRequest.setEmail(mEmailView.getText().toString());
       // loginRequest.setPassword(mPasswordView.getText().toString());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/auth/facebook/callback?access_token="+mFacebookToken,
                SocialResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                 if (response instanceof SocialResponse)
                {
                    SocialResponse userResponse = (SocialResponse) response;
                    AppConstants.setAccessToken(getActivity(),userResponse.getAccess_token());//To save token to request the details
                    getUserDetails(userResponse.getUserId(),AppConstants.LOGIN_TYPE_FACEBOOK);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),message);
            }
        }, Parameters.SERVICE_LOGIN);

        requestHelper.executeGet();

    }

    public void getHashCode() {
        try {
            Log.d("KeyHash:", getActivity().getPackageName());
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleSignInClient;


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly. We can try and retrieve an
            // authentication code.
            Log.d("", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
           // handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
          //  final ProgressDialog progressDialog = new ProgressDialog(this);
           // progressDialog.setMessage("Checking sign in state...");
            //progressDialog.show();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
              //      progressDialog.dismiss();
                   // handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    // [START handleSignInResult]
   /* private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            callLoginWithGoogle(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("", "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
        }
    }*/
    // [END handleSignInResult]

    private void callLoginWithGoogle(GoogleSignInAccount account) {
        LoginRequest loginRequest = new LoginRequest();
        // loginRequest.setEmail(mEmailView.getText().toString());
        // loginRequest.setPassword(mPasswordView.getText().toString());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/auth/google/callback?access_token="+account.getIdToken(),
                SocialResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

               if (response instanceof SocialResponse)
                {
                    SocialResponse userResponse = (SocialResponse) response;
                    AppConstants.setAccessToken(getActivity(),userResponse.getAccess_token());//To save token to request the details
                    getUserDetails(userResponse.getUserId(),AppConstants.LOGIN_TYPE_GOOGLE);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),message);
            }
        }, Parameters.SERVICE_LOGIN);

        requestHelper.executeGet();

    }
}