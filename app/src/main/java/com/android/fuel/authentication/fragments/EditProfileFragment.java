package com.android.fuel.authentication.fragments;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.beans.PhotoFile;
import com.android.fuel.beans.User;
import com.android.fuel.beans.UserImage;
import com.android.fuel.requests.EditProfileRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetPhotosResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Mohamed Elgendy.
 */

public class EditProfileFragment extends Fragment implements RequestListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_first_name)
    EditText mUserNameView;

    @BindView(R.id.et_email)
    EditText mEmailView;

    @BindView(R.id.et_code)
    EditText codeEditText;

    @BindView(R.id.bt_update)
    Button updateButton;
    private User mUser;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.iv_profile_thumbnail)
    ImageView profileThumbnailImageView;

    private Uri outputFileUri;
    private String mCurrentPath=null;
    private ArrayList<PhotoFile> photosUrls = new ArrayList<>();//Uploaded photos

    View rootView;
    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, rootView);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.edit_profile));
        }

        mUser = AppConstants.getUser(getActivity());
        CircleImageView userPicImageView = (CircleImageView)rootView.findViewById(R.id.iv_profile_thumbnail);
        if (
                mUser!=null && mUser.getImage()!=null && mUser.getImage().getUrl()!=null &&
                        !Utils.isEmptyString(AppConstants.getUser(getActivity()).getImage().getUrl())
                        && Utils.hasConnection(getActivity()))
        {
            Picasso.with(getContext()).load(mUser.getImage().getUrl()).placeholder(R.drawable.avatar_icon).centerCrop().fit().into(userPicImageView);
        }

        mEmailView.setText(mUser.getEmail());
        mUserNameView.setText(mUser.getName());
        codeEditText.setText(mUser.getIqama());
        return rootView;
    }

    @OnClick(R.id.bt_update)
    void update() {
        attemptLogin();
    }

    @OnClick(R.id.iv_profile_thumbnail)
    void profileThumbnail() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }

    private void uploadImage()
    {
        mProgressBar.setVisibility(View.VISIBLE);
        if (mCurrentPath!=null && !TextUtils.isEmpty(mCurrentPath))
        {

            Map<String, File> queryParams = new HashMap<>();
            queryParams.put("file",new File(mCurrentPath));

            if (queryParams.size()>0)
            {
                RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                        Parameters.BASE_URL, Parameters.UPLOAD_PHOTOS,
                        GetPhotosResponse.class, new RequestListener<Object>() {

                    @Override
                    public void onSuccess(Object response, String apiName) {

                        GetPhotosResponse getPhotosResponse = (GetPhotosResponse) response;
                        if (getPhotosResponse!=null && getPhotosResponse.getResult()!=null
                                &&getPhotosResponse.getResult().getFiles() !=null
                                &&getPhotosResponse.getResult().getFiles().getFile() !=null)
                        {
                            photosUrls.addAll(getPhotosResponse.getResult().getFiles().getFile());
                            callRegister();
                        }

                    }

                    @Override
                    public void onFail(String message, String apiName) {

                        mProgressBar.setVisibility(ProgressBar.GONE);
                        rootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                                uploadImage();
                            }
                        });
                        rootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
                    }
                }, Parameters.SERVICE_UPLOAD_PHOTO, queryParams);

                requestHelper.executeMultiPart();
            }
            else
                callRegister();

        }
        else
            callRegister();
    }

    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mUserNameView.setError(null);
        codeEditText.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String userName = mUserNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(codeEditText.getText().toString())) {
            codeEditText.setError(getString(R.string.error_field_required));
            focusView = codeEditText;
            cancel = true;
        }

        if (!TextUtils.isEmpty(codeEditText.getText().toString()) && codeEditText.getText().toString().length()!=10) {
            codeEditText.setError(getString(R.string.iqama_validation));
            focusView = codeEditText;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            uploadImage();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void callRegister() {
        Utils.hideKeyboard(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);

        EditProfileRequest editProfileRequest = new EditProfileRequest();

        Map<String, String> params = new HashMap<>();
        if (!mUser.getName().equals(mUserNameView.getText().toString()))
            editProfileRequest.setName(mUserNameView.getText().toString());
        else
            editProfileRequest.setName(mUser.getName());

        if (!mUser.getEmail().equals(mEmailView.getText().toString()))
            editProfileRequest.setEmail(mEmailView.getText().toString());
        else
            editProfileRequest.setEmail(mUser.getEmail());

        if (!mUser.getIqama().equals(codeEditText.getText().toString()))
            editProfileRequest.setIqama(codeEditText.getText().toString());
        else
            editProfileRequest.setIqama(mUser.getIqama());

        ArrayList<UserImage> images = new ArrayList<>();
        for (int i=0;i<photosUrls.size();i++)
        {
            UserImage image = new UserImage();
            image.setUrl(Parameters.PHOTOS_BASE+photosUrls.get(i).getName());
            images.add(image);
        }
        if (images.size()>0)
            editProfileRequest.setImage(images.get(0));

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+mUser.getId(),
                User.class, this, params,Parameters.SERVICE_EDIT_PROFILE);

        requestHelper.executePatchFromRawJson(new Gson().toJson(editProfileRequest));
    }

    @Override
    public void onSuccess(Object response, String apiName) {
        mProgressBar.setVisibility(View.GONE);

        if (response instanceof GeneralResponse)
        {
            GeneralResponse generalResponse = (GeneralResponse) response;
            if (generalResponse.getId().equals("401"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.name_exist);
            }
            else  if (generalResponse.getId().equals("402"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.email_exist);
            }
            else  if (generalResponse.getId().equals("403"))//Error
            {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),R.string.check_login_info);
            }
        }
        else
        {
            User user = (User)response;
            mUser.setEmail(user.getEmail());
            mUser.setImage(user.getImage());
            mUser.setName(user.getName());
            mUser.setIqama(user.getIqama());

            AppConstants.setUser(getActivity(),mUser);

            if (!mUser.getEmail().equals(mEmailView.getText().toString()))
                showAlertDialog(user.getId());
            else {
                Utils.showToast(getActivity(), R.string.profile_edited);
                getActivity().finish();
            }
        }
    }

    private void showAlertDialog(final String userId) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(R.string.profile_edited_check_mail)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        //AppManager.openVerifyPhone(RegisterActivity.this,userId);
                        getActivity().finish();
                    }
                })
                .setCancelable(true)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        getActivity().finish();
                    }
                })
                .show();
    }

    @Override
    public void onFail(String message, String apiName) {
        mProgressBar.setVisibility(View.GONE);
        Utils.showToast(getActivity(),message);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openPickerIntent();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void openPickerIntent() {

// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name) + File.separator);
        root.mkdirs();
        final String fname = "profile"+ Calendar.getInstance().getTimeInMillis()+".png";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getActivity().getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, 1200);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri selectedImageUri=null;
            if (requestCode == 1200) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }
                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }

                if(selectedImageUri!=null)
                {
                    mCurrentPath = getRealPathFromURI(getActivity(),selectedImageUri);

                    Picasso.with(getActivity())
                            .load(selectedImageUri)
                            .fit()
                            .into(profileThumbnailImageView);

                }
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = getActivity().managedQuery(contentUri, projection, null, null,
                null);

        if (cursor != null)
        {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return contentUri.getPath();
    }

}
