package com.android.fuel.authentication.fragments;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.fuel.R;
import com.android.fuel.beans.Phone;
import com.android.fuel.beans.PhotoFile;
import com.android.fuel.beans.User;
import com.android.fuel.beans.UserImage;
import com.android.fuel.requests.VerifyPhoneRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetPhotosResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Mohamed Elgendy.
 */

public class managePhoneFragment extends Fragment{

    @BindView(R.id.et_country_code)
    EditText mCountryCodeEditText;

    @BindView(R.id.et_mobile)
    EditText mMobileEditText;

    @BindView(R.id.bt_sign_up)
    Button signUpButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;
    private String userId;

    public managePhoneFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_add_phone, container, false);
        ButterKnife.bind(this, mRootView);

        userId = getArguments().getString("userId");

        User user = AppConstants.getUser(getActivity());
        if (user!=null && user.getPhone()!=null && !TextUtils.isEmpty(user.getPhone().getPhone()))
        {
            mMobileEditText.setText(user.getPhone().getPhone());
            mCountryCodeEditText.setText(user.getPhone().getCountryCode());
        }

        return mRootView;
    }

    @OnClick(R.id.bt_sign_up)
    void signUp() {
        attemptLogin();
    }


    private void attemptLogin() {

        // Reset errors.

        mMobileEditText.setError(null);
        mCountryCodeEditText.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(mCountryCodeEditText.getText().toString())) {
            mCountryCodeEditText.setError(getString(R.string.error_field_required));
            focusView = mCountryCodeEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(mMobileEditText.getText().toString())) {
            mMobileEditText.setError(getString(R.string.error_field_required));
            focusView = mMobileEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            addPhone();
        }
    }


    private void addPhone() {
        Utils.hideKeyboard(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);

        VerifyPhoneRequest verifyPhoneRequest = new VerifyPhoneRequest();
        verifyPhoneRequest.setCountryCode(mCountryCodeEditText.getText().toString());
        verifyPhoneRequest.setPhone(mMobileEditText.getText().toString());
        verifyPhoneRequest.setA4RUserId(userId);

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/" + userId + "/phone",
                GeneralResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {

                GeneralResponse generalResponse = (GeneralResponse) response;
                if (response instanceof GeneralResponse)
                {

                    if (generalResponse.getId().equals("500"))//Error
                    {
                        mProgressBar.setVisibility(View.GONE);
                        Utils.showToast(getActivity(),R.string.phone_exist);
                    }
                    else  if (generalResponse.getId().equals("501"))//Error
                    {
                        mProgressBar.setVisibility(View.GONE);
                        Utils.showToast(getActivity(),R.string.error_invalid_phone);
                    }
                    else
                    {
                        FuelManager.openVerifyPhone(getActivity(),userId);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),message);
            }
        }, Parameters.SERVICE_ADD_PHONE);

        requestHelper.executeFromRawJson(new Gson().toJson(verifyPhoneRequest));
    }
}