package com.android.fuel.authentication.fragments;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.requests.ForgotPasswordRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class ForgetPasswordFragment extends Fragment implements RequestListener {

    @BindView(R.id.et_identity)
    EditText mEmailView;

    @BindView(R.id.bt_send)
    Button sendButton;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    public ForgetPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forget_password, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @OnClick(R.id.bt_send)
    void update() {
        attemptLogin();
    }

    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            callLogin();
        }
    }

    private void callLogin() {
        Utils.hideKeyboard(getActivity());
        mProgressBar.setVisibility(View.VISIBLE);
        /*mServiceFactory = new ServiceFactory();
        mServiceFactory.callPostServiceWithSSOAuthToken(Constants.ID_SSO_LOGIN,this);*/

        ForgotPasswordRequest loginRequest = new ForgotPasswordRequest();
        loginRequest.setEmail(mEmailView.getText().toString());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Drivers/reset",
                GeneralResponse.class, this,Parameters.SERVICE_FOROGOT_PASSWORD);

        requestHelper.executeFromRawJson(new Gson().toJson(loginRequest));
    }

    @Override
    public void onSuccess(Object response, String apiName) {
        mProgressBar.setVisibility(View.GONE);
        GeneralResponse generalResponse = (GeneralResponse) response;
        if (response instanceof GeneralResponse)
        {
            if (generalResponse.getId().equals("500"))//Error
            {
                Utils.showToast(getActivity(),R.string.error_invalid_email);
            }
            else
            {
                showAlertDialog();
            }
        }
    }

    private void showAlertDialog() {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(R.string.check_email_inbox)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        getActivity().finish();
                    }
                })
                .setCancelable(true)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        getActivity().finish();
                    }
                })
                .show();
    }

    @Override
    public void onFail(String message, String apiName) {
        mProgressBar.setVisibility(View.GONE);
        Utils.showToast(getActivity(),message);
    }

}