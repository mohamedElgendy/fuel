package com.android.fuel.authentication.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class ActivationFragment extends Fragment {

    @BindView(R.id.et_pin1)
    EditText pin1EditText;

    @BindView(R.id.et_pin2)
    EditText pin2EditText;

    @BindView(R.id.et_pin3)
    EditText pin3EditText;

    @BindView(R.id.et_pin4)
    EditText pin4EditText;

    @BindView(R.id.et_pin5)
    EditText pin5EditText;

    @BindView(R.id.et_pin6)
    EditText pin6EditText;

    @BindView(R.id.bt_activate)
    Button activateButton;

    @BindView(R.id.tv_resend)
    TextView resentTextView;

    private String userId;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    public ActivationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activiation, container, false);
        ButterKnife.bind(this, rootView);


        pin1EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                {
                    pin2EditText.setFocusableInTouchMode(true);
                    pin2EditText.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pin2EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                {
                    pin3EditText.setFocusableInTouchMode(true);
                    pin3EditText.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pin3EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                {
                    pin4EditText.setFocusableInTouchMode(true);
                    pin4EditText.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pin4EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                {
                    pin5EditText.setFocusableInTouchMode(true);
                    pin5EditText.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        pin5EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                {
                    pin6EditText.setFocusableInTouchMode(true);
                    pin6EditText.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        userId = getArguments().getString("userId");

        return rootView;
    }

    @OnClick(R.id.bt_activate)
    void activate() {
        sendCode();
    }

    @OnClick(R.id.tv_resend)
    void resend() {
        sendVerificationCode();
    }

    private void sendCode() {

        String code = "";
        if (TextUtils.isEmpty(pin1EditText.getText().toString()) || TextUtils.isEmpty(pin1EditText.getText().toString())
                || TextUtils.isEmpty(pin1EditText.getText().toString()) || TextUtils.isEmpty(pin1EditText.getText().toString())
                || TextUtils.isEmpty(pin5EditText.getText().toString()) || TextUtils.isEmpty(pin6EditText.getText().toString()))
        {
            Utils.showToast(getActivity(),R.string.enter_code);
            return;
        }

        code = pin1EditText.getText().toString()+pin2EditText.getText().toString()+
                pin3EditText.getText().toString()+pin4EditText.getText().toString() +
                pin5EditText.getText().toString()+pin6EditText.getText().toString();

        Map<String, String> params = new HashMap<>();
        params.put("id",userId);
        params.put("passCode",code+"");
        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+userId+"/validatePasscode",
                GeneralResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                GeneralResponse generalResponse = (GeneralResponse) response;
                if (response instanceof GeneralResponse)
                {
                    mProgressBar.setVisibility(View.GONE);
                    if (generalResponse.getId().equals("500"))//Error
                    {
                        Utils.showToast(getActivity(),R.string.error_incorrect_code);
                    }
                    else
                    {
                        Utils.showToast(getActivity(),R.string.phone_verify_success);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, params,Parameters.SERVICE_SEND_VERIFY_PHONE);

        requestHelper.executeFormUrlEncoded();

    }



    private void sendVerificationCode() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+userId+"/verifyPhone",
                GeneralResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {

                GeneralResponse generalResponse = (GeneralResponse) response;
                if (response instanceof GeneralResponse)
                {
                    mProgressBar.setVisibility(View.GONE);
                    if (generalResponse.getId().equals("500"))//Error
                    {
                        Utils.showToast(getActivity(),R.string.error_invalid_phone);
                    }
                    else
                    {
                        mProgressBar.setVisibility(View.GONE);
                        Utils.showToast(getActivity(),R.string.enter_code_verify);
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                Utils.showToast(getActivity(),message);
            }
        }, Parameters.SERVICE_SEND_CODE_PHONE);

        requestHelper.executeGet();
    }

}
