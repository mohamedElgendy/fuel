package com.android.fuel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.android.fuel.application.FuelConstants;
import com.android.fuel.authentication.AuthenticationActivity;
import com.android.fuel.beans.ChargePackage;
import com.android.fuel.beans.RefundRequest;
import com.android.fuel.beans.User;
import com.android.fuel.main.FillFuelDoneActivity;
import com.android.fuel.main.MainActivity;
import com.android.fuel.main.QrCodeStationReaderActivity;
import com.android.fuel.main.RefundRequestActivity;
import com.android.fuel.responses.FillFuelResponse;
import com.android.fuel.splash.SplashActivity;
import com.google.gson.Gson;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class FuelManager {

    public static void openLogin(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.LOGIN_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openSplash(Activity activity)
    {
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        System.exit(0);
    }


    public static void openRegister(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.SIGN_UP_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openSettings(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.SETTINGS_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openAddOrEditPhone(Activity activity, String userId)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.ADD_PHONE_FRAG_TAG);
        intent.putExtra("userId", userId);
        activity.startActivity(intent);
    }

    public static void openVerifyPhone(Activity activity,String userId)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.ACTIVATION_FRAG_TAG);
        intent.putExtra("userId", userId);
        activity.startActivity(intent);
    }

    public static void openForgotPassword(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.FORGET_PASSWORD_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openChangePassword(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.CHANGE_PASSWORD_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openEditProfile(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.EDIT_PROFILE_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openOrderResult(Activity activity, FillFuelResponse response)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.FILL_ORDER_RESULT_FRAG_TAG);
        intent.putExtra("item", new Gson().toJson(response));
        activity.startActivity(intent);
    }

    public static void openMain(Activity activity)
    {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    public static void openQrCodeReader(Fragment fragment, int requestId)
    {
        Intent intent = new Intent(fragment.getActivity(), QrCodeStationReaderActivity.class);
        fragment.startActivityForResult(intent,requestId);
    }

    public static void openFillFuelDone(Activity activity,String gasFilledId)
    {
        Intent intent = new Intent(activity, FillFuelDoneActivity.class);
        intent.putExtra("gasFilledId", gasFilledId);
        activity.startActivity(intent);
    }

    public static void openUpdateFillFuel(Context activity, String gasFilledId) {

        Intent intent = new Intent(activity, RefundRequestActivity.class);
        intent.putExtra("gasFilledId", gasFilledId);
        activity.startActivity(intent);
    }

    public static void openStationsListing(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.STATION_LISTING_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openFillFuel(Activity activity,String stationId)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.STATION_FILL_FUEL_FRAG_TAG);
        intent.putExtra("stationId", stationId);
        activity.startActivity(intent);
    }

    public static void openStationInfo(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.STATION_INFO_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openRechargePackagesList(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_RECHARGE_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openPackageDetails(Activity activity, ChargePackage chargePackage)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_PACKAGE_DETAILS_FRAG_TAG);
        intent.putExtra("package", new Gson().toJson(chargePackage));
        activity.startActivity(intent);
    }

    public static void openRechargeFinish(Activity activity,float totalBalance,int totalPoints)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_RECHARGE_FINISH_FRAG_TAG);
        intent.putExtra("totalBalance",totalBalance );
        intent.putExtra("totalPoints",totalPoints );
        activity.startActivity(intent);
    }

    public static void openWalletTransactions(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_TRANSACTIONS_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openWalletTransferCredit(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_CREDIT_TRANSFER_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openTransferWalletCreditDetails(Activity activity, int transferBalance, User user)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_CREDIT_TRANSFER_DETAILS_FRAG_TAG);
        intent.putExtra("transferBalance",transferBalance );
        intent.putExtra("user", new Gson().toJson(user));
        activity.startActivity(intent);
    }

    public static void openConvertPoints(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_CONVERT_POINTS_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openConvertPointsFinish(Activity activity,float totalBalance,int totalPoints)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_CONVERT_POINTS_FINISH_FRAG_TAG);
        intent.putExtra("totalBalance",totalBalance );
        intent.putExtra("totalPoints",totalPoints );
        intent.putExtra("isFromConvertPoints",true );
        activity.startActivity(intent);
    }

    public static void openBuySubWallet(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_BUY_SUBWALLET_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openAddSubWallet(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_ADD_SUBWALLET_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openSubWalletInvitations(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_SUBWALLET_INVITATIONS_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openSubWallet(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_SUBWALLET_FRAG_TAG);
        activity.startActivity(intent);
    }

    public static void openInviteToSubWallet(Activity activity)
    {
        Intent intent = new Intent(activity, AuthenticationActivity.class);
        intent.putExtra("tag", FuelConstants.WALLET_INVITE_SUBWALLET_FRAG_TAG);
        activity.startActivity(intent);
    }
}
