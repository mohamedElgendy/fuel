package com.android.fuel.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.android.fuel.beans.User;
import com.google.gson.Gson;

import java.util.ArrayList;


/**
 * Created by Beshoy Adel on 7/6/2017.
 */

public class AppConstants {

    // Location updates intervals
    public static int UPDATE_INTERVAL = 3000; // 3 sec
    public static int FATEST_INTERVAL = 3000; // 5 sec
    public static int DISPLACEMENT = 10; // 10 meters

    public static User getUser(Activity activity) {

        SavePrefs<User> mUserSavePrefs = new SavePrefs<>(activity, User.class);
        User mUser = mUserSavePrefs.load();
        if (mUser != null) {
            //mUser.setId("5a2e62f2efb9e2001c831c21");
            return mUser;
        }

        return mUser;
    }

    public static void setAccessToken(Context activity, String accessToken) {
        SavePrefs<String> mUserSavePrefs = new SavePrefs<>(activity, String.class);
        mUserSavePrefs.save(accessToken);
    }

    public static String getAccessToken(Activity activity) {

        SavePrefs<String> mUserSavePrefs = new SavePrefs<>(activity, String.class);
        String accessToken = mUserSavePrefs.load();
        if (accessToken != null && !TextUtils.isEmpty(accessToken)) {
            //mUser.setId("5a2e62f2efb9e2001c831c21");
            return accessToken;
        }

        return null;
    }

    public static String getUserId(Activity activity) {

        SavePrefs<User> mUserSavePrefs = new SavePrefs<>(activity, User.class);
        User mUser = mUserSavePrefs.load();
        if (mUser != null) {
            //mUser.setId("5a2e62f2efb9e2001c831c21");
            return mUser.getUserId();
        }
        return null;
    }


    public static void setUserId(Context activity, String id) {
        SavePrefs<User> mUserSavePrefs = new SavePrefs<>(activity, User.class);
        User mUser = mUserSavePrefs.load();
        if (mUser == null)
            mUser = new User();

        mUser.setId(id);
        mUserSavePrefs.save(mUser);
    }

    public static void setUser(Context activity, User user) {
        SavePrefs<User> mUserSavePrefs = new SavePrefs<>(activity, User.class);
        mUserSavePrefs.save(user);
    }

    public static void clearUser(Context activity) {
        setAccessToken(activity,null);
        SavePrefs<User> mUserSavePrefs = new SavePrefs<>(activity, User.class);
        /*User mUser = mUserSavePrefs.load();
        if (mUser != null) {
            mUser.setId("");
        }*/
        mUserSavePrefs.save(null);
    }

    public static boolean isUserLoggedIn(Context activity) {
        SavePrefs<User> mUserSavePrefs = new SavePrefs<>(activity, User.class);
        User mUser = mUserSavePrefs.load();
        if (mUser == null)
            return false;
        else if (mUser.getId() == null || TextUtils.isEmpty(mUser.getId()))
            return false;
        return true;
    }

    public static final int LOGIN_TYPE_ACCOUNT = 0;
    public static final int LOGIN_TYPE_FACEBOOK = 1;
    public static final int LOGIN_TYPE_GOOGLE = 2;
}
