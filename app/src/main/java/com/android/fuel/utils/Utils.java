package com.android.fuel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Utility class to override system fonts all over the application
 */
public class Utils {


    private static final boolean showLog = true;
    private static final String LOGTAG  = "All4Rent-TAG:";


    public static void showLog(String logTXT){
        if (showLog){
            Log.d(LOGTAG,logTXT);
        }
    }

    public static boolean hasConnection(Activity context) {
        ConnectivityManager cm = (ConnectivityManager) context.getApplication()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }
        NetworkInfo mobileNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }
        return false;
    }




    public static boolean isEmptyString(String s) {
        if(s==null)
            return true;
        if(s.equals(""))
            return true;
        return false;
    }



    public static void showToast(Activity activity, int msgID) {
        if (activity!=null)
            Toast.makeText(activity, activity.getString(msgID), Toast.LENGTH_LONG)
                .show();
    }

    public static void showToast(Activity activity, String msgID) {
        if (activity!=null)
            Toast.makeText(activity, msgID, Toast.LENGTH_LONG)
                .show();
    }
    public static void showToast(Context activity, String msgID) {
        if (activity!=null)
            Toast.makeText(activity, msgID, Toast.LENGTH_LONG)
                .show();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static boolean isArabicLocale() {
        return Locale.getDefault().getLanguage().equals("ar");
    }

    public static void updateLanguage(Context context, String selectedLanguage) {
        if (!"".equals(selectedLanguage)) {
            Locale locale = new Locale(selectedLanguage);
            Locale.setDefault(locale);
            Resources res = context.getResources();
            Configuration config = new Configuration();
            DisplayMetrics dm = res.getDisplayMetrics();
            config.locale = locale;
            context.getResources().updateConfiguration(config, dm);
        }
    }

    public static boolean emailValidator(final String mailAddress) {
        return !TextUtils.isEmpty(mailAddress) && android.util.Patterns.EMAIL_ADDRESS.matcher(mailAddress).matches();
    }

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@") && !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isMobileValid(String mobile) {
        //TODO: Replace this with your own logic
        if (mobile.equals(null) || mobile.equals(""))
            return true;// is optional

        if (mobile.length()==11)
            return true;

        return false;
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 5;
    }

    public static String getTime(String time) {
        String delegate = "hh:mm aaa";
        return (String) DateFormat.format(delegate,generateDate(time));
    }

    public static String getDate(String time) {
        String delegate = "dd-MM-yyyy";
        return (String) DateFormat.format(delegate,generateDate(time));
    }

    public static Date generateDate(String time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //sdf.setTimeZone(tz);//set time zone.
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
       // String localTime = sdf.format(new Date(time));
        Date date = new Date();
        try {
            date = sdf.parse(time);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    // for Activities
   /* public static void showProgress(Activity activity)
    {
        activity.findViewById(R.id.v_progress).setVisibility(View.VISIBLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    // for Activities
    public static void hideProgress(Activity activity)
    {
        activity.findViewById(R.id.v_progress).setVisibility(View.GONE);
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    // for Fragments
    public static void showProgress(View view)
    {
        view.findViewById(R.id.v_progress).setVisibility(View.VISIBLE);
    }

    // for Fragments
    public static void hideProgress(View view)
    {
        view.findViewById(R.id.v_progress).setVisibility(View.GONE);
    }*/

    public static void changeTabsFont(Activity activity, TabLayout tabLayout, boolean isAllowRotation) {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    Typeface fontTypeface  = Typeface.createFromAsset(activity.getAssets(),"font.ttf");;
                    ((TextView) tabViewChild).setTypeface(fontTypeface);
                    float scale = activity.getResources().getDisplayMetrics().density;
                    int dpAsPixels = (int) (4*scale + 0.5f);
                    ((TextView) tabViewChild).setAllCaps(false);
                    ((TextView) tabViewChild).setPadding(0,0,0,dpAsPixels);
                    ((TextView) tabViewChild).setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                    if (isAllowRotation)
                        ((TextView) tabViewChild).setRotationY(180);
                }
            }
        }
    }
}
