package com.android.fuel.services;

import android.util.Pair;

import com.google.gson.GsonBuilder;

/**
 * Created by beshoy adel on 10/12/2016.
 */

public class Parameters {

    public static final String TAG = "EPM-TAG:";

    public static final String PHOTOS_BASE = "https://s3.amazonaws.com/sfp_images/";

    public static final String BASE_URL = "http://smartfuel.herokuapp.com/api/";

    public static final String UPLOAD_PHOTOS = "Containers/sfp_images/upload";


    public static final int SERVICE_UPLOAD_PHOTO = 1;
    public static final int SERVICE_LOGIN = 8;
    public static final int SERVICE_FOROGOT_PASSWORD = 9;
    public static final int SERVICE_REGISTER = 10;
    public static final int SERVICE_EMAIL_VERIFY = 11;
    public static final int SERVICE_ADD_PHONE = 12;
    public static final int SERVICE_SEND_CODE_PHONE = 13;
    public static final int SERVICE_SEND_VERIFY_PHONE = 14;
    public static final int SERVICE_EDIT_PROFILE = 15;
    public static final int SERVICE_CHANGE_PASSWORD = 16;
    public static final int SERVICE_GET_STATION_BY_ID = 17;
    public static final int SERVICE_GET_STATIONS = 18;
    public static final int SERVICE_GAS_FILL = 19;
    public static final int SERVICE_RATE_STATION = 20;
    public static final int SERVICE_CHECK_ATTANDANT = 21;
    public static final int SERVICE_GET_BANNERS = 22;
    public static final int SERVICE_GET_DRIVER = 23;
    public static final int SERVICE_RECHARGE = 24;

    public static final int MAX_PRODUCTS_COUNT = 10;

    public static String toJson(Object object) {
        //Gson disableHtmlEscaping for handling urls as it is
        // http://stackoverflow.com/questions/11149931/gson-tojson-object-contain-url
        return new GsonBuilder().disableHtmlEscaping().create().toJson(object);
    }

    public static Pair<String, String> makePair(String first, Object second) {
        return Pair.create(first, toJson(second));
    }
}
