package com.android.fuel.services;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.util.Charsets;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.fuel.services.Parameters.SERVICE_ADD_PHONE;
import static com.android.fuel.services.Parameters.SERVICE_CHANGE_PASSWORD;
import static com.android.fuel.services.Parameters.SERVICE_CHECK_ATTANDANT;
import static com.android.fuel.services.Parameters.SERVICE_EDIT_PROFILE;
import static com.android.fuel.services.Parameters.SERVICE_EMAIL_VERIFY;
import static com.android.fuel.services.Parameters.SERVICE_FOROGOT_PASSWORD;
import static com.android.fuel.services.Parameters.SERVICE_GAS_FILL;
import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;
import static com.android.fuel.services.Parameters.SERVICE_GET_DRIVER;
import static com.android.fuel.services.Parameters.SERVICE_GET_STATIONS;
import static com.android.fuel.services.Parameters.SERVICE_GET_STATION_BY_ID;
import static com.android.fuel.services.Parameters.SERVICE_LOGIN;
import static com.android.fuel.services.Parameters.SERVICE_RATE_STATION;
import static com.android.fuel.services.Parameters.SERVICE_RECHARGE;
import static com.android.fuel.services.Parameters.SERVICE_REGISTER;
import static com.android.fuel.services.Parameters.SERVICE_SEND_CODE_PHONE;
import static com.android.fuel.services.Parameters.SERVICE_SEND_VERIFY_PHONE;
import static com.android.fuel.services.Parameters.SERVICE_UPLOAD_PHOTO;

public class RequestHelper<T> {

    private static int TIMEOUT = 45 * 1000; // Ion's default timeout is 30 seconds.
    private static final String LOG_TAG = "Fuel-TAG: request_helper";

    Activity activity;
    String baseUrl;
    String apiName;
    Class<?> cls;
    RequestListener<T> listener;
    Map<String, List<String>> params;
    Map<String, File> files;

    Future<String> future;
    int serviceId=-1;

    long startTime, finishTime;


    public RequestHelper(Activity activity, String baseUrl, String apiName, @Nullable Class<?> cls, RequestListener<T> listener, int serviceId) {
        this.activity = activity;
        this.baseUrl = baseUrl;
        this.apiName = apiName;
        this.cls = cls;
        this.listener = listener;
        this.serviceId=serviceId;
    }

    public RequestHelper(Activity activity, String baseUrl, String apiName, @Nullable Class<?> cls, RequestListener<T> listener, int serviceId, Map<String, File> files) {
        this.activity = activity;
        this.baseUrl = baseUrl;
        this.apiName = apiName;
        this.cls = cls;
        this.listener = listener;
        this.serviceId=serviceId;

        if (files != null) {
            this.files = new HashMap<>();
            for (String key : files.keySet()) {
                this.files.put(key, files.get(key));
            }
        }
    }

    public RequestHelper(Activity activity, String baseUrl, String apiName, @Nullable Class<?> cls, RequestListener<T> listener, Map<String, String> params, int serviceId) {
        this(activity, baseUrl, apiName, cls, listener,serviceId);
        this.serviceId=serviceId;
        if (params != null) {
            this.params = new HashMap<>();
            for (String key : params.keySet()) {
                this.params.put(key, Collections.singletonList(params.get(key)));
            }
        }

    }


    private void printLogs(int level) {
        switch (level) {
            case 0:
                startTime = System.currentTimeMillis();
                Utils.showLog(LOG_TAG + "'" + apiName + "' request started. time=" + Calendar.getInstance().getTime());
                break;
            case 1:
                finishTime = System.currentTimeMillis();
                Utils.showLog(LOG_TAG + "'" + apiName + "' request finished and parsing started. time=" + Calendar.getInstance().getTime() + ", Time diff: " + (finishTime - startTime) + " MS");
                break;

        }
    }

    /**
     * Execute get request. (requires baseUrl & apiName)
     * @return Future object for cancelling the request.
     */
    public Future<String> executeGet() {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);  // request started

            if (activity==null)
                return null;
            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }
            future = Ion.with(activity)
                    .load(baseUrl + apiName)
                    .addHeader("Accept","application/json")
                    .addHeader("Content-Type","application/x-www-form-urlencoded")
                    .addHeader("Authorization",accessToken)
                    .setTimeout(TIMEOUT)
                    .setLogging("EPM-TAG:", Log.VERBOSE)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });

            return future;
        }
    }

    /**
     * Execute x-www-form-urlencoded post request with string parameters.
     * (requires at least baseUrl & apiName)
     * @return Future object for cancelling the request.
     */
    public Future<String> executeFormUrlEncoded() {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);

            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }
            Builders.Any.B ionBuilder = Ion.with(activity)
                    .load("POST", baseUrl + apiName)
                    .setLogging("EPM-TAG:", Log.VERBOSE)
                    .addHeader("Accept","application/json")
                    .addHeader("Content-Type","application/x-www-form-urlencoded")
                    .addHeader("Authorization",accessToken)
                    .setTimeout(TIMEOUT);

            if (params != null) {
                for (String key : params.keySet()) {
                    Utils.showLog(LOG_TAG + "Request Parameter: " + key + "=" + params.get(key));
                }
                ionBuilder.setBodyParameters(params);
            }

            future = ionBuilder.asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });

            return future;
        }
    }

    public Future<String> executePatchFormUrlEncoded() {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);

            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }
            Builders.Any.B ionBuilder = Ion.with(activity)
                    .load("PATCH", baseUrl + apiName)
                    .setLogging("EPM-TAG:", Log.VERBOSE)
                    .addHeader("Accept","application/json")
                    .addHeader("Content-Type","application/x-www-form-urlencoded")
                    .addHeader("Authorization",accessToken)
                    .setTimeout(TIMEOUT);

            if (params != null) {
                for (String key : params.keySet()) {
                    Utils.showLog(LOG_TAG + "Request Parameter: " + key + "=" + params.get(key));
                }
                ionBuilder.setBodyParameters(params);
            }

            future = ionBuilder.asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });

            return future;
        }
    }

    /**
     * Execute a multipart post request with text and image parameters.
     * (requires at least baseUrl & apiName)
     * @return Future object for cancelling the request.
     */
    public Future<String> executeMultiPart() {
        if (baseUrl == null || apiName == null || (params == null && files == null)) {
            throw new IllegalArgumentException("No baseUrl, apiName, params or files found.");
        } else {
            printLogs(0);  // request started

            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }

            Builders.Any.B ionBuilder =
                    Ion.with(activity)
                            .load("POST", baseUrl + apiName)
                            .addHeader("Authorization",accessToken)
                            .setTimeout(TIMEOUT);
//                            .uploadProgress(new ProgressCallback() {
//                                @Override
//                                public void onProgress(long downloaded, long total) {
//                                    Log.e("onProgress()", (int) downloaded + "/ " + total);
//                                }
//                            });

            if (params != null) {
                for (String key : params.keySet()) {
                    Utils.showLog(LOG_TAG + "Request Parameter: " + key + "=" + params.get(key).get(0));
                }

                ionBuilder.setMultipartParameters(params);
            }

            for (String key : files.keySet()) {
                Utils.showLog(LOG_TAG + "Multipart Parameter: " + key + "=" + files.get(key).getAbsolutePath());
                ionBuilder.setMultipartFile(key, "image/jpeg", files.get(key));
            }

            future = ionBuilder.asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });

            return future;
        }
    }

    /**
     * Execute Raw post request with Json string parameters.
     * (requires at least baseUrl & apiName)
     * @return Future object for cancelling the request.
     */
    public Future<String> executeFromRawJson(String requestBody) {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);

            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }

            String fullURL = baseUrl + apiName;
            //Query parameters
            Utils.showLog(LOG_TAG + "Request POST Full URL: " + fullURL);

            Builders.Any.B ionBuilder = Ion.with(activity)
                    .load("POST", fullURL)
                    .setHeader("Content-Type", "application/json")
                    .addHeader("Authorization",accessToken)
                    .setLogging(LOG_TAG, Log.VERBOSE)
                    .setTimeout(TIMEOUT);

            ionBuilder.setStringBody(requestBody);

            future = ionBuilder.asString(Charsets.UTF_8)
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });


            return future;
        }
    }

    public Future<String> executeDeleteFromRawJson(String requestBody) {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);


            String fullURL = baseUrl + apiName;
            //Query parameters
            Utils.showLog(LOG_TAG + "Request POST Full URL: " + fullURL);

            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }

            Builders.Any.B ionBuilder = Ion.with(activity)
                    .load("DELETE", fullURL)
                    .addHeader("Authorization",accessToken)
                    .setHeader("Content-Type", "application/json")
                    .setLogging(LOG_TAG, Log.VERBOSE)
                    .setTimeout(TIMEOUT);

            ionBuilder.setStringBody(requestBody);

            future = ionBuilder.asString(Charsets.UTF_8)
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });


            return future;
        }
    }

    public Future<String> executePutFromRawJson(String requestBody) {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);


            String fullURL = baseUrl + apiName;
            //Query parameters
            Utils.showLog(LOG_TAG + "Request POST Full URL: " + fullURL);


            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }

            Builders.Any.B ionBuilder = Ion.with(activity)
                    .load("PUT", fullURL)
                    .addHeader("Authorization",accessToken)
                    .setHeader("Content-Type", "application/json")
                    .setLogging(LOG_TAG, Log.VERBOSE)
                    .setTimeout(TIMEOUT);

            ionBuilder.setStringBody(requestBody);

            future = ionBuilder.asString(Charsets.UTF_8)
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });


            return future;
        }
    }

    public Future<String> executePatchFromRawJson(String requestBody) {
        if (baseUrl == null || apiName == null) {
            throw new IllegalArgumentException("No baseUrl or apiName found.");
        } else {
            printLogs(0);


            String fullURL = baseUrl + apiName;
            //Query parameters
            Utils.showLog(LOG_TAG + "Request POST Full URL: " + fullURL);


            String accessToken ="";
            if (AppConstants.getAccessToken(activity)!=null)
            {
                accessToken = AppConstants.getAccessToken(activity);
            }

            Builders.Any.B ionBuilder = Ion.with(activity)
                    .load("PATCH", fullURL)
                    .addHeader("Authorization",accessToken)
                    .setHeader("Content-Type", "application/json")
                    .setLogging(LOG_TAG, Log.VERBOSE)
                    .setTimeout(TIMEOUT);

            ionBuilder.setStringBody(requestBody);

            future = ionBuilder.asString(Charsets.UTF_8)
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            handleOnCompleted(e, result);
                        }
                    });


            return future;
        }
    }

    /**
     * Handles the completion of the request if (success or fail) and deserialize the string response using the given Class object and executes the  onFail or onSuccess method.
     * @param e the exception object (may be null if request failed).
     * @param result the string response.
     */
    @SuppressWarnings("unchecked")
    private void handleOnCompleted(Exception e, String result) {
        printLogs(1);  // request finished

        if (listener != null) {
            if (e != null) { //on request failure and cancellation
                listener.onFail(e.toString(),apiName);
            } else if (result != null) {
                Utils.showLog(LOG_TAG + "Response:-" + result);
                if (cls == null) { //T must be of type: Object or String
                    listener.onSuccess((T) result, apiName);
                } else {
                    try {
                        if (serviceId==SERVICE_LOGIN)
                        {
                            if (result.contains("error") && result.contains("401"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("401");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_EMAIL_VERIFY)
                        {
                            if (result.equals(""))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("0");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("1");
                                listener.onSuccess((T)response, apiName);
                            }
                        }
                        else if (serviceId==SERVICE_SEND_CODE_PHONE)
                        {
                            if (result.contains("res") && result.contains("success"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("0");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("500");
                                listener.onSuccess((T)response,apiName);
                            }
                        }
                        else if (serviceId==SERVICE_SEND_VERIFY_PHONE)
                        {
                            if (result.contains("res") && result.contains("Passcode accepted"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("0");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("500");
                                listener.onSuccess((T)response,apiName);
                            }
                        }
                        else if (serviceId == SERVICE_UPLOAD_PHOTO)
                        {
                            listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                        }
                        else if (serviceId==SERVICE_REGISTER)
                        {
                            if (result.contains("error") && result.contains("422") && result.contains("User already exists"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("401");
                                listener.onSuccess((T)response,apiName);
                            }
                            else if (result.contains("error") && result.contains("422") && result.contains("Email already exists"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("402");
                                listener.onSuccess((T)response,apiName);
                            }
                            else if (result.contains("error") && result.contains("422"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("403");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_RECHARGE)
                        {
                            if (result.contains("error") && result.contains("422") && result.contains("not enough balance"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("401");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_EDIT_PROFILE)
                        {
                            if (result.contains("error") && result.contains("422") && result.contains("User already exists"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("401");
                                listener.onSuccess((T)response,apiName);
                            }
                            else if (result.contains("error") && result.contains("422") && result.contains("Email already exists"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("402");
                                listener.onSuccess((T)response,apiName);
                            }
                            else if (result.contains("error") && result.contains("422"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("403");
                                listener.onSuccess((T)response,apiName);
                            }
                            else if (result.contains("error") && result.contains("500"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("403");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_FOROGOT_PASSWORD)
                        {
                            if (result.contains("error") && result.contains("500"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("500");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("1");
                                listener.onSuccess((T)response, apiName);
                            }
                        }
                        else if (serviceId==SERVICE_CHANGE_PASSWORD)
                        {
                            if (result.contains("error") && result.contains("400") && result.contains("Invalid current password"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("401");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("1");
                                listener.onSuccess((T)response, apiName);
                            }
                        }
                        else if (serviceId==SERVICE_ADD_PHONE)
                        {
                            if (result.contains("error") && result.contains("500"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("500");
                                listener.onSuccess((T)response,apiName);
                            }
                            else if (result.contains("error") && result.contains("422"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("501");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_GET_STATION_BY_ID)
                        {
                            if (result.contains("error") && result.contains("404") && result.contains("MODEL_NOT_FOUND"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("404");//station not found
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_GAS_FILL)
                        {
                            if (result.contains("error") && result.contains("500"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("500");//station not found
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_GET_DRIVER)
                        {
                            if (result.contains("error"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("400");//station not found
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_CHECK_ATTANDANT)
                        {
                            if (result.contains("error") && result.contains("404"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("404");//station not found
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }
                        else if (serviceId==SERVICE_RATE_STATION)
                        {
                            if (result.contains("error") && result.contains("11000"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("11000");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("1");
                                listener.onSuccess((T)response,apiName);
                            }
                        }
                        else if (serviceId==SERVICE_GET_STATIONS || serviceId==SERVICE_GET_BANNERS)
                            listener.onSuccess((T) new Gson().fromJson("{\"items\":"+result+"}", cls), apiName);
                        /*if (serviceId==SERVICE_LOGIN)
                        {
                            if (result.contains("error") && result.contains("401"))
                            {
                                if (result.contains("LOGIN_FAILED_EMAIL_NOT_VERIFIED")) //email not verified
                                {
                                    JSONObject mainObject = new JSONObject(result);
                                    JSONObject errorObject = mainObject.getJSONObject("error");
                                    JSONObject uniObject = errorObject.getJSONObject("details");
                                    String userId = uniObject.getString("userId");

                                    GeneralResponse response = new GeneralResponse();
                                    response.setId("402");
                                    response.setUserId(userId);
                                    listener.onSuccess((T)response,apiName);
                                }
                                else//login data invalid
                                {
                                    GeneralResponse response = new GeneralResponse();
                                    response.setId("401");
                                    listener.onSuccess((T)response,apiName);
                                }
                            }
                            else
                            {
                                listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                            }
                        }*/
                        /*else if (serviceId==SERVICE_GET_PRODUCT_DISCOUNTS ||
                                serviceId==SERVICE_GET_PRODUCT_REVIEWS ||
                                serviceId==SERVICE_GET_PRODUCTS_BY_CATEGORIES ||
                                serviceId==SERVICE_GET_CATEGORIES ||
                                serviceId == SERVICE_GET_CURRENCIES ||
                                serviceId == SERVICE_GET_COUNTRIES)
                            listener.onSuccess((T) new Gson().fromJson("{\"items\":"+result+"}", cls), apiName);


                        else if (serviceId==SERVICE_ADD_RENT)
                        {
                            if (result.contains("error") && result.contains("11000"))
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("11000");
                                listener.onSuccess((T)response,apiName);
                            }
                            else
                            {
                                GeneralResponse response = new GeneralResponse();
                                response.setId("1");
                                listener.onSuccess((T)response,apiName);
                            }
                        }
                        */
                        else
                            listener.onSuccess((T) new Gson().fromJson(result, cls), apiName);
                    } catch (Exception ex) {
                        Utils.showLog(LOG_TAG + "Parsing Exception: " + ex.toString());
                        listener.onFail("",apiName);
                    }
                }
            }
        }
    }

    /**
     * Cancels the request.
     * @param interruptThread true if the thread executing this task should be interrupted; otherwise, in-progress tasks are allowed to complete.
     * @return false if the task could not be cancelled, typically because it has already completed normally; true otherwise.
     */
    public boolean cancel(boolean interruptThread) {
        if (!(future.isCancelled() || future.isDone())) {
            return future.cancel(interruptThread);
        }

        return false;
    }


}
