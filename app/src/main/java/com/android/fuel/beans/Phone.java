package com.android.fuel.beans;

/**
 * Created by beshoy adel on 11/17/2017.
 */

public class Phone {

    private String phone;

    private String countryCode;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
