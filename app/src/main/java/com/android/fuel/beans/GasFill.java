package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/16/2017.
 */

public class GasFill {

    private String createdOn;

    private String walletId;

    private boolean isComplete;

    private String gasStationId;

    private String gasType;

    private String liters;

    private String gasCompanyId;

    private String driverId;

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public boolean getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public String getGasStationId() {
        return gasStationId;
    }

    public void setGasStationId(String gasStationId) {
        this.gasStationId = gasStationId;
    }

    public String getGasType() {
        return gasType;
    }

    public void setGasType(String gasType) {
        this.gasType = gasType;
    }

    public String getLiters() {
        return liters;
    }

    public void setLiters(String liters) {
        this.liters = liters;
    }

    public String getGasCompanyId() {
        return gasCompanyId;
    }

    public void setGasCompanyId(String gasCompanyId) {
        this.gasCompanyId = gasCompanyId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

}
