package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/16/2017.
 */

public class GasCompany {

    private String createdOn;

    private String contactEmail;

    private String name;

    private UserImage _image;

    private String state;

    private String id;

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserImage get_image() {
        return _image;
    }

    public void set_image(UserImage _image) {
        this._image = _image;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
