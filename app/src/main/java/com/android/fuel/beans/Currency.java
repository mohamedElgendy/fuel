package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class Currency {

    private String id;

    private String currentUSDValue;

    private String enCurrencyCode;

    private String enCurrency;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCurrentUSDValue ()
    {
        return currentUSDValue;
    }

    public void setCurrentUSDValue (String currentUSDValue)
    {
        this.currentUSDValue = currentUSDValue;
    }

    public String getEnCurrencyCode ()
    {
        return enCurrencyCode;
    }

    public void setEnCurrencyCode (String enCurrencyCode)
    {
        this.enCurrencyCode = enCurrencyCode;
    }

    public String getEnCurrency ()
    {
        return enCurrency;
    }

    public void setEnCurrency (String enCurrency)
    {
        this.enCurrency = enCurrency;
    }

}
