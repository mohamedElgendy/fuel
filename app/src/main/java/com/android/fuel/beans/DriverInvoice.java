package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/16/2017.
 */

public class DriverInvoice {

    private Station gasStation;

    private GasCompany gasCompany;

    private String createdOn;

    private User driver;

    private String gasType;

    private GasFill gasFill;

    private double total;

    private Wallet wallet;

    private String gasFillId;;

    public Station getGasStation() {
        return gasStation;
    }

    public void setGasStation(Station gasStation) {
        this.gasStation = gasStation;
    }

    public GasCompany getGasCompany() {
        return gasCompany;
    }

    public void setGasCompany(GasCompany gasCompany) {
        this.gasCompany = gasCompany;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public String getGasType() {
        return gasType;
    }

    public void setGasType(String gasType) {
        this.gasType = gasType;
    }

    public GasFill getGasFill() {
        return gasFill;
    }

    public void setGasFill(GasFill gasFill) {
        this.gasFill = gasFill;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public String getGasFillId() {
        return gasFillId;
    }

    public void setGasFillId(String gasFillId) {
        this.gasFillId = gasFillId;
    }
}
