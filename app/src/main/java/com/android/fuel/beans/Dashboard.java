package com.android.fuel.beans;

/**
 * Created by beshoy adel on 2/1/2018.
 */

public class Dashboard {

    private float gas91Price;
    private float gas95Price;
    private float dieselPrice;
    private float balance;
    private int points;
    private int currentWalletCount;//wait Ismail to send the real param name
    private int maxWalletCount;


    public float getGas91Price() {
        return gas91Price;
    }

    public void setGas91Price(float gas91Price) {
        this.gas91Price = gas91Price;
    }

    public float getGas95Price() {
        return gas95Price;
    }

    public void setGas95Price(float gas95Price) {
        this.gas95Price = gas95Price;
    }

    public float getDieselPrice() {
        return dieselPrice;
    }

    public void setDieselPrice(float dieselPrice) {
        this.dieselPrice = dieselPrice;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getCurrentWalletCount() {
        return currentWalletCount;
    }

    public void setCurrentWalletCount(int currentWalletCount) {
        this.currentWalletCount = currentWalletCount;
    }

    public int getMaxWalletCount() {
        return maxWalletCount;
    }

    public void setMaxWalletCount(int maxWalletCount) {
        this.maxWalletCount = maxWalletCount;
    }
}
