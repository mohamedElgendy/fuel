package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class Wallet {

    private String id;

    private String createdOn;

    private float balance;

    private String currencyId;

    private String ownerId;

    private String name;

    private User owner;

    private String walletParentId;

    private int points;

    private int sunWalletsNum;//wait Ismail to send the real param name

    private String type;

    private Currency currency;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCreatedOn ()
    {
        return createdOn;
    }

    public void setCreatedOn (String createdOn)
    {
        this.createdOn = createdOn;
    }

    public float getBalance ()
    {
        return balance;
    }

    public void setBalance (float balance)
    {
        this.balance = balance;
    }

    public String getCurrencyId ()
    {
        return currencyId;
    }

    public void setCurrencyId (String currencyId)
    {
        this.currencyId = currencyId;
    }

    public String getOwnerId ()
    {
        return ownerId;
    }

    public void setOwnerId (String ownerId)
    {
        this.ownerId = ownerId;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public User getOwner ()
    {
        return owner;
    }

    public void setOwner (User owner)
    {
        this.owner = owner;
    }

    public int getPoints ()
    {
        return points;
    }

    public void setPoints (int points)
    {
        this.points = points;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public Currency getCurrency ()
    {
        return currency;
    }

    public void setCurrency (Currency currency)
    {
        this.currency = currency;
    }

    public String getWalletParentId() {
        return walletParentId;
    }

    public void setWalletParentId(String walletParentId) {
        this.walletParentId = walletParentId;
    }

    public int getSunWalletsNum() {
        return sunWalletsNum;
    }

    public void setSunWalletsNum(int sunWalletsNum) {
        this.sunWalletsNum = sunWalletsNum;
    }
}
