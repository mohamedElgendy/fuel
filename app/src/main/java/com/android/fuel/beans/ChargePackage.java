package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class ChargePackage {

    private String id;

    private String createdOn;

    private float balance;

    private int price;

    private String name;

    private UserImage _image;

    private int points;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserImage get_image() {
        return _image;
    }

    public void set_image(UserImage _image) {
        this._image = _image;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
