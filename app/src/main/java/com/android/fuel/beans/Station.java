package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class Station {

    private Position position;

    private float gas95Price;

    private boolean hasRadio;

    private String currencyId;

    private String gasCompanyId;

    private float gas91Price;

    private boolean hasATM;

    private boolean hasCafe;

    private boolean hasMensWC;

    private boolean hasRestaurant;

    private boolean hasMasjid;

    private Currency currency;

    private String id;

    private boolean isActive;

    private String createdOn;

    private boolean hasHotel;

    private boolean hasMarket;

    private String title;

    private boolean isGas91Available;

    private String address;

    private boolean hasLadiesWC;

    private Company company;

    private float rating;

    private float dieselPrice;

    private boolean isGas95Available;

    private boolean isDieselAvailable;


    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isDieselAvailable() {
        return isDieselAvailable;
    }

    public void setDieselAvailable(boolean dieselAvailable) {
        isDieselAvailable = dieselAvailable;
    }

    public float getGas95Price() {
        return gas95Price;
    }

    public void setGas95Price(float gas95Price) {
        this.gas95Price = gas95Price;
    }

    public boolean isHasRadio() {
        return hasRadio;
    }

    public void setHasRadio(boolean hasRadio) {
        this.hasRadio = hasRadio;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getGasCompanyId() {
        return gasCompanyId;
    }

    public void setGasCompanyId(String gasCompanyId) {
        this.gasCompanyId = gasCompanyId;
    }

    public float getGas91Price() {
        return gas91Price;
    }

    public void setGas91Price(float gas91Price) {
        this.gas91Price = gas91Price;
    }

    public boolean isHasCafe() {
        return hasCafe;
    }

    public void setHasCafe(boolean hasCafe) {
        this.hasCafe = hasCafe;
    }

    public boolean isHasMensWC() {
        return hasMensWC;
    }

    public void setHasMensWC(boolean hasMensWC) {
        this.hasMensWC = hasMensWC;
    }

    public boolean isHasRestaurant() {
        return hasRestaurant;
    }

    public void setHasRestaurant(boolean hasRestaurant) {
        this.hasRestaurant = hasRestaurant;
    }

    public boolean isHasMasjid() {
        return hasMasjid;
    }

    public void setHasMasjid(boolean hasMasjid) {
        this.hasMasjid = hasMasjid;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isHasHotel() {
        return hasHotel;
    }

    public void setHasHotel(boolean hasHotel) {
        this.hasHotel = hasHotel;
    }

    public boolean isHasMarket() {
        return hasMarket;
    }

    public void setHasMarket(boolean hasMarket) {
        this.hasMarket = hasMarket;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isGas91Available() {
        return isGas91Available;
    }

    public void setGas91Available(boolean gas91Available) {
        isGas91Available = gas91Available;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isHasLadiesWC() {
        return hasLadiesWC;
    }

    public void setHasLadiesWC(boolean hasLadiesWC) {
        this.hasLadiesWC = hasLadiesWC;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public float getDieselPrice() {
        return dieselPrice;
    }

    public void setDieselPrice(float dieselPrice) {
        this.dieselPrice = dieselPrice;
    }

    public boolean isGas95Available() {
        return isGas95Available;
    }

    public void setGas95Available(boolean gas95Available) {
        isGas95Available = gas95Available;
    }

    public boolean isHasATM() {
        return hasATM;
    }

    public void setHasATM(boolean hasATM) {
        this.hasATM = hasATM;
    }
}
