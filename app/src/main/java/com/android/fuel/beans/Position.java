package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class Position {

    private double lng;

    private double lat;


    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
