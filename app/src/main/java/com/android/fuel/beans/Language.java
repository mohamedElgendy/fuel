package com.android.fuel.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beshoy adel on 12/2/2017.
 */

public class Language {

    private String locale;

    @SerializedName("_icon")
    private UserImage icon;

    private boolean isSelected;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public UserImage getIcon() {
        return icon;
    }

    public void setIcon(UserImage icon) {
        this.icon = icon;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
