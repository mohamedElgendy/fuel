package com.android.fuel.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beshoy adel on 11/7/2017.
 */

public class User {

    private String name;
    private String password;
    private String id;
    private int loginType;
    private String userId;
    private boolean isSMSable;
    private boolean isEmailable;
    private boolean phoneVerified;
    private float rating;
    private String createdOn;
    private String state;
    private String preferedLanguage;
    private String prefCurrencyId;
    private String countryId;
    private String realm;
    private String username;
    private String email;
    private boolean emailVerified;
    private boolean isCompany;

    private String iqama;
    @SerializedName("phone")
    private Phone phone;

    @SerializedName("_image")
    private UserImage image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSMSable() {
        return isSMSable;
    }

    public void setSMSable(boolean SMSable) {
        isSMSable = SMSable;
    }

    public boolean isEmailable() {
        return isEmailable;
    }

    public void setEmailable(boolean emailable) {
        isEmailable = emailable;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(String preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public UserImage getImage() {
        return image;
    }

    public void setImage(UserImage image) {
        this.image = image;
    }

    public String getPrefCurrencyId() {
        return prefCurrencyId;
    }

    public void setPrefCurrencyId(String prefCurrencyId) {
        this.prefCurrencyId = prefCurrencyId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIqama() {
        return iqama;
    }

    public void setIqama(String iqama) {
        this.iqama = iqama;
    }

    public boolean getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(boolean isCompany) {
        this.isCompany = isCompany;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }
}
