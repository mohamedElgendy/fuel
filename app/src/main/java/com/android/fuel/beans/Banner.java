package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/17/2017.
 */

public class Banner {

    private String type;
    private String id;
    private UserImage _image;
    private String width;
    private String height;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserImage get_image() {
        return _image;
    }

    public void set_image(UserImage _image) {
        this._image = _image;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
