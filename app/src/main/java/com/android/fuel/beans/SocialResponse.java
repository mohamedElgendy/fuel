package com.android.fuel.beans;

/**
 * Created by beshoy adel on 2/2/2018.
 */

public class SocialResponse {

    private String access_token;

    private String userId;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
