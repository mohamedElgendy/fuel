package com.android.fuel.beans;

/**
 * Created by Mohamed Elgendy.
 */

public class SubWalletPackage {

    private int numberOfWallets;
    private int price;
    private boolean isActive;
    private String id;
    private String currencyId;

    public int getNumberOfWallets() {
        return numberOfWallets;
    }

    public void setNumberOfWallets(int numberOfWallets) {
        this.numberOfWallets = numberOfWallets;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }
}