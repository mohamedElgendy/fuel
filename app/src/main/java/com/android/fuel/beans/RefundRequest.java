package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/16/2017.
 */

public class RefundRequest {

    private String id;

    private String createdOn;

    private String driverMessage;

    private String repliedOn;

    private String adminLiters;

    private UserImage _image;

    private String state;

    private String driverLiters;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getDriverMessage() {
        return driverMessage;
    }

    public void setDriverMessage(String driverMessage) {
        this.driverMessage = driverMessage;
    }

    public String getRepliedOn() {
        return repliedOn;
    }

    public void setRepliedOn(String repliedOn) {
        this.repliedOn = repliedOn;
    }

    public String getAdminLiters() {
        return adminLiters;
    }

    public void setAdminLiters(String adminLiters) {
        this.adminLiters = adminLiters;
    }

    public UserImage get_image() {
        return _image;
    }

    public void set_image(UserImage _image) {
        this._image = _image;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDriverLiters() {
        return driverLiters;
    }

    public void setDriverLiters(String driverLiters) {
        this.driverLiters = driverLiters;
    }
}
