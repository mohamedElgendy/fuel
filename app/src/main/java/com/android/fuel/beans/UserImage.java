package com.android.fuel.beans;

/**
 * Created by beshoy adel on 11/7/2017.
 */

public class UserImage {

    private String url;
    private String id;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
