package com.android.fuel.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beshoy adel on 12/2/2017.
 */

public class Country {

    private String code;
    private String title;
    private String id;
    private String currencyId;

    private boolean isSelected;

    @SerializedName("_icon")
    private UserImage icon;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public UserImage getIcon() {
        return icon;
    }

    public void setIcon(UserImage icon) {
        this.icon = icon;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
