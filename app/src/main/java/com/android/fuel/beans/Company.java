package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class Company {

    private String id;

    private String name;

    private UserImage _image;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }


    public UserImage get_image() {
        return _image;
    }

    public void set_image(UserImage _image) {
        this._image = _image;
    }
}
