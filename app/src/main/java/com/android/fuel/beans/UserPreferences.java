package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/2/2017.
 */

public class UserPreferences {

    private Country country;
    private Language language;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }


    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
