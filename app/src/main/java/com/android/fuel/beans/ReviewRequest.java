package com.android.fuel.beans;

/**
 * Created by beshoy adel on 11/9/2017.
 */

public class ReviewRequest {

    private int rating;
    private String review;
    private String reviewerId;


    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(String reviewerId) {
        this.reviewerId = reviewerId;
    }
}
