package com.android.fuel.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by beshoy adel on 12/15/2017.
 */

public class WalletTransaction {

    private String id;

    private String createdOn;

    private String walletId;

    private float balance;

    private int points;

    private String type;

    @SerializedName("package")
    private ChargePackage chargePackage;

    private String packageId;


    private String toWalletId;

    private Wallet toWallet;

    private Wallet fromWallet;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCreatedOn ()
    {
        return createdOn;
    }

    public void setCreatedOn (String createdOn)
    {
        this.createdOn = createdOn;
    }

    public String getWalletId ()
    {
        return walletId;
    }

    public void setWalletId (String walletId)
    {
        this.walletId = walletId;
    }


    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public ChargePackage getChargePackage() {
        return chargePackage;
    }

    public void setChargePackage(ChargePackage chargePackage) {
        this.chargePackage = chargePackage;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getToWalletId() {
        return toWalletId;
    }

    public void setToWalletId(String toWalletId) {
        this.toWalletId = toWalletId;
    }

    public Wallet getToWallet() {
        return toWallet;
    }

    public void setToWallet(Wallet toWallet) {
        this.toWallet = toWallet;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Wallet getFromWallet() {
        return fromWallet;
    }

    public void setFromWallet(Wallet fromWallet) {
        this.fromWallet = fromWallet;
    }
}
