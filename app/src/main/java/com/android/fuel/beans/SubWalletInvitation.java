package com.android.fuel.beans;

/**
 * Created by beshoy adel on 12/23/2017.
 */

public class SubWalletInvitation {

    private String  id;

    private String driverId;

    private String walletId;

    private boolean hasChargePermission;

    private boolean hasGasfillPermission;

    private Wallet wallet;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public boolean isHasChargePermission() {
        return hasChargePermission;
    }

    public void setHasChargePermission(boolean hasChargePermission) {
        this.hasChargePermission = hasChargePermission;
    }

    public boolean isHasGasfillPermission() {
        return hasGasfillPermission;
    }

    public void setHasGasfillPermission(boolean hasGasfillPermission) {
        this.hasGasfillPermission = hasGasfillPermission;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
