package com.android.fuel.application;

/**
 * Created by Mohamed Elgendy.
 */

public class FuelConstants {

    public static final String LOGIN_FRAG_TAG = "login_fragment_tag";
    public static final String SIGN_UP_FRAG_TAG = "sign_up_fragment_tag";
    public static final String CHANGE_PASSWORD_FRAG_TAG = "change_password_fragment_tag";
    public static final String EDIT_PROFILE_FRAG_TAG = "edit_profile_fragment_tag";
    public static final String FORGET_PASSWORD_FRAG_TAG = "forget_password_fragment_tag";
    public static final String ACTIVATION_FRAG_TAG = "activation_fragment_tag";
    public static final String ADD_PHONE_FRAG_TAG = "add_phone_fragment_tag";
    public static final String FILL_FUEL_FRAG_TAG = "fill_fuel_fragment_tag";
    public static final String FIND_STATION_FRAG_TAG = "find_station_fragment_tag";
    public static final String INVOICES_FRAG_TAG = "invoices_fragment_tag";
    public static final String PROFILE_FRAG_TAG = "profile_fragment_tag";
    public static final String WALLETS_FRAG_TAG = "wallets_fragment_tag";
    public static final String SETTINGS_FRAG_TAG = "settings_fragment_tag";
    public static final String FILL_ORDER_RESULT_FRAG_TAG = "fill_order_result_fragment_tag";
    public static final String STATION_LISTING_FRAG_TAG = "stations_listing_fragment_tag";
    public static final String STATION_INFO_FRAG_TAG = "stations_info_fragment_tag";
    public static final String STATION_FILL_FUEL_FRAG_TAG = "stations_fill_fuel_fragment_tag";
    public static final String WALLET_RECHARGE_FRAG_TAG = "wallet_recharge_fragment_tag";
    public static final String WALLET_PACKAGE_DETAILS_FRAG_TAG = "wallet_Package_details_fragment_tag";
    public static final String WALLET_RECHARGE_FINISH_FRAG_TAG = "wallet_recharge_finish_fragment_tag";
    public static final String WALLET_TRANSACTIONS_FRAG_TAG = "wallet_transactions_fragment_tag";
    public static final String WALLET_CREDIT_TRANSFER_FRAG_TAG = "wallet_credit_transfer_fragment_tag";
    public static final String WALLET_CREDIT_TRANSFER_DETAILS_FRAG_TAG = "wallet_credit_transfer_details_fragment_tag";
    public static final String WALLET_CONVERT_POINTS_FINISH_FRAG_TAG = "wallet_convert_points_finish_fragment_tag";
    public static final String WALLET_CONVERT_POINTS_FRAG_TAG = "wallet_convert_points_fragment_tag";
    public static final String WALLET_BUY_SUBWALLET_FRAG_TAG = "WALLET_BUY_SUBWALLET_FRAG_TAG";
    public static final String WALLET_ADD_SUBWALLET_FRAG_TAG = "WALLET_ADD_SUBWALLET_FRAG_TAG";
    public static final String WALLET_SUBWALLET_FRAG_TAG = "WALLET_SUBWALLET_FRAG_TAG";
    public static final String WALLET_INVITE_SUBWALLET_FRAG_TAG = "WALLET_INVITE_SUBWALLET_FRAG_TAG";
    public static final String WALLET_SUBWALLET_INVITATIONS_FRAG_TAG = "WALLET_SUBWALLET_INVITATIONS_FRAG_TAG";


    public static final String WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION = "package_redemption";

    public static final String WALLET_TRANSACTION_TYPE_PACKAGE_PURCHASE = "package_purchase";
    public static final String WALLET_TRANSACTION_TYPE_CHARGE = "charge";
    public static final String WALLET_TRANSACTION_TYPE_GAS_FILL = "gas_fill";
    public static final String WALLET_TRANSACTION_TYPE_TRANSFER_TO = "transfer_to";
    public static final String WALLET_TRANSACTION_TYPE_TRANSFER_FROM = "transfer_from";
    public static final String WALLET_TRANSACTION_TYPE_POINTS_REDEMPTION = "points_redemption";
    public static final String WALLET_TRANSACTION_TYPE_REFUND = "refund";

    public static final int WALLET_TRANSACTION_TYPE_PACKAGE_PURCHASE_ID = 0;
    public static final int WALLET_TRANSACTION_TYPE_CHARGE_ID = 1;
    public static final int WALLET_TRANSACTION_TYPE_GAS_FILL_ID = 2;
    public static final int WALLET_TRANSACTION_TYPE_TRANSFER_TO_ID = 3;
    public static final int WALLET_TRANSACTION_TYPE_TRANSFER_FROM_ID = 4;
    public static final int WALLET_TRANSACTION_TYPE_POINTS_REDEMPTION_ID = 5;
    public static final int WALLET_TRANSACTION_TYPE_REFUND_ID = 6;
    public static final int WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION_ID = 7;
}
