package com.android.fuel.application;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.android.fuel.beans.UserPreferences;
import com.android.fuel.utils.SavePrefs;
import com.android.fuel.utils.Utils;

/**
 * Created by beshoy adel on 12/16/2017.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        SavePrefs<UserPreferences> userPreferencesSavePrefs = new SavePrefs<>(this,UserPreferences.class);
        UserPreferences userPreferences = userPreferencesSavePrefs.load();
        if (userPreferences!=null)
            Utils.updateLanguage(this, userPreferences.getLanguage().getLocale());
    }

}
