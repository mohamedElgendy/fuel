package com.android.fuel.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.android.fuel.beans.GasCompany;
import com.android.fuel.beans.Station;
import com.android.fuel.beans.UserPreferences;
import com.android.fuel.beans.Wallet;
import com.android.fuel.utils.SavePrefs;
import com.android.fuel.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Mohamed Elgendy.
 */

public class FuelApp extends MultiDexApplication {

    private static FuelApp instance;
    private static Station currentStation;
    private static Wallet currentWallet;
    private static ArrayList<GasCompany> currentFuelCompaniesList;

    public static FuelApp getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Something went horribly wrong!!, no application attached!");
        }
        return instance;
    }

    public static Wallet getCurrentWallet() {
        return currentWallet;
    }

    public static void setCurrentWallet(Wallet currentWallet) {
        FuelApp.currentWallet = currentWallet;
    }

    public static ArrayList<GasCompany> getCurrentFuelCompaniesList() {
        return currentFuelCompaniesList;
    }

    public static void setCurrentFuelCompaniesList(ArrayList<GasCompany> currentFuelCompaniesList) {
        FuelApp.currentFuelCompaniesList = currentFuelCompaniesList;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Station getCurrentStation() {
        return currentStation;
    }

    public static void setCurrentStation(Station currentStation) {
        FuelApp.currentStation = currentStation;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        SavePrefs<UserPreferences> userPreferencesSavePrefs = new SavePrefs<>(this,UserPreferences.class);
        UserPreferences userPreferences = userPreferencesSavePrefs.load();
        if (userPreferences!=null)
            Utils.updateLanguage(this, userPreferences.getLanguage().getLocale());

    }
}

