package com.android.fuel.main.filter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.android.fuel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class StationSortFragment extends Fragment {

    @BindView(R.id.checkBox_nearest)
    CheckBox mNearestCheckBox;

    @BindView(R.id.checkBox_price_highest)
    CheckBox mHighestPriceCheckBox;

    @BindView(R.id.checkBox_price_lowest)
    CheckBox mLowestPriceCheckBox;

    @BindView(R.id.checkBox_stars_highest)
    CheckBox mHighestStarsCheckBox;

    @BindView(R.id.checkBox_stars_lowest)
    CheckBox mLowestStarsCheckBox;

    @BindView(R.id.checkBox_names_az)
    CheckBox mAscendingNamesCheckBox;

    @BindView(R.id.checkBox_names_za)
    CheckBox mDescendingNamesCheckBox;


    public StationSortFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_station_sort, container, false);
        ButterKnife.bind(this, rootView);

        if (getArguments().containsKey("sortQuery")) {
            String query = getArguments().getString("sortQuery");

            if (query.contains("NEAREST") || query.contains("near"))
                mNearestCheckBox.setChecked(true);

            if (query.contains("pricePerDay") && query.contains("ASC"))
                mLowestPriceCheckBox.setChecked(true);

            if (query.contains("pricePerDay") && query.contains("DESC"))
                mHighestPriceCheckBox.setChecked(true);

            if (query.contains("rating") && query.contains("ASC"))
                mLowestStarsCheckBox.setChecked(true);

            if (query.contains("rating") && query.contains("DESC"))
                mHighestStarsCheckBox.setChecked(true);

            if (query.contains("title") && query.contains("DESC"))
                mDescendingNamesCheckBox.setChecked(true);
            else if (query.contains("title"))
                mAscendingNamesCheckBox.setChecked(true);
        }

        return rootView;
    }

    @OnClick(R.id.checkBox_nearest)
    void nearest() {
        unCheckAll();
        mNearestCheckBox.setChecked(true);

        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(1));
        }
    }

    @OnClick(R.id.checkBox_price_highest)
    void highestPrice() {
        unCheckAll();
        mHighestPriceCheckBox.setChecked(true);

        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(3));
        }
    }

    @OnClick(R.id.checkBox_price_lowest)
    void lowestPrice() {
        unCheckAll();
        mLowestPriceCheckBox.setChecked(true);
        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(2));
        }
    }

    @OnClick(R.id.checkBox_stars_highest)
    void highestStars() {
        unCheckAll();
        mHighestStarsCheckBox.setChecked(true);
        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(5));
        }
    }

    @OnClick(R.id.checkBox_stars_lowest)
    void lowestStars() {
        unCheckAll();
        mLowestStarsCheckBox.setChecked(true);
        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(4));
        }
    }

    @OnClick(R.id.checkBox_names_az)
    void ascendingNames() {
        unCheckAll();
        mAscendingNamesCheckBox.setChecked(true);
        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(6));
        }
    }

    @OnClick(R.id.checkBox_names_za)
    void descendingNames() {
        unCheckAll();
        mDescendingNamesCheckBox.setChecked(true);
        if (getActivity() instanceof FilterActivity)
        {
            ((FilterActivity) getActivity()).setSortQuery(getOrderParam(7));
        }
    }

    private void unCheckAll()
    {
        mAscendingNamesCheckBox.setChecked(false);
        mDescendingNamesCheckBox.setChecked(false);
        mHighestPriceCheckBox.setChecked(false);
        mLowestPriceCheckBox.setChecked(false);
        mLowestStarsCheckBox.setChecked(false);
        mHighestStarsCheckBox.setChecked(false);
        mNearestCheckBox.setChecked(false);
    }

    private String getOrderParam(int sortValue) {

        if (sortValue ==1)//Nearest
        {
            return "NEAREST";
        }
        else  if (sortValue ==2)//Price low-high
            return "filter[order]=pricePerDay ASC";
        else  if (sortValue ==3)//Price high-low
            return "filter[order]=pricePerDay DESC";
        else  if (sortValue ==4)//Stars 1-5
            return "filter[order]=rating ASC";
        else  if (sortValue ==5)//Stars 5-1
            return "filter[order]=rating DESC";
        else  if (sortValue ==6)//Name a-z
            return "filter[order]=title";
        else  if (sortValue ==7)//Name z-a
            return "filter[order]=title DESC";

        return "";
    }

}
