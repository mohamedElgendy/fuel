package com.android.fuel.main.fragments.wallets.rechargewallet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.fuel.R;
import com.android.fuel.beans.ChargePackage;
import com.android.fuel.main.fragments.wallets.rechargewallet.adapter.PackageListAdapter;
import com.android.fuel.responses.GetPackagesResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class RechargePackageListFragment extends Fragment implements PackageListAdapter.PackageClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.wallet_packages_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;

    // declare station list components
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ChargePackage> mPackagesArrayList = new ArrayList<>();;

    public RechargePackageListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list_recharge_packages, container, false);
        ButterKnife.bind(this, mRootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.recharge_wallet));
        }



        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        // initialize views
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PackageListAdapter(mPackagesArrayList,this);
        mRecyclerView.setAdapter(mAdapter);

        getStations();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getStations() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Packages",
                GetPackagesResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetPackagesResponse)
                {
                    GetPackagesResponse stationsResponse = (GetPackagesResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mPackagesArrayList.clear();
                        mPackagesArrayList.addAll(stationsResponse.getItems());
                        mAdapter.notifyDataSetChanged();
                    }
                    else
                        Utils.showToast(getActivity(),R.string.no_station_result);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getStations();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

    @Override
    public void onPackageClick(int position, View v) {

        FuelManager.openPackageDetails(getActivity(),mPackagesArrayList.get(position));
        getActivity().finish();
    }


//    @OnClick(R.id.bt_activate)
//    void activate() {
//    }

}

