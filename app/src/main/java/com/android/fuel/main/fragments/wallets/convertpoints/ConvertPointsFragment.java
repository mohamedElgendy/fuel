package com.android.fuel.main.fragments.wallets.convertpoints;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.Station;
import com.android.fuel.main.MainActivity;
import com.android.fuel.responses.FillFuelResponse;
import com.android.fuel.responses.GetTransferCreditResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GAS_FILL;

public class ConvertPointsFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_min_points )
    TextView minCostTextView;

    @BindView(R.id.tv_max_points)
    TextView maxCostTextView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.seekBar_points)
    SeekBar mPointsSeekBar;


    private View rootView;

    public ConvertPointsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_convert_points, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.reward));
        }


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPointsSeekBar.setMax(FuelApp.getCurrentWallet().getPoints());

        maxCostTextView.setText(FuelApp.getCurrentWallet().getPoints()+"");

        mPointsSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                minCostTextView.setText(i+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rootView.findViewById(R.id.bt_convert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                convertNow();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void convertNow() {

        final RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+FuelApp.getCurrentWallet().getId()+"/pointsRedemtion/"+mPointsSeekBar.getProgress(),
                GetTransferCreditResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetTransferCreditResponse)
                {
                    mProgressBar.setVisibility(View.GONE);
                    if (response instanceof GetTransferCreditResponse) {

                        GetTransferCreditResponse creditResponse = (GetTransferCreditResponse)response;
                        float totalBalance = FuelApp.getCurrentWallet().getBalance()+creditResponse.getRes().getBalance();
                        int totalPoints = FuelApp.getCurrentWallet().getPoints()+creditResponse.getRes().getPoints();
                        FuelManager.openConvertPointsFinish(getActivity(),totalBalance,totalPoints);
                        getActivity().finish();
                    }
                    else
                        Utils.showToast(getActivity(),R.string.internet_error);
                }
                else
                    Utils.showToast(getActivity(),R.string.no_enough_balance);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                rootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        convertNow();
                    }
                });
                rootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, -1);
        requestHelper.executeFormUrlEncoded();

    }
}
