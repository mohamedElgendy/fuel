package com.android.fuel.main.filter;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.container)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    private String filterQuery="";

    private String sortQuery="";

    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("sortQuery"))
            sortQuery = getIntent().getStringExtra("sortQuery");

        if (getIntent().hasExtra("filterQuery"))
            filterQuery = getIntent().getStringExtra("filterQuery");

        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),sortQuery,filterQuery);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //remove toolbar title
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private String sortQuery;
        private String filterQuery;

        public SectionsPagerAdapter(FragmentManager fm,String sortQuery,String filterquery) {
            super(fm);

            this.filterQuery=filterquery;
            this.sortQuery=sortQuery;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch (position) {
                case 0:
                    StationSortFragment  stationSortFragment =  new StationSortFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("sortQuery",sortQuery);
                    stationSortFragment.setArguments(bundle);
                    return stationSortFragment;
                case 1:
                    StationFilterFragment  stationFilterFragment =  new StationFilterFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("filterQuery",filterQuery);
                    stationFilterFragment.setArguments(bundle2);
                    return stationFilterFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.sort);
                case 1:
                    return getResources().getString(R.string.filter);
            }
            return null;
        }
    }

    public void setFilterQuery(String query)
    {
        filterQuery = query;
    }

    public void setSortQuery(String query)
    {
        sortQuery = query.replaceAll(" ", "%20");;
    }

    @Override
    public void onBackPressed() {

        if (filterQuery.equals("") && sortQuery.equals(""))
        {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("sortQuery",sortQuery);
            returnIntent.putExtra("filterQuery",filterQuery);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }
}
