package com.android.fuel.main.fragments.invoices.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelConstants;
import com.android.fuel.beans.DriverInvoice;
import com.android.fuel.beans.WalletTransaction;
import com.android.fuel.main.FillFuelDoneActivity;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class InvociesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<DriverInvoice> mDataSet;
    private Context context;

    public InvociesAdapter(ArrayList<DriverInvoice> mDataSet) {
        this.mDataSet = mDataSet;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        context = parent.getContext();

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invoice, parent, false);
        DataObjectChargeHolder dataObjectHolder = new DataObjectChargeHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        DriverInvoice mItem = mDataSet.get(position);

        DataObjectChargeHolder chargeHolder = (DataObjectChargeHolder)holder;

        chargeHolder.mFuelTypeTextView.setText(mItem.getGasType());
        chargeHolder.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
        chargeHolder.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
        chargeHolder.mLiterSarTextView.setText(mItem.getGasFill().getLiters()+" "+context.getString(R.string.litre)+" - "+mItem.getTotal()+" "+context.getString(R.string.sar));

        if (mItem.getGasCompany()!=null)
        {
            if (mItem.getGasCompany().get_image()!=null && mItem.getGasCompany().get_image().getUrl()!=null &&
                    !Utils.isEmptyString(mItem.getGasCompany().get_image().getUrl()))
            {
                Picasso.with(context).load(mItem.getGasCompany().get_image().getUrl())
                        .placeholder(R.drawable.avatar_icon).fit().into(chargeHolder.mStationImageView);
            }
        }
        chargeHolder.mStationNameTextView.setText(mItem.getGasCompany().getName()+" "+mItem.getGasStation().getAddress());

        chargeHolder.mRefundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FuelManager.openUpdateFillFuel(context,mDataSet.get(position).getGasFillId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


    public static class DataObjectChargeHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.tv_fuel_type)
        TextView mFuelTypeTextView;
        @BindView(R.id.tv_station_name)
        TextView mStationNameTextView;
        @BindView(R.id.tv_date)
        TextView mDateTextView;
        @BindView(R.id.tv_time)
        TextView mTimeTextView;
        @BindView(R.id.iv_station)
        ImageView mStationImageView;
        @BindView(R.id.tv_liters_sar)
        TextView mLiterSarTextView;

        @BindView(R.id.btn_refund)
        TextView mRefundButton;

        public DataObjectChargeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
