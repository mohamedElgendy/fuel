package com.android.fuel.main.fragments.wallets.rechargewallet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.ChargePackage;
import com.android.fuel.beans.Wallet;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetBuyPackageResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.fuel.application.FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION;
import static com.android.fuel.services.Parameters.SERVICE_RECHARGE;

/**
 * Created by Mohamed Elgendy.
 */

public class PackageDetailsFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_package_name)
    TextView mNameTextView;

    @BindView(R.id.tv_package_number)
    TextView mBalanceTextView;

    @BindView(R.id.tv_package_points)
    TextView mPoints5TextView;

    @BindView(R.id.tv_package_price)
    TextView mPriceTextView;

    @BindView(R.id.bt_update)
    Button mBuyButton;

    private ChargePackage mPackage;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;

    public PackageDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_package_details, container, false);
        ButterKnife.bind(this, mRootView);

        mPackage = new Gson().fromJson(getArguments().getString("package"), ChargePackage.class);

        mBalanceTextView.setText(((int)mPackage.getBalance()) + "");
        mNameTextView.setText(mPackage.getName() + "");
        mPoints5TextView.setText(mPackage.getPoints() + "");
        mPriceTextView.setText(mPackage.getPrice() + " " + getString(R.string.sar));


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(mPackage.getName());
        }



        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.bt_update)
    void buyNow() {

        buy();
    }


    private void buy() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+FuelApp.getCurrentWallet().getId()+"/purchasePackage/"+mPackage.getId(),
                GetBuyPackageResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetBuyPackageResponse)
                {
                    GetBuyPackageResponse getBuyPackageResponse =(GetBuyPackageResponse) response;

                    if (getBuyPackageResponse.getRes()!=null)
                    {
                        for (int i=0;i<getBuyPackageResponse.getRes().size();i++)
                        {
                            if (getBuyPackageResponse.getRes().get(i).getType().equals(WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION))
                            {
                                float totalBalance = FuelApp.getCurrentWallet().getBalance()+getBuyPackageResponse.getRes().get(i).getBalance();
                                int totalPoints = FuelApp.getCurrentWallet().getPoints()+getBuyPackageResponse.getRes().get(i).getPoints();
                                FuelManager.openRechargeFinish(getActivity(),totalBalance,totalPoints);
                                getActivity().finish();
                            }
                        }
                    }
                }
                else
                {
                    if (response instanceof GeneralResponse && ((GeneralResponse) response).getId().equals("401"))
                    {
                        Utils.showToast(getActivity(),R.string.no_enough_balance);
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        buy();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_RECHARGE);
        requestHelper.executeFormUrlEncoded();
    }

}


