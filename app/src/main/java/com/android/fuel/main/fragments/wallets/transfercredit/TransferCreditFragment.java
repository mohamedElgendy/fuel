package com.android.fuel.main.fragments.wallets.transfercredit;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.User;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class TransferCreditFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.seekBar_transfer_credit)
    SeekBar mTransferSeekBar;

    @BindView(R.id.textView31)
    TextView mBalanceTextView;

    @BindView(R.id.tv_cost_max_value)
    TextView mMaxBalanceTextView;

    @BindView(R.id.et_identity)
    EditText mIdentityEditText;

    @BindView(R.id.bt_transfer)
    Button mTransferButton;

    LinearLayout thumbView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;

    public TransferCreditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_transfer_credit, container, false);
        ButterKnife.bind(this, mRootView);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.transfer_credit));
        }


        thumbView = (LinearLayout) inflater.inflate(R.layout.layout_seekbar_thumb, container, false);
        mTransferSeekBar.setThumb(getThumb(1));

        mTransferSeekBar.setMax((int)FuelApp.getCurrentWallet().getBalance());
        mMaxBalanceTextView.setText(FuelApp.getCurrentWallet().getBalance()+"");
        mBalanceTextView.setText(FuelApp.getCurrentWallet().getBalance()+"");


        return mRootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mTransferSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                // You can have your own calculation for progress
                seekBar.setThumb(getThumb(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public Drawable getThumb(int progress) {
        ((TextView) thumbView.findViewById(R.id.tvProgress)).setText(progress + "");

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(getResources(), bitmap);
    }

    @OnClick(R.id.bt_transfer)
    void transferCredit() {

        attempt();
    }

    private void attempt() {

        // Reset errors.
        mIdentityEditText.setError(null);

        // Store values at the time of the login attempt.
        String identity = mIdentityEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(identity)) {
            mIdentityEditText.setError(getString(R.string.error_field_required));
            focusView = mIdentityEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            transfer();
        }
    }

    private void transfer() {

        if (mTransferSeekBar.getProgress()==0)
        {
            Utils.showToast(getActivity(),R.string.plz_transfer_balance);
            return;
        }

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/search?q="+mIdentityEditText.getText().toString().replaceAll(" ", "%20"),
                User.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof User)
                {
                    User user = (User) response;
                    if (user.getId().equals("400"))
                        Utils.showToast(getActivity(),R.string.no_found_user);
                    else
                    {
                        FuelManager.openTransferWalletCreditDetails(getActivity(),mTransferSeekBar.getProgress(),user);
                        getActivity().finish();
                    }
                }
                else
                    Utils.showToast(getActivity(),R.string.no_found_user);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        transfer();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);

            }
        }, Parameters.SERVICE_GET_DRIVER);
        requestHelper.executeGet();
    }

}