package com.android.fuel.main.fragments.wallets.rechargewallet.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.beans.ChargePackage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class PackageListAdapter extends RecyclerView.Adapter<PackageListAdapter.DataObjectHolder> {


    private ArrayList<ChargePackage> mDataSet;
    private Context context;
    private static PackageClickListener mPackageClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_package_name)
        TextView mNameTextView;

        @BindView(R.id.tv_package_number)
        TextView mBalanceTextView;

        @BindView(R.id.tv_package_points)
        TextView mPoints5TextView;

        @BindView(R.id.tv_package_price)
        TextView mPriceTextView;


        //fill out rest of views

        public DataObjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            //adding listener...
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mPackageClickListener.onPackageClick(getAdapterPosition(), v);
        }
    }

    public PackageListAdapter(ArrayList<ChargePackage> mDataSet, PackageClickListener mPackageClickListener) {
        this.mDataSet = mDataSet;
        this.mPackageClickListener = mPackageClickListener;
    }

    @Override
    public PackageListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_recharge_package, parent, false);
        context = parent.getContext();

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(PackageListAdapter.DataObjectHolder holder, int position) {

        ChargePackage mItem = mDataSet.get(position);

        holder.mBalanceTextView.setText(((int)mItem.getBalance())+"");
        holder.mNameTextView.setText(mItem.getName()+"");
        holder.mPoints5TextView.setText(mItem.getPoints()+"");
        holder.mPriceTextView.setText(mItem.getPrice()+" "+context.getString(R.string.sar));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface PackageClickListener {
        void onPackageClick(int position, View v);
    }
}
