package com.android.fuel.main.fragments.wallets.wallettransactions.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelConstants;
import com.android.fuel.beans.ChargePackage;
import com.android.fuel.beans.WalletTransaction;
import com.android.fuel.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class WalletTransactionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<WalletTransaction> mDataSet;
    private Context context;

    public WalletTransactionsAdapter(ArrayList<WalletTransaction> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDataSet.get(position).getType()) {
            case FuelConstants.WALLET_TRANSACTION_TYPE_CHARGE:
                return FuelConstants.WALLET_TRANSACTION_TYPE_CHARGE_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_GAS_FILL:
                return FuelConstants.WALLET_TRANSACTION_TYPE_GAS_FILL_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_PURCHASE:
                return FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_PURCHASE_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_POINTS_REDEMPTION:
                return FuelConstants.WALLET_TRANSACTION_TYPE_POINTS_REDEMPTION_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_REFUND:
                return FuelConstants.WALLET_TRANSACTION_TYPE_REFUND_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_FROM:
                return FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_FROM_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_TO:
                return FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_TO_ID;

            case FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION:
                return FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION_ID;
        }

        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        context = parent.getContext();
        switch (viewType) {
            case FuelConstants.WALLET_TRANSACTION_TYPE_CHARGE_ID:
            case FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_PURCHASE_ID:
            case FuelConstants.WALLET_TRANSACTION_TYPE_REFUND_ID:
            case FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_FROM_ID:

                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trans_charge, parent, false);
                DataObjectChargeHolder dataObjectHolder = new DataObjectChargeHolder(view);
                return dataObjectHolder;

            case FuelConstants.WALLET_TRANSACTION_TYPE_POINTS_REDEMPTION_ID:
            case FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_TO_ID:
            case FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION_ID:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trans_transfer_to, parent, false);
                DataObjectTransferToHolder dataObjectHolder2 = new DataObjectTransferToHolder(view);
                return dataObjectHolder2;

            case FuelConstants.WALLET_TRANSACTION_TYPE_GAS_FILL_ID:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trans_gas_fill, parent, false);
                DataObjectGasFillHolder dataObjectHolder3 = new DataObjectGasFillHolder(view);
                return dataObjectHolder3;

        }

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trans_charge, parent, false);
        DataObjectChargeHolder dataObjectHolder = new DataObjectChargeHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        WalletTransaction mItem = mDataSet.get(position);

        switch (getItemViewType(position))
        {
            case FuelConstants.WALLET_TRANSACTION_TYPE_CHARGE_ID:
                DataObjectChargeHolder chargeHolder = (DataObjectChargeHolder)holder;
                chargeHolder.mArrowImageView.setImageResource(R.drawable.creditor_arrow);
                chargeHolder.mTypeTextView.setText(context.getString(R.string.wallet_type_charge));
                chargeHolder.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                chargeHolder.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                chargeHolder.mCreditTextView.setText(mItem.getBalance()+" "+context.getString(R.string.sar));
                break;
            case FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_PURCHASE_ID:
                DataObjectChargeHolder chargeHolder2 = (DataObjectChargeHolder)holder;
                chargeHolder2.mArrowImageView.setImageResource(R.drawable.creditor_arrow);
                chargeHolder2.mTypeTextView.setText(context.getString(R.string.wallet_type_charge));
                chargeHolder2.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                chargeHolder2.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                if (mItem.getChargePackage()!=null)
                    chargeHolder2.mCreditTextView.setText(mItem.getChargePackage().getBalance()+" "+context.getString(R.string.sar));
                break;
            case FuelConstants.WALLET_TRANSACTION_TYPE_REFUND_ID:
                DataObjectChargeHolder chargeHolder3 = (DataObjectChargeHolder)holder;
                chargeHolder3.mArrowImageView.setImageResource(R.drawable.creditor_arrow);
                chargeHolder3.mTypeTextView.setText(context.getString(R.string.wallet_type_refund));
                chargeHolder3.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                chargeHolder3.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                chargeHolder3.mCreditTextView.setText(mItem.getBalance()+" "+context.getString(R.string.sar));
                break;
            case FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_FROM_ID:
                DataObjectChargeHolder chargeHolder4 = (DataObjectChargeHolder)holder;
                chargeHolder4.mArrowImageView.setImageResource(R.drawable.creditor_arrow);
                chargeHolder4.mTypeTextView.setText(context.getString(R.string.wallet_type_transfer_from)+" "+mItem.getToWallet().getOwner().getName());
                chargeHolder4.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                chargeHolder4.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                chargeHolder4.mCreditTextView.setText((mItem.getBalance()*-1)+" "+context.getString(R.string.sar));
                break;

            case FuelConstants.WALLET_TRANSACTION_TYPE_GAS_FILL_ID:
                DataObjectGasFillHolder gasFillHolder = (DataObjectGasFillHolder)holder;
                gasFillHolder.mArrowImageView.setImageResource(R.drawable.debtor_arrow);
                gasFillHolder.mTypeTextView.setText(context.getString(R.string.wallet_type_gas_fill));
                gasFillHolder.mStationTextView.setVisibility(View.GONE);
                gasFillHolder.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                gasFillHolder.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));

                float balance = (mItem.getBalance()*-1);
                if (String.valueOf(balance).length()>3)
                    gasFillHolder.mCreditTextView.setText(String.format("%.2f",balance));
                else
                    gasFillHolder.mCreditTextView.setText(balance+" "+context.getString(R.string.sar));
                break;

            case FuelConstants.WALLET_TRANSACTION_TYPE_TRANSFER_TO_ID:
                DataObjectTransferToHolder transferToHolder = (DataObjectTransferToHolder)holder;
                transferToHolder.mArrowImageView.setImageResource(R.drawable.debtor_arrow);
                transferToHolder.mTypeTextView.setText(context.getString(R.string.wallet_type_transfer_to));
                transferToHolder.mTransferToTextView.setText(context.getString(R.string.to_str)+" "+mItem.getFromWallet().getOwner().getName());
                transferToHolder.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                transferToHolder.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                transferToHolder.mCreditTextView.setText(mItem.getBalance()+" "+context.getString(R.string.sar));
                break;

            case FuelConstants.WALLET_TRANSACTION_TYPE_POINTS_REDEMPTION_ID:
                DataObjectTransferToHolder transferToHolder2 = (DataObjectTransferToHolder)holder;
                transferToHolder2.mArrowImageView.setImageResource(R.drawable.creditor_arrow);
                transferToHolder2.mTypeTextView.setText(context.getString(R.string.wallet_type_points_redemption));
                transferToHolder2.mTransferToTextView.setText((mItem.getPoints()*-1)+" "+context.getString(R.string.points));
                transferToHolder2.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                transferToHolder2.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                float balance2 = mItem.getBalance();
                if (String.valueOf(balance2).length()>3)
                    transferToHolder2.mCreditTextView.setText(String.format("%.2f",balance2)+" "+context.getString(R.string.sar));
                else
                    transferToHolder2.mCreditTextView.setText(balance2+" "+context.getString(R.string.sar));
                break;

            case FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION_ID:
                DataObjectTransferToHolder transferToHolder3 = (DataObjectTransferToHolder)holder;
                transferToHolder3.mArrowImageView.setImageResource(R.drawable.creditor_arrow);
                transferToHolder3.mTypeTextView.setText(context.getString(R.string.wallet_type_buy_subwallet));
                transferToHolder3.mTransferToTextView.setText((mItem.getPoints()*-1)+" "+context.getString(R.string.points));
                transferToHolder3.mDateTextView.setText(Utils.getDate(mItem.getCreatedOn()));
                transferToHolder3.mTimeTextView.setText(Utils.getTime(mItem.getCreatedOn()));
                float balance3 = mItem.getBalance();
                if (String.valueOf(balance3).length()>3)
                    transferToHolder3.mCreditTextView.setText(String.format("%.2f",balance3)+" "+context.getString(R.string.sar));
                else
                    transferToHolder3.mCreditTextView.setText(balance3+" "+context.getString(R.string.sar));
                break;
        }


    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface PackageClickListener {
        void onPackageClick(int position, View v);
    }

    public static class DataObjectChargeHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.tv_type)
        TextView mTypeTextView;
        @BindView(R.id.tv_credit)
        TextView mCreditTextView;
        @BindView(R.id.tv_date)
        TextView mDateTextView;
        @BindView(R.id.tv_time)
        TextView mTimeTextView;
        @BindView(R.id.iv_arrow)
        ImageView mArrowImageView;

        public DataObjectChargeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class DataObjectGasFillHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.tv_type)
        TextView mTypeTextView;
        @BindView(R.id.tv_credit)
        TextView mCreditTextView;
        @BindView(R.id.tv_station_name)
        TextView mStationTextView;
        @BindView(R.id.tv_date)
        TextView mDateTextView;
        @BindView(R.id.tv_time)
        TextView mTimeTextView;
        @BindView(R.id.iv_arrow)
        ImageView mArrowImageView;

        public DataObjectGasFillHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public static class DataObjectTransferToHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.tv_type)
        TextView mTypeTextView;
        @BindView(R.id.tv_credit)
        TextView mCreditTextView;
        @BindView(R.id.tv_transfer_to)
        TextView mTransferToTextView;
        @BindView(R.id.tv_date)
        TextView mDateTextView;
        @BindView(R.id.tv_time)
        TextView mTimeTextView;
        @BindView(R.id.iv_arrow)
        ImageView mArrowImageView;

        public DataObjectTransferToHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
