package com.android.fuel.main.fragments.wallets.subwallet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.fuel.R;
import com.android.fuel.beans.DriverInvoice;
import com.android.fuel.beans.SubWalletInvitation;
import com.android.fuel.main.fragments.invoices.adapters.InvociesAdapter;
import com.android.fuel.main.fragments.wallets.subwallet.adapters.SubWalletInvitationAdapter;
import com.android.fuel.responses.GetInvoicesResponse;
import com.android.fuel.responses.GetSubWalletInvitationsResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class SubWalletsListFragment extends Fragment {

//    @BindView(R.id.constraintLayout)
//    ConstraintLayout mUserLayout;

    @BindView(R.id.wallet_packages_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private View mRootView;

    // declare station list components
    private SubWalletInvitationAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<SubWalletInvitation> mPackagesArrayList = new ArrayList<>();;


    public SubWalletsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_wallets_list, container, false);
        ButterKnife.bind(this, rootView);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.sub_wallets));
        }


        // initialize views
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SubWalletInvitationAdapter(mPackagesArrayList,getActivity());
        mRecyclerView.setAdapter(mAdapter);

        getStations();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Only if you need to restore open/close state when
        // the orientation is changed
        if (mAdapter != null) {
            mAdapter.saveStates(outState);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        // Only if you need to restore open/close state when
        // the orientation is changed
        if (mAdapter != null) {
            mAdapter.restoreStates(savedInstanceState);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getStations() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Drivers/"+AppConstants.getUserId(getActivity())+"/invitations",
                GetSubWalletInvitationsResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetSubWalletInvitationsResponse)
                {
                    GetSubWalletInvitationsResponse stationsResponse = (GetSubWalletInvitationsResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mPackagesArrayList.clear();
                        mPackagesArrayList.addAll(stationsResponse.getItems());
                        mAdapter.notifyDataSetChanged();
                    }
                    else {
                        Utils.showToast(getActivity(), R.string.no_sub_wallet);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getStations();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }
}

