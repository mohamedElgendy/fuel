package com.android.fuel.main.fragments.wallets.subwallet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.utils.FuelManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class SubWalletFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.constraintLayout)
    ConstraintLayout mUserLayout;

    @BindView(R.id.bt_add)
    Button mAddButton;

    public SubWalletFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_wallets, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.add_sub_wallets));
        }



        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.constraintLayout)
    void userLayout() {
        Toast.makeText(getActivity(), "mUserLayout Clicked", Toast.LENGTH_SHORT).show();
        // navigate to sub wallets list Fragment
    }

    @OnClick(R.id.bt_add)
    void addButton() {
        // navigate to add sub wallet fragment
        FuelManager.openAddSubWallet(getActivity());
        getActivity().finish();
    }

    @OnClick(R.id.bt_my_sub_invitations)
    void mySubWalletInvitations() {
        // navigate to add sub wallet fragment
        FuelManager.openSubWalletInvitations(getActivity());
        getActivity().finish();
    }

    @OnClick(R.id.bt_invite_driver)
    void inviteDriver() {
        // navigate to add sub wallet fragment
        FuelManager.openInviteToSubWallet(getActivity());
        getActivity().finish();
    }
}
