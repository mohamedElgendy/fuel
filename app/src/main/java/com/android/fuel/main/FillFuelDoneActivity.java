package com.android.fuel.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.android.fuel.R;
import com.android.fuel.application.BaseActivity;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.ReviewRequest;
import com.android.fuel.beans.Station;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FillFuelDoneActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_rate)
    Button mRateNowButton;

    @BindView(R.id.btn_rate_update)
    Button mRateAndUpdateButton;

    @BindView(R.id.rb_station)
    RatingBar mStationRatingBar;


    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.et_comment)
    EditText mCommentEditText;

    private Station mStation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_fuel_done);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //Add toolbar title
            getSupportActionBar().setTitle(getResources().getString(R.string.rate_now));
        }


        mStation = FuelApp.getCurrentStation();

        if (mStation==null)
            finish();

        setStationInfo();
    }

    private void setStationInfo() {

        if (mStation.getCompany()!=null)
        {
            //mStationNameTextView.setText(mStation.getCompany().getName());
            if (mStation.getCompany()!=null && mStation.getCompany().get_image()!=null && mStation.getCompany().get_image().getUrl()!=null &&
                    !Utils.isEmptyString(mStation.getCompany().get_image().getUrl()))
            {
                //Picasso.with(this).load(mStation.getCompany().get_image().getUrl())
                  //      .placeholder(R.drawable.avatar_icon).centerCrop().fit().into(mStationImageView);
            }
        }

        //mStationAddressTextView.setText(mStation.getTitle()+","+mStation.getAddress());
    }


    @OnClick(R.id.btn_rate)
    void rate() {

        if (TextUtils.isEmpty(mCommentEditText.getText().toString()))
        {
            Utils.showToast(this,R.string.error_field_required);
            return;
        }
        rateNow(false);
    }


    @OnClick(R.id.btn_rate_update)
    void rateAndUpdate() {
        if (TextUtils.isEmpty(mCommentEditText.getText().toString()))
        {
            Utils.showToast(this,R.string.error_field_required);
            return;
        }
        rateNow(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void rateNow(final boolean isUpdate)
    {
        mProgressBar.setVisibility(View.VISIBLE);
        //Call send message
        ReviewRequest reviewRequest = new ReviewRequest();
        reviewRequest.setRating((int)mStationRatingBar.getRating());

        if (!TextUtils.isEmpty(mCommentEditText.getText().toString()))
            reviewRequest.setReview(mCommentEditText.getText().toString());
        reviewRequest.setReviewerId(AppConstants.getUser(this).getId());

        RequestHelper<Object> requestHelper = new RequestHelper<>(this,
                Parameters.BASE_URL, "GasStations/" + mStation.getId() + "/reviews",
                GeneralResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                GeneralResponse generalResponse = (GeneralResponse)response;
                if (generalResponse.getId().equals("11000")) //Already review
                    Utils.showToast(FillFuelDoneActivity.this,R.string.rate_already_done);
                else
                    Utils.showToast(FillFuelDoneActivity.this,R.string.rate_done);

                if (isUpdate)
                    FuelManager.openUpdateFillFuel(FillFuelDoneActivity.this,getIntent().getStringExtra("gasFilledId"));

                finish();
            }

            @Override
            public void onFail(String message, String apiName) {
                Utils.showToast(FillFuelDoneActivity.this,message);
            }
        }, Parameters.SERVICE_RATE_STATION);

        requestHelper.executeFromRawJson(new Gson().toJson(reviewRequest));

    }

}
