package com.android.fuel.main.fragments.fillfuel;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.Station;
import com.android.fuel.beans.Wallet;
import com.android.fuel.main.MainActivity;
import com.android.fuel.requests.FillFuelRequest;
import com.android.fuel.responses.FillFuelResponse;
import com.android.fuel.responses.GetBannersResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.fuel.services.Parameters.SERVICE_GAS_FILL;
import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class FillFuelFragment extends Fragment {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_station_image)
    ImageView mStationImageView;

    @BindView(R.id.tv_station_name)
    TextView mStationNameTextView;

    @BindView(R.id.radio_fuel_91)
    RadioButton mFuel91RadioButton;

    @BindView(R.id.radio_fuel_95)
    RadioButton mFuel95RadioButton;

    @BindView(R.id.radio_fuel_diesel)
    RadioButton mFuelDieselRadioButton;

    @BindView(R.id.seekBar_fuel_liters)
    SeekBar mFuelLitersSeekBar;

    @BindView(R.id.seekBar_fuel_cost)
    SeekBar mFuelCostSeekBar;

    @BindView(R.id.bt_fill_now)
    Button mFillNowButton;

    @BindView(R.id.tv_fuel_min_value)
    TextView minValueTextView;

    @BindView(R.id.tv_fuel_max_value)
    TextView maxValueTextView;

    @BindView(R.id.tv_cost_min_value)
    TextView minCostTextView;

    @BindView(R.id.tv_cost_max_value)
    TextView maxCostTextView;

    @BindView(R.id.textView14)
    TextView mLitresTextView;

    @BindView(R.id.textView15)
    TextView mTotalCostTextView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private Station mStation;

    private View rootView;

    public FillFuelFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.fragment_fill_fuel, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.fill_fuel));
        }


        initializeUi();

        if (getArguments()!=null && getArguments().containsKey("stationId"))
        {
            getStation(getArguments().getString("stationId"));
        }
        else
            checkCameraPermission();//Fol live

        //getStation("5a201316f6c44e002e8104d0");//For test

        return rootView;
    }

    private void checkCameraPermission()
    {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1006);
            } else {
                FuelManager.openQrCodeReader(this, 1002);
            }
        }
        else
            FuelManager.openQrCodeReader(this, 1002);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1006:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    FuelManager.openQrCodeReader(this, 1002);
                } else {
                    // Permission Denied
                    if (getActivity() instanceof MainActivity)
                    {
                        ((MainActivity)getActivity()).setCurrentFragment(R.id.action_find_station);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1002) {
            if(resultCode == Activity.RESULT_OK){
                String stationId=data.getStringExtra("stationId");
                //Utils.showToast(getActivity(),stationId);
                getStation(stationId);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                if (getActivity() instanceof MainActivity)
                {
                    ((MainActivity)getActivity()).setCurrentFragment(R.id.action_find_station);
                }
            }
        }
    }

    int stepSize = 2;
    private void initializeUi() {
        //mFuelLitersSeekBar.setMax(100);
        //mFuelCostSeekBar.setMax(310);

        mFuelLitersSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (b) {
                    float cost = (seekBar.getProgress() * getCurrentFuelTypeCost());

                    mFuelCostSeekBar.setProgress((int)cost);

                    mLitresTextView.setText(seekBar.getProgress()+"");

                    if (String.valueOf(cost).length() > 3)
                        mTotalCostTextView.setText(String.format("%.2f", cost));
                    else
                        mTotalCostTextView.setText(cost + "");
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });

        mFuelCostSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if (b)
                {
                    int diesl = (int)(mStation.getDieselPrice()*100);
                    if(seekBar.getProgress()>=diesl && mFuelDieselRadioButton.isChecked())
                    {
                        seekBar.setProgress(diesl);
                    }
                    else
                    {
                        float litres = (seekBar.getProgress()/getCurrentFuelTypeCost());
                        if (String.valueOf(litres).length()>3)
                            mLitresTextView.setText(String.format("%.2f",litres));
                        else
                            mLitresTextView.setText(litres+"");

                        mFuelLitersSeekBar.setProgress((int)litres);

                        mTotalCostTextView.setText(seekBar.getProgress()+"");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private float getCurrentFuelTypeCost() {

        if (mFuel91RadioButton.isChecked())
            return mStation.getGas91Price();

        if (mFuel95RadioButton.isChecked())
            return mStation.getGas95Price();

        if (mFuelDieselRadioButton.isChecked())
            return mStation.getDieselPrice();

        return 0;
    }

    private String getCurrentFuelType() {

        if (mFuel91RadioButton.isChecked())
            return "91";

        if (mFuel95RadioButton.isChecked())
            return "95";

        if (mFuelDieselRadioButton.isChecked())
            return "diesel";

        return "91";
    }

    @OnClick(R.id.radio_fuel_91)
    void fuel91() {
        setSeekBarMax(91);
    }

    @OnClick(R.id.radio_fuel_95)
    void fuel95() {
        setSeekBarMax(95);
    }

    @OnClick(R.id.radio_fuel_diesel)
    void fuelDiesel() {
        setSeekBarMax(90);
    }

    private void setSeekBarMax(int fuelType) {

        mFuelLitersSeekBar.setProgress(0);
        mFuelCostSeekBar.setProgress(0);
        if (fuelType==90)
        {
            mFuelLitersSeekBar.setMax(100);
            mFuelCostSeekBar.setMax(100);//Because max at leat should be 100

            maxCostTextView.setText(mStation.getDieselPrice()*100+"");
        }
        else if (fuelType==91)
        {
            mFuelLitersSeekBar.setMax(100);
            mFuelCostSeekBar.setMax((int)mStation.getGas91Price()*100);
            maxCostTextView.setText(mStation.getGas91Price()*100+"");
        }
        else
        {
            mFuelLitersSeekBar.setMax(100);
            mFuelCostSeekBar.setMax((int)mStation.getGas95Price()*100);
            maxCostTextView.setText(mStation.getGas95Price()*100+"");
        }
    }

    @OnClick(R.id.bt_fill_now)
    void fillNow() {

        //go to fill  order result fragment
        if (mFuelLitersSeekBar.getProgress()>0)
        {
            Wallet userWallet = FuelApp.getCurrentWallet();
            if (userWallet!=null)
            {
                fillFuelNow(userWallet);
            }
            else
                getUserWallet();//In case opened from BottomNavigation in HomeActivity
        }
        else
            Utils.showToast(getActivity(),R.string.enter_litres);
    }

    private void getUserWallet() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+AppConstants.getUserId(getActivity())+"/wallet",
                Wallet.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof Wallet)
                {
                    Wallet userWallet = (Wallet) response;

                    if (userWallet!=null)
                    {
                        if (userWallet.getBalance()>=Float.valueOf(mTotalCostTextView.getText().toString()))
                        {
                            fillFuelNow(userWallet);
                        }
                        else
                            //fillFuelNow(userWallet);//For test
                            Utils.showToast(getActivity(),R.string.no_enough_balance);//For live
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper.executeGet();
    }

    private void fillFuelNow(Wallet userWallet) {

        mProgressBar.setVisibility(View.VISIBLE);


        final FillFuelRequest fillFuelRequest = new FillFuelRequest();
        fillFuelRequest.setDriverId(AppConstants.getUserId(getActivity()));
        fillFuelRequest.setGasStationId(mStation.getId());
        fillFuelRequest.setGasType(getCurrentFuelType());

        float liters = Float.valueOf(mLitresTextView.getText().toString());
        fillFuelRequest.setLiters((int)liters);


        fillFuelRequest.setWalletId(userWallet.getId());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "GasFills",
                FillFuelResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof FillFuelResponse)
                {
                    FillFuelResponse fillFuelResponse = (FillFuelResponse) response;

                    float cost = Float.valueOf(mTotalCostTextView.getText().toString());
                    fillFuelResponse.setCost(cost);

                    if (getActivity() instanceof MainActivity)
                    {
                        ((MainActivity)getActivity()).setCurrentFragment(R.id.action_find_station);
                    }
                    else
                        getActivity().finish();

                    FuelApp.setCurrentStation(mStation);
                    FuelManager.openOrderResult(getActivity(),fillFuelResponse);
                }
                else
                    Utils.showToast(getActivity(),R.string.no_enough_balance);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, SERVICE_GAS_FILL);
        requestHelper.executeFromRawJson(new Gson().toJson(fillFuelRequest));
    }


    private void getStation(String stationId) {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "GasStations/" + stationId+"?filter={\"include\":\"company\"}",
                Station.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof Station)
                {
                    mStation = (Station) response;

                    initView();
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper.executeGet();
    }

    private void initView() {

        if (mStation!=null)
        {
            boolean isAtLeastOne = false;

            if (mStation.isGas91Available()) {
                mFuel91RadioButton.setVisibility(View.VISIBLE);
                isAtLeastOne = true;
                mFuel91RadioButton.setChecked(true);
                setSeekBarMax(91);
            }
            else
                mFuel91RadioButton.setVisibility(View.GONE);

            if (mStation.isGas95Available()) {
                mFuel95RadioButton.setVisibility(View.VISIBLE);
                if (!isAtLeastOne)
                {
                    mFuel95RadioButton.setChecked(true);
                    setSeekBarMax(95);
                }
                isAtLeastOne = true;
            }
            else
                mFuel95RadioButton.setVisibility(View.GONE);

            if (mStation.isDieselAvailable()) {
                mFuelDieselRadioButton.setVisibility(View.VISIBLE);
                if (!isAtLeastOne)
                {
                    mFuelDieselRadioButton.setChecked(true);
                    setSeekBarMax(90);
                }
            }
            else
                mFuelDieselRadioButton.setVisibility(View.GONE);

            if (mStation.getCompany()!=null)
            {
                mStationNameTextView.setText(mStation.getCompany().getName()+","+mStation.getTitle());
                if (mStation.getCompany()!=null && mStation.getCompany().get_image()!=null && mStation.getCompany().get_image().getUrl()!=null &&
                        !Utils.isEmptyString(mStation.getCompany().get_image().getUrl()))
                {
                    Picasso.with(getContext()).load(mStation.getCompany().get_image().getUrl())
                            .placeholder(R.drawable.avatar_icon).centerCrop().fit().into(mStationImageView);
                }

            }

        }
    }


}
