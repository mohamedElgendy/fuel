package com.android.fuel.main.fragments.invoices;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.DriverInvoice;
import com.android.fuel.beans.GasCompany;
import com.android.fuel.main.filter.FilterInvoicesActivity;
import com.android.fuel.main.fragments.invoices.adapters.InvociesAdapter;
import com.android.fuel.responses.GetInvoicesResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class InvoicesFragment  extends Fragment {

    @BindView(R.id.wallet_packages_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String filterQuery = "";

    // declare station list components
    private InvociesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<DriverInvoice> mPackagesArrayList = new ArrayList<>();;

    public InvoicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.station_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter ) {
            //navigate to filter Activity
            if (mPackagesArrayList.size()>0)
                FuelApp.setCurrentFuelCompaniesList(getCurrentFuelCompanies());
            Intent filter = new Intent(getActivity(), FilterInvoicesActivity.class);
            if (!filterQuery.equals(""))
                filter.putExtra("filterQuery",filterQuery);
            startActivityForResult(filter,7777);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<GasCompany> getCurrentFuelCompanies() {

        ArrayList<GasCompany> gasCompanies = new ArrayList<>();
        for (int i=0;i<mPackagesArrayList.size();i++)
        {
            if (isNotAddedBefore(gasCompanies,mPackagesArrayList.get(i).getGasCompany()))
            {
                gasCompanies.add(mPackagesArrayList.get(i).getGasCompany());
            }
        }

        return gasCompanies;
    }

    private boolean isNotAddedBefore(ArrayList<GasCompany> gasCompanies,GasCompany gasCompany) {

        for (int i=0;i<gasCompanies.size();i++)
        {
            if (gasCompany.getId().equals(gasCompanies.get(i).getId()))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 7777) {
            if(resultCode == Activity.RESULT_OK){

                filterQuery=data.getStringExtra("filterQuery");
                mPackagesArrayList.clear();
                mAdapter.notifyDataSetChanged();

                String allQuery="";

                if (filterQuery!=null && !filterQuery.equals(""))
                    allQuery+="&"+filterQuery;

                getStations(allQuery);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_invoices, container, false);
        ButterKnife.bind(this, mRootView);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        //remove toolbar title
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        // initialize views
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new InvociesAdapter(mPackagesArrayList);
        mRecyclerView.setAdapter(mAdapter);

        getStations(filterQuery);
    }

    private void getStations(String query) {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Drivers/"+ AppConstants.getUserId(getActivity())+"/driverInvoices?"+query,
                GetInvoicesResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetInvoicesResponse)
                {
                    GetInvoicesResponse stationsResponse = (GetInvoicesResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mPackagesArrayList.clear();
                        mPackagesArrayList.addAll(stationsResponse.getItems());
                        mAdapter.notifyDataSetChanged();
                    }
                    else
                        Utils.showToast(getActivity(),R.string.no_invoices);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getStations(filterQuery);
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

}
