package com.android.fuel.main.fragments.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.application.FuelConstants;
import com.android.fuel.beans.User;
import com.android.fuel.main.MainActivity;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FragmentUtils;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mohamed Elgendy.
 */

public class ProfileFragment  extends Fragment {

    @BindView(R.id.tv_profile_name)
    TextView mProfileNameTextView;

    @BindView(R.id.tv_profile_email)
    TextView mProfileEmailTextView;

    @BindView(R.id.tv_profile_number)
    TextView mProfileNumberTextView;

    @BindView(R.id.item_edit_profile)
    RelativeLayout mEditProfile;

    @BindView(R.id.item_change_password)
    RelativeLayout mChangePassword;

    @BindView(R.id.item_customer_service)
    RelativeLayout mCustomerService;

    @BindView(R.id.item_settings)
    RelativeLayout mSettings;

    @BindView(R.id.iv_logout)
    ImageView mLogout;

    @BindView(R.id.item_about_sfp)
    RelativeLayout mAboutSFP;

    private View rootView;
    private User mUser;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        setUserInfo();
    }

    private void setUserInfo() {
        mUser = AppConstants.getUser(getActivity());
        CircleImageView userPicImageView = (CircleImageView)rootView.findViewById(R.id.iv_profile_thumbnail);
        if (
                mUser!=null && mUser.getImage()!=null && mUser.getImage().getUrl()!=null &&
                        !Utils.isEmptyString(AppConstants.getUser(getActivity()).getImage().getUrl())
                        && Utils.hasConnection(getActivity()))
        {
            Picasso.with(getContext()).load(mUser.getImage().getUrl()).placeholder(R.drawable.avatar_icon).centerCrop().fit().into(userPicImageView);
        }
        mProfileNameTextView.setText(mUser.getName());
        mProfileEmailTextView.setText(mUser.getEmail());
        mProfileNumberTextView.setText(mUser.getIqama());

        if (mUser.getLoginType()!=AppConstants.LOGIN_TYPE_ACCOUNT)
        {
            mChangePassword.setVisibility(View.GONE);
            mEditProfile.setVisibility(View.GONE);
            mCustomerService.setVisibility(View.GONE);//For phone
        }
    }

    @OnClick(R.id.item_edit_profile)
    void editProfile() {
        FuelManager.openEditProfile(getActivity());
    }

    @OnClick(R.id.item_change_password)
    void changePassword() {
        FuelManager.openChangePassword(getActivity());
    }

    @OnClick(R.id.item_customer_service)
    void customerService() {
        FuelManager.openAddOrEditPhone(getActivity(),mUser.getId());
    }

    @OnClick(R.id.item_settings)
    void settings() {
        FuelManager.openSettings(getActivity());
    }

    @OnClick(R.id.item_about_sfp)
    void aboutSFP() {
        Toast.makeText(getActivity(), "About SFP Clicked", Toast.LENGTH_SHORT).show();
    }

    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mUser = AppConstants.getUser(getActivity());
        if (mUser.getLoginType()==AppConstants.LOGIN_TYPE_GOOGLE) {

            String serverClientId = getString(R.string.google_server_client_id);
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestServerAuthCode(serverClientId)
                    .requestEmail()
                    .build();

            // We pass through three "this" arguments to the builder, specifying the:
            // 1. Context
            // 2. Object to use for resolving connection errors
            // 3. Object to call onConnectionFailed on
            // We also add the Google Sign in API we previously created.
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity() /* Context */)
                    .enableAutoManage(getActivity() /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    } /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        // [END build_client]

    }

    @OnClick(R.id.iv_logout)
    void logout() {
        mUser = AppConstants.getUser(getActivity());
        if (mUser.getLoginType()==AppConstants.LOGIN_TYPE_FACEBOOK)
        {
            LoginManager.getInstance().logOut();
        }
        else  if (mUser.getLoginType()==AppConstants.LOGIN_TYPE_GOOGLE)
        {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            // Hide the sign out buttons, show the sign in button.

                        }
                    });
        }
        AppConstants.clearUser(getActivity());
        getActivity().finish();
        FuelManager.openSplash(getActivity());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
