package com.android.fuel.main.filter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.android.fuel.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class StationFilterFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.radio_fuel_91)
    CheckBox mFuel91RadioButton;

    @BindView(R.id.radio_fuel_95)
    CheckBox mFuel95RadioButton;

    @BindView(R.id.radio_fuel_diesel)
    CheckBox mFuelDieselRadioButton;

    @BindView(R.id.checkBox_restaurant)
    CheckBox mRestaurantCheckBox;

    @BindView(R.id.checkBox_mosque)
    CheckBox mMosqueCheckBox;

    @BindView(R.id.checkBox_coffee)
    CheckBox mCoffeeCheckBox;

    @BindView(R.id.checkBox_women_toilet)
    CheckBox mWomenToiletCheckBox;

    @BindView(R.id.checkBox_men_toilet)
    CheckBox mMenToiletCheckBox;

    @BindView(R.id.checkBox_atm)
    CheckBox mATMCheckBox;

    @BindView(R.id.checkBox_hotels)
    CheckBox mHotelsCheckBox;


    public StationFilterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_station_filter, container, false);
        ButterKnife.bind(this, rootView);

        mFuel91RadioButton.setOnCheckedChangeListener(this);
        mFuel95RadioButton.setOnCheckedChangeListener(this);
        mFuelDieselRadioButton.setOnCheckedChangeListener(this);

        mCoffeeCheckBox.setOnCheckedChangeListener(this);
        mHotelsCheckBox.setOnCheckedChangeListener(this);
        mMenToiletCheckBox.setOnCheckedChangeListener(this);
        mWomenToiletCheckBox.setOnCheckedChangeListener(this);
        mMosqueCheckBox.setOnCheckedChangeListener(this);
        mRestaurantCheckBox.setOnCheckedChangeListener(this);
        mATMCheckBox.setOnCheckedChangeListener(this);

        if (getArguments().containsKey("filterQuery"))
        {
            String query = getArguments().getString("filterQuery");

            if (query.contains("isGas91Available"))
                mFuel91RadioButton.setChecked(true);

            if (query.contains("isGas95Available"))
                mFuel95RadioButton.setChecked(true);

            if (query.contains("isDieselAvailable"))
                mFuelDieselRadioButton.setChecked(true);

            if (query.contains("hasMasjid"))
                mMosqueCheckBox.setChecked(true);

            if (query.contains("hasHotel"))
                mHotelsCheckBox.setChecked(true);

            if (query.contains("hasCafe"))
                mCoffeeCheckBox.setChecked(true);

            if (query.contains("hasRestaurant"))
                mRestaurantCheckBox.setChecked(true);

            if (query.contains("hasMensWC"))
                mMenToiletCheckBox.setChecked(true);

            if (query.contains("hasLadiesWC"))
                mWomenToiletCheckBox.setChecked(true);

            if (query.contains("hasATM"))
                mATMCheckBox.setChecked(true);
        }

        return rootView;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        String queryFilter = "";

        if (mFuel91RadioButton.isChecked())
            queryFilter+="filter[where][isGas91Available]=true&";

        if (mFuel95RadioButton.isChecked())
            queryFilter+="filter[where][isGas95Available]=true&";

        if (mFuelDieselRadioButton.isChecked())
            queryFilter+="filter[where][isDieselAvailable]=true&";

        if (mMosqueCheckBox.isChecked())
            queryFilter+="filter[where][hasMasjid]=true&";

        if (mCoffeeCheckBox.isChecked())
            queryFilter+="filter[where][hasCafe]=true&";

        if (mHotelsCheckBox.isChecked())
            queryFilter+="filter[where][hasHotel]=true&";

        if (mRestaurantCheckBox.isChecked())
            queryFilter+="filter[where][hasRestaurant]=true&";

        if (mMenToiletCheckBox.isChecked())
            queryFilter+="filter[where][hasMensWC]=true&";

        if (mWomenToiletCheckBox.isChecked())
            queryFilter+="filter[where][hasLadiesWC]=true&";

        if (mATMCheckBox.isChecked())
            queryFilter+="filter[where][hasATM]=true&";


        if (queryFilter.length()>0)
            queryFilter = queryFilter.substring(0,queryFilter.length()-1);

        if (getActivity() instanceof FilterActivity)
            ((FilterActivity) getActivity()).setFilterQuery(queryFilter);
    }
}
