package com.android.fuel.main.fragments.wallets.transfercredit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.User;
import com.android.fuel.requests.LoginRequest;
import com.android.fuel.requests.TransferCreditRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetBuyPackageResponse;
import com.android.fuel.responses.GetTransferCreditResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mohamed Elgendy.
 */

public class TransferCreditDetailsFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.bt_confirm)
    Button mConfirmButton;

    @BindView(R.id.tv_transfer_amount)
    TextView mTransferTextView;

    @BindView(R.id.tv_receiver_name)
    TextView mProfileNameTextView;

    @BindView(R.id.textView38)
    TextView mProfileEmailTextView;

    @BindView(R.id.tv_receiver_number)
    TextView mProfileNumberTextView;

    private View mRootView;

    @BindView(R.id.textView40)
    TextView mCurrentBalanceTextView;

    int transferBalance =0;
    private User mUser;

    public TransferCreditDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_transfer_credit_details, container, false);
        ButterKnife.bind(this, mRootView);

         mUser = new Gson().fromJson(getArguments().getString("user"),User.class);
         transferBalance = getArguments().getInt("transferBalance");


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.transfer_credit));
        }


        CircleImageView userPicImageView = (CircleImageView)mRootView.findViewById(R.id.iv_profile_thumbnail);
        if (
                mUser!=null && mUser.getImage()!=null && mUser.getImage().getUrl()!=null &&
                        !Utils.isEmptyString(mUser.getImage().getUrl())
                        && Utils.hasConnection(getActivity()))
        {
            Picasso.with(getContext()).load(mUser.getImage().getUrl()).placeholder(R.drawable.avatar_icon).centerCrop().fit().into(userPicImageView);
        }
        mProfileNameTextView.setText(mUser.getName());
        mProfileEmailTextView.setText(mUser.getEmail());
        mProfileNumberTextView.setText(mUser.getIqama());

        mTransferTextView.setText(transferBalance+"");

        mCurrentBalanceTextView.setText(FuelApp.getCurrentWallet().getBalance()+"");

        return mRootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.bt_confirm)
    void confirm() {


        if (transferBalance>FuelApp.getCurrentWallet().getBalance())
        {
            Utils.showToast(getActivity(),R.string.no_enough_balance);
            return;
        }

        mProgressBar.setVisibility(View.VISIBLE);

        TransferCreditRequest request = new TransferCreditRequest();
        request.setBalance(transferBalance);
        request.setToUserId(mUser.getId());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+FuelApp.getCurrentWallet().getId()+"/transferBalance",
                GetTransferCreditResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetTransferCreditResponse) {

                    float newBalance = FuelApp.getCurrentWallet().getBalance()-transferBalance;
                    mCurrentBalanceTextView.setText(newBalance+"");
                    onConfirmedDone();
                }
                else
                    Utils.showToast(getActivity(),R.string.internet_error);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        confirm();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, -1);

        requestHelper.executeFromRawJson(new Gson().toJson(request));

    }


    private void onConfirmedDone()
    {
        new AwesomeSuccessDialog(getActivity())
                .setMessage(R.string.transfer_dialog_message)
                .setColoredCircle(R.color.colorPrimary)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(getString(R.string.go_to_wallet))
                .setPositiveButtonbackgroundColor(R.color.colorAccent)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        //click
                        getActivity().finish();
                    }
                })
                .show();
    }
}
