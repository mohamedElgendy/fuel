package com.android.fuel.main.fragments.findstation.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.beans.Station;
import com.android.fuel.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class StationListAdapter extends RecyclerView.Adapter<StationListAdapter.DataObjectHolder> {

    private ArrayList<Station> mDataSet;
    private Context context;
    private static StationClickListener mStationClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_station_logo)
        ImageView mStationImageView;

        @BindView(R.id.tv_station_address)
        TextView mStationAddressTextView;

        @BindView(R.id.tv_station_name)
        TextView mStationNameTextView;

        @BindView(R.id.tv_station_91)
        TextView mStation91TextView;

        @BindView(R.id.tv_station_95)
        TextView mStation95TextView;

        @BindView(R.id.tv_station_diesel)
        TextView mStationDieselTextView;

        @BindView(R.id.rb_station)
        RatingBar mStationRatingBar;

        @BindView(R.id.iv_station_mosque)
        ImageView mMosqueImageView;

        @BindView(R.id.iv_station_coffee)
        ImageView mCoffeeImageView;

        @BindView(R.id.iv_station_hotel)
        ImageView mHotelImageView;

        @BindView(R.id.iv_station_men_toilet)
        ImageView mMenToiletImageView;

        @BindView(R.id.iv_station_women_toilet)
        ImageView mWomenToiletImageView;

        @BindView(R.id.iv_station_restaurant)
        ImageView mRestaurantImageView;

        @BindView(R.id.iv_station_atm)
        ImageView mATMImageView;

        @BindView(R.id.iv_station_navigation)
        ImageView mStationNavigationImageView;

        //fill out rest of views

        public DataObjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            //adding listener...
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mStationClickListener.onRowClick();
        }

        @OnClick(R.id.iv_station_navigation)
        public void navigate(){
            mStationClickListener.onStationClick(getAdapterPosition());
        }
    }

    public StationListAdapter(ArrayList<Station> mDataSet, StationClickListener mStationClickListener) {
        this.mDataSet = mDataSet;
        this.mStationClickListener = mStationClickListener;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_station, parent, false);
        context = parent.getContext();

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {

        Station mStation = mDataSet.get(position);

        if (mStation.getCompany()!=null)
        {
            if (mStation.getCompany()!=null && mStation.getCompany().get_image()!=null && mStation.getCompany().get_image().getUrl()!=null &&
                    !Utils.isEmptyString(mStation.getCompany().get_image().getUrl()))
            {
                Picasso.with(context).load(mStation.getCompany().get_image().getUrl())
                        .placeholder(R.drawable.avatar_icon).fit().into(holder.mStationImageView);
            }else{
                Picasso.with(context).load(R.drawable.logo).fit().into(holder.mStationImageView);
            }

        }

        if (!mStation.isHasCafe())
            holder.mCoffeeImageView.setVisibility(View.GONE);

        if (!mStation.isHasHotel())
            holder.mHotelImageView.setVisibility(View.GONE);

        if (!mStation.isHasLadiesWC())
            holder.mWomenToiletImageView.setVisibility(View.GONE);

        if (!mStation.isHasMensWC())
            holder.mMenToiletImageView.setVisibility(View.GONE);

        if (!mStation.isHasRestaurant())
            holder.mRestaurantImageView.setVisibility(View.GONE);

        if (!mStation.isHasMasjid())
            holder.mMosqueImageView.setVisibility(View.GONE);

        if (!mStation.isHasATM())
            holder.mATMImageView.setVisibility(View.GONE);

        holder.mStationNameTextView.setText(mStation.getTitle());
        holder.mStationAddressTextView.setText(mStation.getAddress());

        holder.mStation91TextView.setText(mStation.getGas91Price() + "");
        holder.mStation95TextView.setText(mStation.getGas95Price() + "");
        holder.mStationDieselTextView.setText(mStation.getDieselPrice() + "");

        holder.mStationRatingBar.setRating(mStation.getRating());

    }

    public void addItem(Station dataObj) {
        mDataSet.add(dataObj);
        notifyDataSetChanged();
    }

    public void deleteItem(int index) {
        mDataSet.remove(index);
        notifyItemRemoved(index);
    }

    public void updateItem(int index, Station dataObj) {
        mDataSet.set(index, dataObj);
        notifyItemChanged(index, dataObj);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface StationClickListener {
        void onRowClick();
        void onStationClick(int position);
    }
}

