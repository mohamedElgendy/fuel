package com.android.fuel.main.fragments.fillfuel;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.beans.Wallet;
import com.android.fuel.responses.AttendationConfirmationResponse;
import com.android.fuel.responses.FillFuelResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Contents;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.QRCodeEncoder;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by Mohamed Elgendy.
 */

public class FillOrderResultFragment extends Fragment {

    @BindView(R.id.tv_order_number)
    TextView mOrderNumberTextView;

    @BindView(R.id.iv_order_qr_code)
    ImageView qrOrderTextView;

    @BindView(R.id.tv_fuel_type_result)
    TextView mFuelTypeResultTextView;

    @BindView(R.id.tv_fuel_liters_result)
    TextView mFuelLitersResultTextView;

    @BindView(R.id.tv_fuel_cost_result)
    TextView mFuelCostResult;

    @BindView(R.id.bt_cancel)
    Button mCancelButton;

    private FillFuelResponse mFillFuelResponse;

    public FillOrderResultFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fill_order_result, container, false);
        ButterKnife.bind(this, rootView);


        mFillFuelResponse = new Gson().fromJson(getArguments().getString("item"),FillFuelResponse.class);

        initView();
        return rootView;
    }

    private void initView() {

        mFuelTypeResultTextView.setText(mFillFuelResponse.getGasType());
        mFuelLitersResultTextView.setText(mFillFuelResponse.getLiters()+"");
        mOrderNumberTextView.setText(mFillFuelResponse.getId());
        mFuelCostResult.setText(mFillFuelResponse.getCost()+"");


        //Find screen size
        WindowManager manager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;

        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(mFillFuelResponse.getId(),
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                smallerDimension);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();

            qrOrderTextView.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.bt_cancel)
    void cancelOrder() {

        getActivity().finish();
    }


    Handler h = new Handler();
    int delay = 15000; //15 seconds
    Runnable runnable;

    @Override
    public void onResume() {
        super.onResume();

        h.postDelayed(new Runnable() {
            public void run() {
                //do something

                if (!isApiCalled && getActivity()!=null)
                    checkStationConfirmation();

                runnable=this;
                h.postDelayed(runnable, delay);
            }
        }, delay);

    }

    @Override
    public void onPause() {
        super.onPause();
        h.removeCallbacks(runnable); //stop handler when activity not visible
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        h.removeCallbacks(runnable); //stop handler when activity not visible
    }

    boolean isApiCalled= false;
    private void checkStationConfirmation()
    {
        isApiCalled =true;
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "GasFills/"+mFillFuelResponse.getId()+"/attendantConfirmation",
                AttendationConfirmationResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                isApiCalled =false;
                if (getActivity()!=null && response instanceof AttendationConfirmationResponse)
                {
                    if (!((AttendationConfirmationResponse) response).getId().equals("404"))
                    {
                        //Utils.showToast(getActivity(),"done");
                        h.removeCallbacks(runnable); //stop handler when activity not visible
                        FuelManager.openFillFuelDone(getActivity(),((AttendationConfirmationResponse) response).getGasFillId());
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                isApiCalled =false;
            }
        }, Parameters.SERVICE_CHECK_ATTANDANT);

        if (getActivity()!=null)
            requestHelper.executeGet();
    }
}
