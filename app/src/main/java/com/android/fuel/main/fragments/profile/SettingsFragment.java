package com.android.fuel.main.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.beans.Language;
import com.android.fuel.beans.UserPreferences;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.SavePrefs;
import com.android.fuel.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class SettingsFragment extends Fragment {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.radio_english)
    RadioButton mEnglishRadioButton;

    @BindView(R.id.radio_arabic)
    RadioButton mArabicRadioButton;

    @BindView(R.id.item_privacy_policy)
    RelativeLayout mPrivacyPolicy;

    @BindView(R.id.item_terms_and_conditions)
    RelativeLayout mTermsAndConditions;

    @BindView(R.id.item_share_sfp)
    RelativeLayout mShareSFP;

    @BindView(R.id.item_rate_sfp)
    RelativeLayout mRateSFP;


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.settings));
        }


        SavePrefs<UserPreferences> userPreferencesSavePrefs = new SavePrefs<>(getActivity(),UserPreferences.class);
        UserPreferences userPreferences = userPreferencesSavePrefs.load();
        if (userPreferences!=null)
        {
            if (userPreferences.getLanguage().getLocale().equals("ar"))
                mArabicRadioButton.setChecked(true);
            else
                mEnglishRadioButton.setChecked(true);
        }

        return rootView;
    }

    @OnClick(R.id.radio_english)
    void englishLanguage() {
        setLanguage("en");
    }

    @OnClick(R.id.radio_arabic)
    void arabicLanguage() {
        setLanguage("ar");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setLanguage(String locale)
    {
        UserPreferences userPreferences = new UserPreferences();
        Language language = new Language();
        language.setLocale(locale);
        userPreferences.setLanguage(language);
        SavePrefs<UserPreferences> userPreferencesSavePrefs = new SavePrefs<>(getActivity(),UserPreferences.class);
        userPreferencesSavePrefs.save(userPreferences);
        FuelManager.openSplash(getActivity());
    }

    @OnClick(R.id.item_terms_and_conditions)
    void termsAndConditions() {
        Toast.makeText(getActivity(), "Terms and Conditions Clicked", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.item_privacy_policy)
    void privacyPolicy() {
        Toast.makeText(getActivity(), "Privacy Policy Clicked", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.item_share_sfp)
    void shareSFP() {
        Toast.makeText(getActivity(), "Share SFP Clicked", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.item_rate_sfp)
    void rateSFP() {
        Toast.makeText(getActivity(), "Rate SFP Clicked", Toast.LENGTH_SHORT).show();
    }

}

