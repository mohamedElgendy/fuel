package com.android.fuel.main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.os.Bundle;
import android.view.MenuItem;

import com.android.fuel.R;
import com.android.fuel.application.BaseActivity;
import com.android.fuel.application.FuelConstants;
import com.android.fuel.main.fragments.fillfuel.FillFuelFragment;
import com.android.fuel.main.fragments.findstation.FindStationFragment;
import com.android.fuel.main.fragments.invoices.InvoicesFragment;
import com.android.fuel.main.fragments.profile.ProfileFragment;
import com.android.fuel.main.fragments.wallets.WalletsFragment;
import com.android.fuel.main.fragments.wallets.buysubwallets.BuySubWalletsFragment;
import com.android.fuel.main.fragments.wallets.subwallet.AddSubWalletFragment;
import com.android.fuel.main.fragments.wallets.subwallet.SubWalletFragment;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FragmentUtils;
import com.android.fuel.utils.FuelManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.navigation)
    BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initializeBottomNavigationView();

    }

    private void initializeBottomNavigationView() {

        //Manually displaying the first fragment - one time only
        FragmentUtils.replaceFragment(this, new FindStationFragment(), R.id.main_container, false, FuelConstants.FIND_STATION_FRAG_TAG);

        setCurrentFragment(R.id.action_find_station);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    public void setCurrentFragment(int itemId)
    {
        mBottomNavigationView.setSelectedItemId(itemId);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_fill_fuel:

                FragmentUtils.replaceFragment(this, new FillFuelFragment(), R.id.main_container,
                        false, FuelConstants.FILL_FUEL_FRAG_TAG);

                break;
            case R.id.action_find_station:

                FragmentUtils.replaceFragment(this, new FindStationFragment(), R.id.main_container,
                        false, FuelConstants.FIND_STATION_FRAG_TAG);

                break;
            case R.id.action_invoices:

                FragmentUtils.replaceFragment(this, new InvoicesFragment(), R.id.main_container,
                        false, FuelConstants.INVOICES_FRAG_TAG);

                break;
            case R.id.action_profile:

                FragmentUtils.replaceFragment(this, new ProfileFragment(), R.id.main_container,
                        false, FuelConstants.PROFILE_FRAG_TAG);

                break;
            case R.id.action_wallets:

                FragmentUtils.replaceFragment(this, new WalletsFragment(), R.id.main_container,
                        false, FuelConstants.WALLETS_FRAG_TAG);

                break;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
