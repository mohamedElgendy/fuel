package com.android.fuel.main.fragments.wallets.buysubwallets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.SubWalletPackage;
import com.android.fuel.requests.PurchaseSubWalletPackage;
import com.android.fuel.requests.TransferCreditRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetInvoicesResponse;
import com.android.fuel.responses.GetSubWalletsPackagesResponse;
import com.android.fuel.responses.GetTransferCreditResponse;
import com.android.fuel.responses.SubWalletPurchasePackageResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;
import static com.android.fuel.services.Parameters.SERVICE_RECHARGE;

/**
 * Created by Mohamed Elgendy.
 */

public class BuySubWalletsFragment  extends Fragment implements SubWalletListAdapter.SubWalletPurchaseClickListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.sub_wallets_recycler_view)
    RecyclerView mSubWalletsRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    // declare station list components
    private RecyclerView.Adapter subWalletsAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<SubWalletPackage> mSubWalletsArrayList = new ArrayList<>();

    View rootView;

    public BuySubWalletsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.fragment_buy_sub_wallets, container, false);
        ButterKnife.bind(this, rootView);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.buy_sub_wallets));
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        // initialize views
        mSubWalletsRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(),3);
        mSubWalletsRecyclerView.setLayoutManager(mLayoutManager);
        subWalletsAdapter = new SubWalletListAdapter(mSubWalletsArrayList,this);
        mSubWalletsRecyclerView.setAdapter(subWalletsAdapter);
        getSubWalletsPackages();
    }

    private void getSubWalletsPackages() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"WalletPackages/active",
                GetSubWalletsPackagesResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetSubWalletsPackagesResponse)
                {
                    GetSubWalletsPackagesResponse stationsResponse = (GetSubWalletsPackagesResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mSubWalletsArrayList.clear();
                        mSubWalletsArrayList.addAll(stationsResponse.getItems());
                        subWalletsAdapter.notifyDataSetChanged();
                    }
                    else
                        Utils.showToast(getActivity(),R.string.no_sub_wallet_packages);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                rootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getSubWalletsPackages();
                    }
                });
                rootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSubWalletPurchaseClick(final int position, View v) {

        purchaseNow(position);


    }

    private void purchaseNow(final int position) {
        mProgressBar.setVisibility(View.VISIBLE);

        PurchaseSubWalletPackage request = new PurchaseSubWalletPackage();
        request.setWalletPackageId(mSubWalletsArrayList.get(position).getId());

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+ FuelApp.getCurrentWallet().getId()+"/walletPackagePurchases",
                SubWalletPurchasePackageResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof SubWalletPurchasePackageResponse) {

                    showDoneDialog(position);
                }
                else
                {
                    if (response instanceof GeneralResponse && ((GeneralResponse) response).getId().equals("401"))
                    {
                        Utils.showToast(getActivity(),R.string.no_enough_balance);
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                rootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        purchaseNow(position);
                    }
                });
                rootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_RECHARGE);

        requestHelper.executeFromRawJson(new Gson().toJson(request));
    }

    private void showDoneDialog(final int position) {
        new AwesomeSuccessDialog(getActivity())
                .setMessage(R.string.buy_sub_wallet_dialog_message_valid_year)
                .setColoredCircle(R.color.colorPrimary)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(getString(R.string.go_to_wallet))
                .setPositiveButtonbackgroundColor(R.color.colorAccent)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        //click
                        Log.d("TAG-onWalletPurchase",""+position);
                        getActivity().finish();
                    }
                })
                .show();
    }

}
