package com.android.fuel.main.fragments.wallets;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.Wallet;
import com.android.fuel.responses.GetSubWalletsCount;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mohamed Elgendy.
 */

public class WalletsFragment extends Fragment {

    @BindView(R.id.layout_recharge_wallet)
    RelativeLayout mRechargeWalletItem;

    @BindView(R.id.layout_transfer_credit)
    RelativeLayout mTransferCreditItem;

    @BindView(R.id.layout_wallet_transaction)
    RelativeLayout mWalletTransactionItem;

    @BindView(R.id.layout_reward_points)
    RelativeLayout mRewardPointsItem;

    @BindView(R.id.layout_sub_wallets)
    RelativeLayout mSubWalletsItem;

    @BindView(R.id.layout_buy_sub_wallets)
    RelativeLayout mBuySubWalletsItem;

    @BindView(R.id.textView25)
    TextView mTotalBalanceTextView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.tv_num_wallets)
    TextView mWalletsTextView;

    private Wallet userWallet;

    public WalletsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wallets, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        getUserWallet();
    }

    private boolean isHaveWallet()
    {
        if (userWallet==null)
            return false;

        return true;
    }

    @OnClick(R.id.layout_recharge_wallet)
    void rechargeWalletItem() {

        if (isHaveWallet())
        {
            FuelManager.openRechargePackagesList(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.layout_transfer_credit)
    void transferCredit() {

        if (isHaveWallet() && userWallet.getBalance()==0)
        {
            Utils.showToast(getActivity(),R.string.no_enough_balance);
            return;
        }

        if (isHaveWallet())
        {
            FuelManager.openWalletTransferCredit(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.layout_wallet_transaction)
    void walletTransactions() {
        if (isHaveWallet())
        {
            FuelManager.openWalletTransactions(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.layout_reward_points)
    void rewardPoints() {
        if (isHaveWallet())
        {
            if (userWallet.getPoints()>0)
                FuelManager.openConvertPoints(getActivity());
            else
                Utils.showToast(getActivity(),R.string.no_enough_points);
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.layout_buy_sub_wallets)
    void buySubWallet() {

        if (isHaveWallet() && userWallet.getBalance()==0)
        {
            Utils.showToast(getActivity(),R.string.no_enough_balance);
            return;
        }

        if (isHaveWallet())
        {
            FuelManager.openBuySubWallet(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.layout_sub_wallets)
    void subWallets() {
        if (isHaveWallet())
        {
            FuelManager.openSubWallet(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    private void getUserWallet() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+ AppConstants.getUserId(getActivity())+"/wallet",
                Wallet.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof Wallet)
                {
                     userWallet = (Wallet) response;

                    if (userWallet!=null)
                    {
                        FuelApp.setCurrentWallet(userWallet);
                        if (String.valueOf(userWallet.getBalance()).length()>3)
                            mTotalBalanceTextView.setText(String.format("%.2f",userWallet.getBalance()));
                        else
                            mTotalBalanceTextView.setText(userWallet.getBalance()+"");

                        getUserSubWalletsCountWallet(userWallet.getId());
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper.executeGet();
    }

    private void getUserSubWalletsCountWallet(String walletId) {

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+walletId+"/subWalletLimit",
                GetSubWalletsCount.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetSubWalletsCount)
                {
                    GetSubWalletsCount userWallet = (GetSubWalletsCount) response;

                    if (userWallet!=null)
                    {
                        mWalletsTextView.setText(userWallet.getCurrent()+"/"+userWallet.getMax());//Here add real value
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper.executeGet();
    }

}
