package com.android.fuel.main.fragments.findstation;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.Station;
import com.android.fuel.customviews.SphericalUtil;
import com.android.fuel.main.filter.FilterActivity;
import com.android.fuel.main.fragments.findstation.adapter.StationListAdapter;
import com.android.fuel.responses.GetBannersResponse;
import com.android.fuel.responses.GetStationsResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.koushikdutta.ion.Ion;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class FindStationDetailsFragment extends Fragment implements StationListAdapter.StationClickListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_zoom)
    ImageView mMapZoomImageView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.slidingLayout)
    SlidingUpPanelLayout mSlidingUpPanelLayout;

    @BindView(R.id.stations_recycler_view)
    RecyclerView mStationRecyclerView;

    // declare station list components
    private StationListAdapter stationsAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Station> mStationsArrayList = new ArrayList<>();;

    public FindStationDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_station_details, container, false);
        ButterKnife.bind(this, rootView);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.find_station));
        }

        initMap();

        if (!isLocationServiceEnabled(getActivity())) {
            showGPSDisabledAlertToUser();
        }

        mSlidingUpPanelLayout.setAnchorPoint(0.3f);
        mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
        mSlidingUpPanelLayout.setScrollableView(mStationRecyclerView);
        mSlidingUpPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        return rootView;
    }

    public boolean isLocationServiceEnabled(Context context){
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if(locationManager ==null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        return gps_enabled || network_enabled;

    }

    private AlertDialog mLocationAlertDialog;
    public void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.enable_location)
                .setCancelable(false)
                .setPositiveButton(R.string.location_enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.location_disable, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        mLocationAlertDialog = alertDialogBuilder.create();
        mLocationAlertDialog.show();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        // initialize views
        mStationRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mStationRecyclerView.setLayoutManager(mLayoutManager);
        stationsAdapter = new StationListAdapter(mStationsArrayList,this);
        mStationRecyclerView.setAdapter(stationsAdapter);

        getStations("");
    }

    private void getStations(String query) {

        mProgressBar.setVisibility(View.VISIBLE);
        //String filters = "&filter[where][position][near]=30.560120,31.007588&filter[where][hasMarket]=true&";
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"GasStations/active?filter[limit]=100&filter[skip]=0"+query,
                GetStationsResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetStationsResponse)
                {
                    GetStationsResponse stationsResponse = (GetStationsResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mStationsArrayList.clear();
                        mStationsArrayList.addAll(stationsResponse.getItems());
                        manageMarkers(mStationsArrayList);
                        stationsAdapter.notifyDataSetChanged();
                    }
                    else
                        Utils.showToast(getActivity(),R.string.no_station_result);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.station_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            //navigate to filter Activity
            Intent filter = new Intent(getActivity(), FilterActivity.class);
            if (!sortQuery.equals(""))
                filter.putExtra("sortQuery",sortQuery);
            if (!filterQuery.equals(""))
                filter.putExtra("filterQuery",filterQuery);
            startActivityForResult(filter,5555);
            return true;
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }


        return super.onOptionsItemSelected(item);
    }


    String sortQuery ="";
    String filterQuery ="";

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5555) {
            if(resultCode == Activity.RESULT_OK){

                sortQuery=data.getStringExtra("sortQuery");
                filterQuery=data.getStringExtra("filterQuery");
                mStationsArrayList.clear();
                stationsAdapter.notifyDataSetChanged();

                String allQuery="";

                if (filterQuery!=null && !filterQuery.equals(""))
                    allQuery+="&"+filterQuery;

                if (sortQuery!=null && !sortQuery.equals(""))
                {
                    if (sortQuery.equals("NEAREST") && mLastLocation!=null)
                    {
                        allQuery += "&filter[where][location][near]="+mLastLocation.getLatitude()+","+mLastLocation.getLongitude();
                    }
                    else if (!sortQuery.equals("NEAREST"))
                        allQuery+="&"+sortQuery;
                }

                getStations(allQuery);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onRowClick() {
        if(mSlidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED){
            mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }else{
            mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        }
    }

    @Override
    public void onStationClick(int position) {
        startStationInfo(position);
    }

    private void openStationInfoByStationId(String stationId) {

        for (int i=0;i<mStationsArrayList.size();i++)
        {
            if (mStationsArrayList.get(i).getId().equals(stationId))
            {
                FuelApp.setCurrentStation(mStationsArrayList.get(i));
                FuelManager.openStationInfo(getActivity());
                return;
            }
        }
    }

    private void startStationInfo(int position) {
        FuelApp.setCurrentStation(mStationsArrayList.get(position));
        FuelManager.openStationInfo(getActivity());
    }


    ///////////Map///////////////////////////////////////////////


    private static final int REQUEST_LOCATION = 0;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private GoogleMap mGoogleMap;
    private MarkerOptions myLocationMarkerOptions;
    private ArrayList<MarkerOptions> mMarkersOptionsList = new ArrayList<>();

    private void initMap()
    {
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            //Toast.makeText(getActivity(), "Google Service Is Available!!", Toast.LENGTH_SHORT).show();
        }

        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mMapZoomImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mStationRecyclerView.getVisibility()==View.VISIBLE)
                    mStationRecyclerView.setVisibility(View.GONE);
                else
                    mStationRecyclerView.setVisibility(View.VISIBLE);
            }
        });

    }

    private void manageMarkers(ArrayList<Station> stationsList)
    {
        //To clear markers
        mMarkersOptionsList.clear();
        mGoogleMap.clear();
        myLocationMarkerOptions = null;

        if (mLastLocation!=null)
        {
            addMyLocationMarker(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()));
            addPlacesMarkers(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()),stationsList);
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(final GoogleMap map) {
        mGoogleMap = map;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {


                openStationInfoByStationId(marker.getTitle());

                //Used to hide infoWindow
                marker.hideInfoWindow();
                return true;

                //To show InfoWindow
                //return false;
            }
        });

    }

    private void moveToCurrentLocation(LatLng currentLocation)
    {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        // Zoom in, animating the camera.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    private Bitmap getMarkerBitmapFromView(LatLng myLocation, LatLng targetLocation, String name, Station mStation) {



        double distance = SphericalUtil.computeDistanceBetween(targetLocation, myLocation);

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        TextView disTextView = (TextView) customMarkerView.findViewById(R.id.tv_distance);
        disTextView.setText(new DecimalFormat("#.##").format((distance/1000))+"KM");

        if (mStation.getCompany()!=null)
        {
            if (mStation.getCompany()!=null && mStation.getCompany().get_image()!=null && mStation.getCompany().get_image().getUrl()!=null &&
                    !Utils.isEmptyString(mStation.getCompany().get_image().getUrl()))
            {

                Picasso.with(getActivity()).load(mStation.getCompany().get_image().getUrl()).into(markerImageView);

                /* replace those lines with picasso due to heavy load work on the main thread
                try {
                    Bitmap bmImg = Ion.with(getActivity())
                            .load(mStation.getCompany().get_image().getUrl().replace("https","http")).asBitmap().get();
                    markerImageView.setImageBitmap(bmImg);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                */
            }
        }

        TextView nameTextView = (TextView) customMarkerView.findViewById(R.id.tv_name);
        nameTextView.setText(name);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    /**
     * LOCATION LISTENER EVENTS
     *
     * */

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getServicesAvailable();

        // Resuming the periodic location updates
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }

    //Starting the location updates
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest,  this);
        }
    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(getActivity());
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(getActivity(), isAvailable, 0);
            dialog.show();
        } else {
           // Toast.makeText(getActivity(), "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }
    //Creating location request object
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(AppConstants.DISPLACEMENT);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        displayLocation();

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

        //Toast.makeText(getActivity(), "Location changed!", Toast.LENGTH_SHORT).show();

        // Displaying the new location on UI
        displayLocation();

    }

    //Method to display the location on UI
    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {


            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();

                LatLng currentLocation = new LatLng(latitude,longitude);

                addMyLocationMarker(currentLocation);

            } else {

                //Toast.makeText(this, "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addMyLocationMarker(LatLng currentLocation) {
        if (myLocationMarkerOptions==null) {
            myLocationMarkerOptions = new MarkerOptions().position(currentLocation).title("")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));
            mGoogleMap.addMarker(myLocationMarkerOptions);
        }
        else {
            myLocationMarkerOptions.position(currentLocation);
        }

        moveToCurrentLocation(currentLocation);
    }

    private void addPlacesMarkers(LatLng currentLocation,ArrayList<Station> stationsList) {

        if (mMarkersOptionsList.size()==0)
        {
            for (int i=0;i<stationsList.size();i++)
            {
                String title = stationsList.get(i).getTitle();
                if (stationsList.get(i).getCompany()!=null)
                    title = stationsList.get(i).getCompany().getName();

                LatLng from = new LatLng(stationsList.get(i).getPosition().getLat(),stationsList.get(i).getPosition().getLng());
                MarkerOptions marker1 = new MarkerOptions().position(from).title(stationsList.get(i).getId())//Here put id of station to compare it in case marker click "may name of station repeated"
                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(currentLocation,from,title,stationsList.get(i))));
                mGoogleMap.addMarker(marker1);
                mMarkersOptionsList.add(marker1);
            }
        }
    }

    //////////End Map////////////////////////////////////////////
}