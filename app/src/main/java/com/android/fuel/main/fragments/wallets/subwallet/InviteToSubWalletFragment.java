package com.android.fuel.main.fragments.wallets.subwallet;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.SubWallet;
import com.android.fuel.beans.User;
import com.android.fuel.requests.InviteToSubWalletRequest;
import com.android.fuel.requests.TransferCreditRequest;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetSubWalletsPackage;
import com.android.fuel.responses.GetSubWalletsResponse;
import com.android.fuel.responses.GetTransferCreditResponse;
import com.android.fuel.responses.InviteToSubWalletResponse;
import com.android.fuel.responses.SubWalletPurchasePackageResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class InviteToSubWalletFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_identity)
    EditText mIdentityEditText;

    @BindView(R.id.checkBox_gas_fill)
    CheckBox mFillFuelCheckBox;

    @BindView(R.id.checkBox_charge)
    CheckBox mChargeCheckBox;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;

    @BindView(R.id.sp_packages)
    Spinner mSubWalletPackagesSpinner;

    private ArrayList<SubWallet> mSubWallets = new ArrayList<>();

    public InviteToSubWalletFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_invite_subwallet, container, false);
        ButterKnife.bind(this, mRootView);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.invite));
        }

        return mRootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        getDriverSubWallets();

    }

    private void getDriverSubWallets() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Drivers/"+ AppConstants.getUserId(getActivity())+"/Wallet/subwallets",
                GetSubWalletsResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetSubWalletsResponse)
                {
                    GetSubWalletsResponse stationsResponse = (GetSubWalletsResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mSubWallets.clear();
                        mSubWallets.addAll(stationsResponse.getItems());
                        setSubWalletPackagesSpinner();
                    }
                    else {
                        Utils.showToast(getActivity(), R.string.no_sub_wallet);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getDriverSubWallets();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

    private void setSubWalletPackagesSpinner() {

        ArrayAdapter<SubWallet> spinnerArrayAdapter = new ArrayAdapter<>
                (getActivity(), android.R.layout.simple_spinner_item,
                        mSubWallets); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        mSubWalletPackagesSpinner.setAdapter(spinnerArrayAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.bt_invite)
    void invite() {

        attempt();
    }

    private void attempt() {

        // Reset errors.
        mIdentityEditText.setError(null);

        // Store values at the time of the login attempt.
        String identity = mIdentityEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(identity)) {
            mIdentityEditText.setError(getString(R.string.error_field_required));
            focusView = mIdentityEditText;
            cancel = true;
        }

        if (!mChargeCheckBox.isChecked() && !mFillFuelCheckBox.isChecked()) {
            Utils.showToast(getActivity(),R.string.choose_permission);
            focusView = mIdentityEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            getUser();
        }
    }

    private void getUser() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/search?q="+mIdentityEditText.getText().toString().replaceAll(" ", "%20"),
                User.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof User)
                {
                    User user = (User) response;
                    if (user.getId().equals("400"))
                        Utils.showToast(getActivity(),R.string.no_found_user);
                    else
                    {
                        inviteNow(user);
                    }
                }
                else
                    Utils.showToast(getActivity(),R.string.no_found_user);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getUser();
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);

            }
        }, Parameters.SERVICE_GET_DRIVER);
        requestHelper.executeGet();
    }

    private void inviteNow(final User user) {

        mProgressBar.setVisibility(View.VISIBLE);

        InviteToSubWalletRequest request = new InviteToSubWalletRequest();
        request.setDriverId(user.getId());
        request.setHasChargePermission(mChargeCheckBox.isChecked());
        request.setHasGasfillPermission(mFillFuelCheckBox.isChecked());

        String subWalletId= ((SubWallet)mSubWalletPackagesSpinner.getSelectedItem()).getId();
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+subWalletId+"/Invitations",
                InviteToSubWalletResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof InviteToSubWalletResponse) {

                    onConfirmedDone();
                }
                else
                    Utils.showToast(getActivity(),R.string.internet_error);
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        inviteNow(user);
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, -1);

        requestHelper.executeFromRawJson(new Gson().toJson(request));
    }


    private void onConfirmedDone()
    {
        new AwesomeSuccessDialog(getActivity())
                .setMessage(R.string.invitation_sent)
                .setColoredCircle(R.color.colorPrimary)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(getString(R.string.go_to_wallet))
                .setPositiveButtonbackgroundColor(R.color.colorAccent)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        //click
                        getActivity().finish();
                    }
                })
                .show();
    }

}