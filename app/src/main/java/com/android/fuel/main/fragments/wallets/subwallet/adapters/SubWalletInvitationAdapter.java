package com.android.fuel.main.fragments.wallets.subwallet.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.DriverInvoice;
import com.android.fuel.beans.SubWalletInvitation;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class SubWalletInvitationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<SubWalletInvitation> mDataSet;
    private Context context;
    private Activity activity;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();


    public SubWalletInvitationAdapter(ArrayList<SubWalletInvitation> mDataSet, Activity activity) {
        this.mDataSet = mDataSet;
        this.activity = activity;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        context = parent.getContext();

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invitation, parent, false);
        DataObjectChargeHolder dataObjectHolder = new DataObjectChargeHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final SubWalletInvitation mItem = mDataSet.get(position);

        DataObjectChargeHolder chargeHolder = (DataObjectChargeHolder)holder;


        final String data = String.valueOf(position);
        binderHelper.bind(chargeHolder.swipeLayout, data);


        chargeHolder.mOwnerNameTextView.setText(mItem.getWallet().getOwner().getName());
        chargeHolder.mOwnerEmailTextView.setText(mItem.getWallet().getOwner().getEmail());

        if (mItem.getWallet().getOwner().getImage()!=null)
        {
            Picasso.with(context).load(mItem.getWallet().getOwner().getImage().getUrl()).fit().into(chargeHolder.mOwnerImageView);
        }

        if (mItem.isHasChargePermission())
            chargeHolder.mChargeButton.setVisibility(View.VISIBLE);
        else
            chargeHolder.mChargeButton.setVisibility(View.GONE);

        if (mItem.isHasGasfillPermission())
            chargeHolder.mFillFuelButton.setVisibility(View.VISIBLE);
        else
            chargeHolder.mFillFuelButton.setVisibility(View.GONE);

        chargeHolder.mChargeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FuelApp.setCurrentWallet(mItem.getWallet());//Temporary we added this Wallet as current until charge it.
                FuelManager.openRechargePackagesList(activity);
            }
        });

        chargeHolder.mFillFuelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FuelApp.setCurrentWallet(mItem.getWallet());//Temporary we added this Wallet as current until charge it.
                FuelManager.openStationsListing(activity);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


    /**
     * Only if you need to restore open/close state when the orientation is changed.
     * Call this method in {@link android.app.Activity#onSaveInstanceState(Bundle)}
     */
    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed.
     * Call this method in {@link android.app.Activity#onRestoreInstanceState(Bundle)}
     */
    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }


    public static class DataObjectChargeHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.swipe_layout)
        SwipeRevealLayout swipeLayout;

        @BindView(R.id.tv_owner_email)
        TextView mOwnerEmailTextView;

        @BindView(R.id.tv_owner_name)
        TextView mOwnerNameTextView;

        @BindView(R.id.iv_owner)
        ImageView mOwnerImageView;

        @BindView(R.id.bt_fill_fuel)
        ImageView mFillFuelButton;

        @BindView(R.id.bt_charge)
        ImageView mChargeButton;

        public DataObjectChargeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
