package com.android.fuel.main.filter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.android.fuel.R;
import com.android.fuel.application.BaseActivity;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterWalletTransactionActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    private String filterQuery="";

    @BindView(R.id.checkBox_charge)
    CheckBox mChargeCheckBox;

    @BindView(R.id.checkBox_refund)
    CheckBox mRefundCheckBox;

    @BindView(R.id.checkBox_transfer_from)
    CheckBox mTransferFromCheckBox;

    @BindView(R.id.checkBox_transfer_credit)
    CheckBox mTransferToCheckBox;

    @BindView(R.id.checkBox_convert_points)
    CheckBox mConvertPointsCheckBox;

    @BindView(R.id.checkBox_fill_fuel)
    CheckBox mFilFuelCheckBox;

    private String dateRange="";

    @BindView(R.id.bt_select_date)
    Button mSelectDateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_transactions_filter);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("filterQuery"))
            filterQuery = getIntent().getStringExtra("filterQuery");

        mTransferFromCheckBox.setOnCheckedChangeListener(this);
        mFilFuelCheckBox.setOnCheckedChangeListener(this);
        mConvertPointsCheckBox.setOnCheckedChangeListener(this);
        mTransferToCheckBox.setOnCheckedChangeListener(this);
        mRefundCheckBox.setOnCheckedChangeListener(this);
        mChargeCheckBox.setOnCheckedChangeListener(this);

        if (!filterQuery.equals(""))
        {
            String query = filterQuery;
            if (query.contains("refund"))
                mRefundCheckBox.setChecked(true);

            if (query.contains("gas_fill"))
                mFilFuelCheckBox.setChecked(true);

            if (query.contains("transfer_from"))
                mTransferFromCheckBox.setChecked(true);

            if (query.contains("package_purchase"))
                mChargeCheckBox.setChecked(true);

            if (query.contains("points_redemption"))
                mConvertPointsCheckBox.setChecked(true);

            if (query.contains("transfer_to"))
                mTransferToCheckBox.setChecked(true);
        }


        findViewById(R.id.bt_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        findViewById(R.id.bt_select_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPicker();
            }
        });
    }

    private void showPicker()
    {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        //dpd.setAutoHighlight(mAutoHighlight);
        dpd.show(getFragmentManager(),"");
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        generateQuery();

    }

    private void generateQuery() {
        int count=0;
        String queryFilter = "";

        if (mChargeCheckBox.isChecked()) {
            queryFilter += "filter[where][or][0][type]=charge&filter[where][or][1][type]=package_purchase&";
            count=2;
        }

        if (mRefundCheckBox.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][type]=refund&";
            count++;
        }

        if (mTransferFromCheckBox.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][type]=transfer_from&";
            count++;
        }

        if (mTransferToCheckBox.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][type]=transfer_to&";
            count++;
        }

        if (mFilFuelCheckBox.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][type]=gas_fill&";
            count++;
        }

        if (mConvertPointsCheckBox.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][type]=points_redemption&";
        }

        if (!dateRange.equals(""))
        {
            queryFilter+=dateRange;
        }

        if (queryFilter.length()>0)
            filterQuery = queryFilter.substring(0,queryFilter.length()-1);
    }

    @Override
    public void onBackPressed() {

        if (filterQuery.equals(""))
        {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("filterQuery",filterQuery);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        monthOfYear+=1;
        monthOfYearEnd+=1;

        String dayStr="";
        if (dayOfMonth<10)
            dayStr="0"+dayOfMonth;
        else dayStr=dayOfMonth+"";

        String dayStrEnd="";
        if (dayOfMonthEnd<10)
            dayStrEnd="0"+dayOfMonthEnd;
        else
            dayStrEnd=dayOfMonthEnd+"";

        String monthStr="";
        if (monthOfYear<10)
            monthStr="0"+monthOfYear;
        else
            monthStr=monthOfYear+"";

        String monthStrEnd="";
        if (monthOfYearEnd<10)
            monthStrEnd="0"+monthOfYearEnd;
        else monthStrEnd=monthOfYearEnd+"";

        dateRange="filter[where][createdOn][between][0]="+year+"-"+monthStr+"-"+dayStr
                +"T00:00:00.000Z&filter[where][createdOn][between][1]="+yearEnd+"-"+monthStrEnd+"-"+dayStrEnd+"T00:00:00.000Z&";

        String date = getString(R.string.from_duration)+ " "+dayOfMonth+"/"+(monthOfYear)+"/"+year+" "+
                getString(R.string.to_duration)+" "+dayOfMonthEnd+"/"+(monthOfYearEnd)+"/"+yearEnd;
        mSelectDateButton.setText(date);
        generateQuery();
    }
}
