package com.android.fuel.main.filter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.android.fuel.R;
import com.android.fuel.application.BaseActivity;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.GasCompany;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterInvoicesActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    private String filterQuery="";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.radio_fuel_91)
    CheckBox mFuel91RadioButton;

    @BindView(R.id.radio_fuel_95)
    CheckBox mFuel95RadioButton;

    @BindView(R.id.radio_fuel_diesel)
    CheckBox mFuelDieselRadioButton;

    @BindView(R.id.sp_fuel_company)
    Spinner mFuelCompanySpinner;

    @BindView(R.id.bt_select_date)
    Button mSelectDateButton;

    private String dateRange="";

    private ArrayList<GasCompany> mFuelCompanies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_invoices_filter);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setTitle(getResources().getString(R.string.filter));
        }

        if (getIntent().hasExtra("filterQuery"))
            filterQuery = getIntent().getStringExtra("filterQuery");

        setSpinner();

        mFuel91RadioButton.setOnCheckedChangeListener(this);
        mFuel95RadioButton.setOnCheckedChangeListener(this);
        mFuelDieselRadioButton.setOnCheckedChangeListener(this);

        if (!filterQuery.equals(""))
        {
            String query = filterQuery;
            if (query.contains("91"))
                mFuel91RadioButton.setChecked(true);

            if (query.contains("95"))
                mFuel95RadioButton.setChecked(true);

            if (query.contains("diesel"))
                mFuelDieselRadioButton.setChecked(true);

            if (query.contains("gasCompanyId"))
            {
                String[] pieces = query.split("=");

                mFuelCompanySpinner.setSelection(getCurrentFuelCompany(pieces[pieces.length-1]));
            }

        }


        findViewById(R.id.bt_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        findViewById(R.id.bt_select_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPicker();
            }
        });
    }

    private void showPicker()
    {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        //dpd.setAutoHighlight(mAutoHighlight);
        dpd.show(getFragmentManager(),"");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void setSpinner() {

        if (FuelApp.getCurrentFuelCompaniesList()!=null)
            mFuelCompanies.addAll(FuelApp.getCurrentFuelCompaniesList());

        ArrayAdapter<GasCompany> spinnerArrayAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_item,
                        mFuelCompanies); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        mFuelCompanySpinner.setAdapter(spinnerArrayAdapter);

        mFuelCompanySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setFilterQuery();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private int getCurrentFuelCompany(String companyId) {

        for (int i=0;i<mFuelCompanies.size();i++)
        {
            if (companyId.equals(mFuelCompanies.get(i).getId()))
                return i;
        }
        return 0;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        setFilterQuery();

    }

    private void setFilterQuery() {

        int count=0;
        String queryFilter = "";

        if (mFuel91RadioButton.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][gasType]=91&";
            count++;
        }

        if (mFuel95RadioButton.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][gasType]=95&";
            count++;
        }

        if (mFuelDieselRadioButton.isChecked()) {
            queryFilter += "filter[where][or][" + count + "][gasType]=diesel&";
            count++;
        }

        if (!dateRange.equals(""))
        {
            queryFilter+=dateRange;
        }

        if (mFuelCompanies.size()>0) {
            queryFilter += "filter[where][and][" + count + "][gasCompanyId]="+getSelectedCompany()+"&";
            count++;
        }

        if (queryFilter.length()>0)
            filterQuery = queryFilter.substring(0,queryFilter.length()-1);
    }

    private String getSelectedCompany() {
        GasCompany selectedItem = (GasCompany)mFuelCompanySpinner.getSelectedItem();
        return selectedItem.getId();
    }

    @Override
    public void onBackPressed() {

        if (filterQuery.equals(""))
        {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("filterQuery",filterQuery);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        monthOfYear+=1;
        monthOfYearEnd+=1;

        String dayStr="";
        if (dayOfMonth<10)
            dayStr="0"+dayOfMonth;
        else dayStr=dayOfMonth+"";

        String dayStrEnd="";
        if (dayOfMonthEnd<10)
            dayStrEnd="0"+dayOfMonthEnd;
        else
            dayStrEnd=dayOfMonthEnd+"";

        String monthStr="";
        if (monthOfYear<10)
            monthStr="0"+monthOfYear;
        else
            monthStr=monthOfYear+"";

        String monthStrEnd="";
        if (monthOfYearEnd<10)
            monthStrEnd="0"+monthOfYearEnd;
        else monthStrEnd=monthOfYearEnd+"";

        dateRange="filter[where][createdOn][between][0]="+year+"-"+monthStr+"-"+dayStr
                +"T00:00:00.000Z&filter[where][createdOn][between][1]="+yearEnd+"-"+monthStrEnd+"-"+dayStrEnd+"T00:00:00.000Z&";

        String date = getString(R.string.from_duration)+ " " +dayOfMonth+"/"+(monthOfYear)+"/"+year+
        " "+ getString(R.string.to_duration)+" "+dayOfMonthEnd+"/"+(monthOfYearEnd)+"/"+yearEnd;
        mSelectDateButton.setText(date);
        setFilterQuery();
    }
}
