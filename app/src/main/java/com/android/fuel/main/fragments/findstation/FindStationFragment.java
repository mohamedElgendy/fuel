package com.android.fuel.main.fragments.findstation;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.Dashboard;
import com.android.fuel.beans.Wallet;
import com.android.fuel.customviews.SphericalUtil;
import com.android.fuel.main.MainActivity;
import com.android.fuel.main.filter.FilterActivity;
import com.android.fuel.responses.GetBannersResponse;
import com.android.fuel.responses.GetSubWalletsCount;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class FindStationFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.map_container)
    FrameLayout mMapContainer;

    @BindView(R.id.rl_balance)
    RelativeLayout balanceRelativeLayout;

    @BindView(R.id.rl_subwallets)
    RelativeLayout subwalletsRelativeLayout;

    @BindView(R.id.rl_reward_points)
    RelativeLayout rewardPointsRelativeLayout;


    @BindView(R.id.bt_find_station)
    Button mFindStationButton;

    @BindView(R.id.tv_liters_in_95)
    TextView mLiters95TextView;

    @BindView(R.id.tv_liters_in_91)
    TextView mLiters91TextView;

    @BindView(R.id.tv_liters_in_diesel)
    TextView mLitersDieselTextView;

    @BindView(R.id.tv_rewards_points)
    TextView mRewardsPointsTextView;

    @BindView(R.id.tv_total_balance)
    TextView mTotalBalanceTextView;

    @BindView(R.id.tv_wallets)
    TextView mWalletsTextView;

    @BindView(R.id.ads_container)
    ImageView mAdsContainer;

    private Wallet userWallet;

    public FindStationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_station, container, false);
        ButterKnife.bind(this, rootView);


        modifyLayouts(rootView);

        initMap();

        if (!isLocationServiceEnabled(getActivity())) {
                showGPSDisabledAlertToUser();
        }
        return rootView;
    }

    private void modifyLayouts(final View rootView) {

        /*
            the target of this method is to get the actual height of the reward point card height
            and set this height to the walled card so in result both layout will have same height
         */

        final LinearLayout rewardCardLayout = rootView.findViewById(R.id.layout_reward_points_card);
        ViewTreeObserver vto = rewardCardLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rewardCardLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width  = rewardCardLayout.getMeasuredWidth();
                int height = rewardCardLayout.getMeasuredHeight();

                LinearLayout walletCardLayout = rootView.findViewById(R.id.layout_wallets_card);
                android.view.ViewGroup.LayoutParams params = walletCardLayout.getLayoutParams();
                params.width = width;
                params.height = height;
                walletCardLayout.setLayoutParams(params);
            }
        });

    }

    private AlertDialog mLocationAlertDialog;
    public void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(R.string.enable_location)
                .setCancelable(false)
                .setPositiveButton(R.string.location_enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.location_disable, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        mLocationAlertDialog = alertDialogBuilder.create();
        mLocationAlertDialog.show();
    }


    private boolean isHaveWallet()
    {
        if (userWallet==null)
            return false;
        return true;
    }

    @OnClick(R.id.rl_balance)
    void openWallet() {
        // navigate to find station details
        if (getActivity() instanceof MainActivity)
        {
            ((MainActivity)getActivity()).setCurrentFragment(R.id.action_wallets);
        }
    }

    @OnClick(R.id.rl_subwallets)
    void opensubWallet() {
        // navigate to find station details
        if (isHaveWallet())
        {
            FuelManager.openSubWallet(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.rl_reward_points)
    void openRewardPointsWallet() {
        // navigate to find station details
        if (isHaveWallet())
        {
            if (userWallet.getPoints()>0)
                FuelManager.openConvertPoints(getActivity());
            else
                Utils.showToast(getActivity(),R.string.no_enough_points);
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    @OnClick(R.id.bt_find_station)
    void findStation() {
        // navigate to find station details
        if (isHaveWallet())
        {
            FuelManager.openStationsListing(getActivity());
        }
        else {
            Utils.showToast(getActivity(),R.string.please_wait_wallet);
            getUserWallet();
        }
    }

    private void getUserWallet() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+ AppConstants.getUserId(getActivity())+"/wallet",
                Wallet.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
               // mProgressBar.setVisibility(View.GONE);
                if (response instanceof Wallet)
                {
                    userWallet = (Wallet) response;
                    FuelApp.setCurrentWallet(userWallet);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper.executeGet();


        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper2 = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Drivers/"+ AppConstants.getUserId(getActivity())+"/dashboardStats",
                Dashboard.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof Dashboard)
                {
                    Dashboard dashboard = (Dashboard) response;
                    if (dashboard!=null)
                    {
                        if (String.valueOf(dashboard.getBalance()).length()>3)
                            mTotalBalanceTextView.setText(String.format("%.1f",dashboard.getBalance()));
                        else
                            mTotalBalanceTextView.setText(dashboard.getBalance()+"");

                        mRewardsPointsTextView.setText(dashboard.getPoints()+"");

                        //getUserSubWalletsCountWallet(userWallet.getId());

                        mWalletsTextView.setText(dashboard.getCurrentWalletCount()+"/"+dashboard.getMaxWalletCount());//Here add real value

                        mLiters91TextView.setText(dashboard.getGas91Price()+"");
                        mLiters95TextView.setText(dashboard.getGas95Price()+"");
                        mLitersDieselTextView.setText(dashboard.getDieselPrice()+"");

                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper2.executeGet();
    }

    private void getUserSubWalletsCountWallet(String walletId) {

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+walletId+"/subWalletLimit",
                GetSubWalletsCount.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetSubWalletsCount)
                {
                    GetSubWalletsCount userWallet = (GetSubWalletsCount) response;

                    if (userWallet!=null)
                    {
                        mWalletsTextView.setText(userWallet.getCurrent()+"/"+userWallet.getMax());//Here add real value
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
            }
        }, -1);
        requestHelper.executeGet();
    }

    private void getWiderBanner() {
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Banners?filter={\"where\":{\"type\":\"wide\"}}",
                GetBannersResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                if (response instanceof GetBannersResponse)
                {
                    GetBannersResponse bannersResponse = (GetBannersResponse) response;
                    if (bannersResponse.getItems()!=null && bannersResponse.getItems().size()>0)
                    {
                        Picasso.with(getActivity())
                                .load(bannersResponse.getItems().get(0).get_image().getUrl())
                                .fit()
                                .into(mAdsContainer);
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }


    ///////////Map///////////////////////////////////////////////


    private static final int REQUEST_LOCATION = 0;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private GoogleMap mGoogleMap;
    private MarkerOptions myLocationMarkerOptions;


    private void initMap()
    {
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
           // Toast.makeText(getActivity(), "Google Service Is Available!!", Toast.LENGTH_SHORT).show();
        }

        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public boolean isLocationServiceEnabled(Context context){
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if(locationManager ==null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        return gps_enabled || network_enabled;

    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(final GoogleMap map) {
        mGoogleMap = map;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

    }


    private void moveToCurrentLocation(LatLng currentLocation)
    {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        // Zoom in, animating the camera.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    private Bitmap getMarkerBitmapFromView(LatLng myLocation, LatLng targetLocation, String name, @DrawableRes int resId) {



        double distance = SphericalUtil.computeDistanceBetween(targetLocation, myLocation);

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        TextView disTextView = (TextView) customMarkerView.findViewById(R.id.tv_distance);
        disTextView.setText(new DecimalFormat("#.##").format((distance/1000))+"KM");
        markerImageView.setImageResource(resId);

        TextView nameTextView = (TextView) customMarkerView.findViewById(R.id.tv_name);
        nameTextView.setText(name);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    /**
     * LOCATION LISTENER EVENTS
     *
     * */

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
//        startLocationUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();

        getUserWallet();
        getWiderBanner();


        getServicesAvailable();

        // Resuming the periodic location updates
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        //To clear markers
        //mMarkersOptionsList.clear();
        //mGoogleMap.clear();
        //myLocationMarkerOptions = null;
    }

    //Starting the location updates
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest,  this);
        }
    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(getActivity());
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(getActivity(), isAvailable, 0);
            dialog.show();
        } else {
            //Toast.makeText(getActivity(), "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }
    //Creating location request object
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(AppConstants.DISPLACEMENT);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        displayLocation();

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

        //Toast.makeText(getActivity(), "Location changed!", Toast.LENGTH_SHORT).show();

        // Displaying the new location on UI
        displayLocation();

    }

    //Method to display the location on UI
    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {


            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();

                LatLng currentLocation = new LatLng(latitude,longitude);

                if (myLocationMarkerOptions==null) {
                    myLocationMarkerOptions = new MarkerOptions().position(currentLocation).title("")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));
                    mGoogleMap.addMarker(myLocationMarkerOptions);
                }
                else {
                    myLocationMarkerOptions.position(currentLocation);
                }

                moveToCurrentLocation(currentLocation);

                //addPlacesMarkers(currentLocation);

            } else {

                //Toast.makeText(this, "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addPlacesMarkers(LatLng currentLocation) {

        /*if (mMarkersOptionsList.size()==0)
        {
            LatLng from =new LatLng(30.0598782,31.1930448);

            MarkerOptions marker1 = new MarkerOptions().position(from).title("ID_Station")//Here put id of station to compare it in case marker click "may name of station repeated"
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(currentLocation,from,"Lebanon Square",R.drawable.ic_close_dark)));
            mGoogleMap.addMarker(marker1);
            mMarkersOptionsList.add(marker1);
            from =new LatLng(30.0566211,31.2043101);
            MarkerOptions marker2 = new MarkerOptions().position(from).title("ID_Station")
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(currentLocation,from,"CIB",R.drawable.ic_close_dark)));
            mGoogleMap.addMarker(marker2);
            mMarkersOptionsList.add(marker2);
        }*/
    }

    //////////End Map////////////////////////////////////////////
}
