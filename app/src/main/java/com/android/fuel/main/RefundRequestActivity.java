package com.android.fuel.main;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.android.fuel.R;
import com.android.fuel.application.BaseActivity;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.PhotoFile;
import com.android.fuel.beans.RefundRequest;
import com.android.fuel.beans.ReviewRequest;
import com.android.fuel.beans.Station;
import com.android.fuel.beans.UserImage;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetPhotosResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RefundRequestActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_send_request)
    Button mSendRequest;

    @BindView(R.id.btn_capture_machine_reading)
    Button mCaptureMarchineReading;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.iv_machine_reading)
    ImageView mMachineReadingImageView;

    @BindView(R.id.et_message)
    EditText mMessageEditText;

    @BindView(R.id.et_liters)
    EditText mLitersEditText;

    private String mGasFilledId;

    private Uri outputFileUri;
    private String mCurrentPath=null;
    private ArrayList<PhotoFile> photosUrls = new ArrayList<>();//Uploaded photos

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_request);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //remove toolbar title
            getSupportActionBar().setTitle(getResources().getString(R.string.refund_request));
        }

        mGasFilledId = getIntent().getStringExtra("gasFilledId");
    }

    @OnClick(R.id.btn_send_request)
    void rate() {
        attemptRefund();
    }


    @OnClick(R.id.btn_capture_machine_reading)
    void capture() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }


    private void attemptRefund() {

        // Reset errors.
        mMessageEditText.setError(null);
        mLitersEditText.setError(null);

        // Store values at the time of the login attempt.
        String comment = mMessageEditText.getText().toString();
        String liters = mLitersEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(comment)) {
            mMessageEditText.setError(getString(R.string.error_field_required));
            focusView = mMessageEditText;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(liters)) {
            mLitersEditText.setError(getString(R.string.error_field_required));
            focusView = mLitersEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            uploadImage();
        }
    }


    private void uploadImage()
    {
        mProgressBar.setVisibility(View.VISIBLE);
        if (mCurrentPath!=null && !TextUtils.isEmpty(mCurrentPath))
        {

            Map<String, File> queryParams = new HashMap<>();
            queryParams.put("file",new File(mCurrentPath));

            if (queryParams.size()>0)
            {
                RequestHelper<Object> requestHelper = new RequestHelper<>(this,
                        Parameters.BASE_URL, Parameters.UPLOAD_PHOTOS,
                        GetPhotosResponse.class, new RequestListener<Object>() {

                    @Override
                    public void onSuccess(Object response, String apiName) {

                        GetPhotosResponse getPhotosResponse = (GetPhotosResponse) response;
                        if (getPhotosResponse!=null && getPhotosResponse.getResult()!=null
                                &&getPhotosResponse.getResult().getFiles() !=null
                                &&getPhotosResponse.getResult().getFiles().getFile() !=null)
                        {
                            photosUrls.addAll(getPhotosResponse.getResult().getFiles().getFile());
                            refundNow();
                        }

                    }

                    @Override
                    public void onFail(String message, String apiName) {

                        mProgressBar.setVisibility(ProgressBar.GONE);
                        findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                findViewById(R.id.v_reload).setVisibility(View.GONE);
                                uploadImage();
                            }
                        });
                        findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
                    }
                }, Parameters.SERVICE_UPLOAD_PHOTO, queryParams);

                requestHelper.executeMultiPart();
            }
            else
                refundNow();

        }
        else
            Utils.showToast(this,R.string.capture_machine);
    }

    private void refundNow()
    {
        mProgressBar.setVisibility(View.VISIBLE);
        //Call send message
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setDriverMessage(mMessageEditText.getText().toString());
        refundRequest.setDriverLiters(mLitersEditText.getText().toString());

        ArrayList<UserImage> images = new ArrayList<>();
        for (int i=0;i<photosUrls.size();i++)
        {
            UserImage image = new UserImage();
            image.setUrl(Parameters.PHOTOS_BASE+photosUrls.get(i).getName());
            images.add(image);
        }
        if (images.size()>0)
            refundRequest.set_image(images.get(0));

        RequestHelper<Object> requestHelper = new RequestHelper<>(this,
                Parameters.BASE_URL, "GasFills/"+mGasFilledId+"/refundRequest",
                RefundRequest.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                Utils.showToast(RefundRequestActivity.this,R.string.refund_request_sent);
                finish();
            }

            @Override
            public void onFail(String message, String apiName) {
                Utils.showToast(RefundRequestActivity.this,message);
            }
        }, -1);

        requestHelper.executeFromRawJson(new Gson().toJson(refundRequest));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openPickerIntent();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void openPickerIntent() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        outputFileUri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(intent, 1200);

    }

    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Camera");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1200) {

                Uri selectedImage = outputFileUri;
                mCurrentPath = getRealPathFromURI(RefundRequestActivity.this,selectedImage);
                mMachineReadingImageView.setImageURI(selectedImage);

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = managedQuery(contentUri, projection, null, null,
                null);

        if (cursor != null)
        {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return contentUri.getPath();
    }

}
