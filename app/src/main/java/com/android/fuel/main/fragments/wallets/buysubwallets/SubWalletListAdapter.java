package com.android.fuel.main.fragments.wallets.buysubwallets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.fuel.R;
import com.android.fuel.beans.SubWalletPackage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class SubWalletListAdapter extends RecyclerView.Adapter<SubWalletListAdapter.DataObjectHolder> {

    private ArrayList<SubWalletPackage> mDataSet;
    private Context context;
    private static SubWalletListAdapter.SubWalletPurchaseClickListener mSubWalletPurchaseClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_numberOfWallets)
        TextView mNumberOfWalletsTextView;

        @BindView(R.id.tv_wallet_price)
        TextView mSubWalletPriceTextView;

        @BindView(R.id.bt_wallet_purchase)
        Button mWalletPurchase;


        public DataObjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            //adding listener...
            mWalletPurchase.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mSubWalletPurchaseClickListener.onSubWalletPurchaseClick(getAdapterPosition(), v);
        }
    }

    public SubWalletListAdapter(ArrayList<SubWalletPackage> mDataSet, SubWalletListAdapter.SubWalletPurchaseClickListener mSubWalletPurchaseClickListener) {
        this.mDataSet = mDataSet;
        this.mSubWalletPurchaseClickListener = mSubWalletPurchaseClickListener;
    }

    @Override
    public SubWalletListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sub_wallets, parent, false);
        context = parent.getContext();

        SubWalletListAdapter.DataObjectHolder dataObjectHolder = new SubWalletListAdapter.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(SubWalletListAdapter.DataObjectHolder holder, int position) {

        SubWalletPackage mSubWallet = mDataSet.get(position);

        holder.mNumberOfWalletsTextView.setText(mSubWallet.getNumberOfWallets()+"");
        holder.mSubWalletPriceTextView.setText(mSubWallet.getPrice()+" "+ context.getResources().getString(R.string.sar));
    }

    public void addItem(SubWalletPackage dataObj) {
        mDataSet.add(dataObj);
        notifyDataSetChanged();
    }

    public void deleteItem(int index) {
        mDataSet.remove(index);
        notifyItemRemoved(index);
    }

    public void updateItem(int index, SubWalletPackage dataObj) {
        mDataSet.set(index, dataObj);
        notifyItemChanged(index, dataObj);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface SubWalletPurchaseClickListener {
        void onSubWalletPurchaseClick(int position, View v);
    }
}


