package com.android.fuel.main.fragments.wallets.wallettransactions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.application.FuelConstants;
import com.android.fuel.beans.WalletTransaction;
import com.android.fuel.main.filter.FilterWalletTransactionActivity;
import com.android.fuel.main.fragments.wallets.wallettransactions.adapters.WalletTransactionsAdapter;
import com.android.fuel.responses.GetWalletTransactionsResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class TransactionsFragment extends Fragment  {

    @BindView(R.id.wallet_packages_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private View mRootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String filterQuery = "";

    // declare station list components
    private WalletTransactionsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<WalletTransaction> mPackagesArrayList = new ArrayList<>();;

    public TransactionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.station_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            //navigate to filter Activity
            Intent filter = new Intent(getActivity(), FilterWalletTransactionActivity.class);
            if (!filterQuery.equals(""))
                filter.putExtra("filterQuery",filterQuery);
            startActivityForResult(filter,6666);
            return true;
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 6666) {
            if(resultCode == Activity.RESULT_OK){

                filterQuery=data.getStringExtra("filterQuery");
                mPackagesArrayList.clear();
                mAdapter.notifyDataSetChanged();

                String allQuery="";

                if (filterQuery!=null && !filterQuery.equals(""))
                    allQuery+="&"+filterQuery;

                getStations(allQuery);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_transactions, container, false);
        ButterKnife.bind(this, mRootView);

      ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.wallet_transaction));
        }


        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        // initialize views
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new WalletTransactionsAdapter(mPackagesArrayList);
        mRecyclerView.setAdapter(mAdapter);

        getStations(filterQuery);
    }

    private void getStations(String query) {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Wallets/"+ FuelApp.getCurrentWallet().getId()+"/transactions?"+query,
                GetWalletTransactionsResponse.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetWalletTransactionsResponse)
                {
                    GetWalletTransactionsResponse stationsResponse = (GetWalletTransactionsResponse) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mPackagesArrayList.clear();
                        mPackagesArrayList.addAll(excludeTransactions(stationsResponse.getItems()));
                        mAdapter.notifyDataSetChanged();
                    }
                    else
                        Utils.showToast(getActivity(),R.string.no_transactions_result);
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                mRootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getStations(filterQuery);
                    }
                });
                mRootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

    private ArrayList<WalletTransaction> excludeTransactions(ArrayList<WalletTransaction> items) {

        ArrayList<WalletTransaction> filteredList = new ArrayList<>();
        for (int i=0;i<items.size();i++)
        {
            filteredList.add(items.get(i));
            /*if (!items.get(i).getType().equals(FuelConstants.WALLET_TRANSACTION_TYPE_PACKAGE_REDEMPTION))
            {
                filteredList.add(items.get(i));
            }*/
        }

        return filteredList;
    }

}

