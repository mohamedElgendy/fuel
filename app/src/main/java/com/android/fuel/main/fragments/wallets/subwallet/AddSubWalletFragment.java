package com.android.fuel.main.fragments.wallets.subwallet;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.android.fuel.R;
import com.android.fuel.application.FuelApp;
import com.android.fuel.beans.GasCompany;
import com.android.fuel.beans.PhotoFile;
import com.android.fuel.beans.UserImage;
import com.android.fuel.requests.AddCarWalletRequest;
import com.android.fuel.requests.AddUserWalletRequest;
import com.android.fuel.requests.PurchaseSubWalletPackage;
import com.android.fuel.responses.GeneralResponse;
import com.android.fuel.responses.GetInvoicesResponse;
import com.android.fuel.responses.GetPhotosResponse;
import com.android.fuel.responses.GetSubWalletsPackage;
import com.android.fuel.responses.GetSubWalletsPackagesResponse;
import com.android.fuel.responses.SubWalletPurchasePackageResponse;
import com.android.fuel.services.Parameters;
import com.android.fuel.services.RequestHelper;
import com.android.fuel.services.RequestListener;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.Utils;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.android.fuel.services.Parameters.SERVICE_GET_BANNERS;

/**
 * Created by Mohamed Elgendy.
 */

public class AddSubWalletFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Uri outputFileUri;
    private String mCurrentPath=null;
    private ArrayList<PhotoFile> photosUrls = new ArrayList<>();//Uploaded photos


    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.radio_fuel_91)
    RadioButton mFuel91RadioButton;

    @BindView(R.id.radio_fuel_95)
    RadioButton mFuel95RadioButton;

    @BindView(R.id.radio_fuel_diesel)
    RadioButton mFuelDieselRadioButton;

    @BindView(R.id.radio_sub_user_wallet)
    RadioButton mSubUserWallet;

    @BindView(R.id.radio_car_wallet)
    RadioButton mRadioCarWallet;

    @BindView(R.id.et_wallet_name)
    EditText mWalletNameEditText;

    @BindView(R.id.et_wallet_car_model)
    EditText mCarModelEditText;
    @BindView(R.id.et_wallet_board_name)
    EditText mBoardNameEditText;

    @BindView(R.id.layout_car_wallet)
    LinearLayout mCarWalletLayout;

    @BindView(R.id.sp_packages)
    Spinner mSubWalletPackagesSpinner;

    private ArrayList<SubWalletPurchasePackageResponse> mSubWalletPackagesList = new ArrayList<>();

    View rootView;

    public AddSubWalletFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_sub_wallet, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.find_station));
        }

        getSubWalletPackages();

        return rootView;
    }

    private void getSubWalletPackages() {

        mProgressBar.setVisibility(View.VISIBLE);
        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL,"Wallets/"+ FuelApp.getCurrentWallet().getId()+"/walletPackagePurchases?",
                GetSubWalletsPackage.class, new RequestListener<Object>() {

            @Override
            public void onSuccess(Object response, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GetSubWalletsPackage)
                {
                    GetSubWalletsPackage stationsResponse = (GetSubWalletsPackage) response;
                    if (stationsResponse.getItems()!=null && stationsResponse.getItems().size()>0)
                    {
                        mSubWalletPackagesList.clear();
                        mSubWalletPackagesList.addAll(stationsResponse.getItems());
                        setSubWalletPackagesSpinner();
                    }
                    else {
                        Utils.showToast(getActivity(), R.string.no_sub_wallet);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFail(String message, String apiName) {
                mProgressBar.setVisibility(View.GONE);
                rootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                        getSubWalletPackages();
                    }
                });
                rootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
            }
        }, SERVICE_GET_BANNERS);
        requestHelper.executeGet();
    }

    private void setSubWalletPackagesSpinner() {

        ArrayAdapter<SubWalletPurchasePackageResponse> spinnerArrayAdapter = new ArrayAdapter<>
                (getActivity(), android.R.layout.simple_spinner_item,
                        mSubWalletPackagesList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        mSubWalletPackagesSpinner.setAdapter(spinnerArrayAdapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.radio_sub_user_wallet)
    void subUserWallet() {
        showCarWallet(false);
    }

    @OnClick(R.id.radio_car_wallet)
    void carWallet() {
        showCarWallet(true);
    }

    @OnClick(R.id.bt_create_wallet)
    void createWallet() {
        if (mCarWalletLayout.getVisibility()==View.VISIBLE)
            attemptCreateCar();
        else
            addUserWallet();
    }

    @OnClick(R.id.bt_car_wallet_upload)
    void loadImage() {

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }

    private void showCarWallet(boolean isShow){
        if(isShow){
            mCarWalletLayout.setVisibility(View.VISIBLE);
        }else {
            mCarWalletLayout.setVisibility(View.GONE);
        }
    }

    private void attemptCreateCar() {

        // Reset errors.
        mWalletNameEditText.setError(null);
        mCarModelEditText.setError(null);
        mBoardNameEditText.setError(null);

        // Store values at the time of the login attempt.
        String name = mWalletNameEditText.getText().toString();
        String carModel = mCarModelEditText.getText().toString();
        String boradName = mBoardNameEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            mWalletNameEditText.setError(getString(R.string.error_field_required));
            focusView = mWalletNameEditText;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(carModel)) {
            mCarModelEditText.setError(getString(R.string.error_field_required));
            focusView = mCarModelEditText;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(boradName)) {
            mBoardNameEditText.setError(getString(R.string.error_field_required));
            focusView = mBoardNameEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            uploadImage();
        }
    }


    private void uploadImage()
    {
        mProgressBar.setVisibility(View.VISIBLE);

        if (mCurrentPath!=null && !TextUtils.isEmpty(mCurrentPath))
        {
            Map<String, File> queryParams = new HashMap<>();
            queryParams.put("file",new File(mCurrentPath));

            if (queryParams.size()>0)
            {
                RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                        Parameters.BASE_URL, Parameters.UPLOAD_PHOTOS,
                        GetPhotosResponse.class, new RequestListener<Object>() {

                    @Override
                    public void onSuccess(Object response, String apiName) {

                        GetPhotosResponse getPhotosResponse = (GetPhotosResponse) response;
                        if (getPhotosResponse!=null && getPhotosResponse.getResult()!=null
                                &&getPhotosResponse.getResult().getFiles() !=null
                                &&getPhotosResponse.getResult().getFiles().getFile() !=null)
                        {
                            photosUrls.addAll(getPhotosResponse.getResult().getFiles().getFile());
                        }
                        addCarWallet();
                    }

                    @Override
                    public void onFail(String message, String apiName) {

                        mProgressBar.setVisibility(ProgressBar.GONE);
                        rootView.findViewById(R.id.rl_reload).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rootView.findViewById(R.id.v_reload).setVisibility(View.GONE);
                                uploadImage();
                            }
                        });
                        rootView.findViewById(R.id.v_reload).setVisibility(View.VISIBLE);
                    }
                }, Parameters.SERVICE_UPLOAD_PHOTO, queryParams);

                requestHelper.executeMultiPart();
            }

            else
                addCarWallet();

        }
        else
            addCarWallet();
    }

    private void addUserWallet() {
        mProgressBar.setVisibility(View.VISIBLE);

        AddUserWalletRequest request = new AddUserWalletRequest();
        request.setName(mWalletNameEditText.getText().toString());
        request.setType("sub_wallet");
        request.setParentWalletId(FuelApp.getCurrentWallet().getId());
        String subPurchaseId= ((SubWalletPurchasePackageResponse)mSubWalletPackagesSpinner.getSelectedItem()).getId();
        request.setUsedSubWalletPuchaseId(subPurchaseId);

        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+ FuelApp.getCurrentWallet().getId()+"/subWallets",
                GeneralResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GeneralResponse) {

                   // showDoneDialog(position);
                    Utils.showToast(getActivity(),R.string.add_subwallet_done);
                    getActivity().finish();
                }
                else
                    Utils.showToast(getActivity(), R.string.internet_error);
            }

            @Override
            public void onFail(String message, String apiName) {

            }
        }, -1);

        requestHelper.executeFromRawJson(new Gson().toJson(request));
    }

    private void addCarWallet() {

            mProgressBar.setVisibility(View.VISIBLE);

        AddCarWalletRequest request = new AddCarWalletRequest();
        request.setName(mWalletNameEditText.getText().toString());
        request.setType("car_specific");
        request.setParentWalletId(FuelApp.getCurrentWallet().getId());

        String subPurchaseId= ((SubWalletPurchasePackageResponse)mSubWalletPackagesSpinner.getSelectedItem()).getId();
        request.setUsedSubWalletPuchaseId(subPurchaseId);
        request.setCarModel(mCarModelEditText.getText().toString());
        request.setPlateNumber(mBoardNameEditText.getText().toString());

        ArrayList<UserImage> images = new ArrayList<>();
        for (int i=0;i<photosUrls.size();i++)
        {
            UserImage image = new UserImage();
            image.setUrl(Parameters.PHOTOS_BASE+photosUrls.get(i).getName());
            images.add(image);
        }
        if (images.size()>0)
            request.set_image(images.get(0));

        if (mFuel91RadioButton.isChecked()) {
            request.setFuelType("91");
        }

        if (mFuel95RadioButton.isChecked()) {
            request.setFuelType("95");
        }

        if (mFuelDieselRadioButton.isChecked()) {
            request.setFuelType("diesel");
        }



        RequestHelper<Object> requestHelper = new RequestHelper<>(getActivity(),
                Parameters.BASE_URL, "Wallets/"+ FuelApp.getCurrentWallet().getId()+"/subWallets",
                GeneralResponse.class, new RequestListener<Object>() {
            @Override
            public void onSuccess(Object response, String apiName) {

                mProgressBar.setVisibility(View.GONE);
                if (response instanceof GeneralResponse) {

                    // showDoneDialog(position);
                    Utils.showToast(getActivity(),R.string.add_subwallet_done);
                    getActivity().finish();
                }
                else
                    Utils.showToast(getActivity(), R.string.internet_error);
            }

            @Override
            public void onFail(String message, String apiName) {

            }
        }, -1);

        requestHelper.executeFromRawJson(new Gson().toJson(request));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openPickerIntent();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void openPickerIntent() {

// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name) + File.separator);
        root.mkdirs();
        final String fname = "profile"+ Calendar.getInstance().getTimeInMillis()+".png";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getActivity().getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, 1200);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri selectedImageUri=null;
            if (requestCode == 1200) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }
                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }

                if(selectedImageUri!=null)
                {
                    mCurrentPath = getRealPathFromURI(getActivity(),selectedImageUri);
                }
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = getActivity().managedQuery(contentUri, projection, null, null,
                null);

        if (cursor != null)
        {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return contentUri.getPath();
    }

}
