package com.android.fuel.main.fragments.wallets.rechargewallet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.fuel.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohamed Elgendy.
 */

public class RechargeWalletFinishFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_balance)
    TextView mBalanceTextView;

    @BindView(R.id.tv_points)
    TextView mPointsTextView;

    @BindView(R.id.tv_title)
    TextView mTitleTextView;

    public RechargeWalletFinishFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recharge_wallet_finish, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null){
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.thank_you));
        }


        mBalanceTextView.setText(getArguments().getFloat("totalBalance")+"");
        mPointsTextView.setText(getArguments().getInt("totalPoints")+"");

        if (getArguments().containsKey("isFromConvertPoints"))
            mTitleTextView.setText(R.string.convert_points_done);

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
