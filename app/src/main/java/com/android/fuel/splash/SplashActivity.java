package com.android.fuel.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.android.fuel.R;;
import com.android.fuel.utils.AppConstants;
import com.android.fuel.utils.FuelManager;

public class SplashActivity extends AppCompatActivity {

    protected boolean _active = true;
    protected int _splashTime = 3 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {

                } finally {
                    finish();
//                    FuelManager.openMain(SplashActivity.this);

                   if (AppConstants.isUserLoggedIn(SplashActivity.this))
                        FuelManager.openMain(SplashActivity.this);
                    else
                        FuelManager.openLogin(SplashActivity.this);
                }
            }
        };
        splashTread.start();
    }
}